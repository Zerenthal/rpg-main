package net.bobmandude9889.rpg.gui.event;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import net.bobmandude9889.rpg.gui.GUI;

public class GUIClickEvent {

	public ItemStack item;
	public int slot;
	public Inventory inventory;
	public Player player;
	public GUI gui;
	public boolean leftClick;
	public boolean rightClick;
	public boolean shiftClick;

	public GUIClickEvent(ItemStack item, int slot, Inventory inventory, Player player, GUI gui, boolean left, boolean shift) {
		super();
		this.item = item;
		this.slot = slot;
		this.inventory = inventory;
		this.player = player;
		this.gui = gui;
		this.leftClick = left;
		this.rightClick = !left;
		this.shiftClick = shift;
	}

}
