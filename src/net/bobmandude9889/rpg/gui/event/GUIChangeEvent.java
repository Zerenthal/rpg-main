package net.bobmandude9889.rpg.gui.event;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import net.bobmandude9889.rpg.gui.GUI;

public class GUIChangeEvent {

	public Inventory inventory;
	public Player player;
	public GUI gui;
	public ItemStack[] added;
	public ItemStack[] removed;
	public ItemStack[] contents;
	public InventoryAction action;
	public int clickedSlot;
	
	
	public GUIChangeEvent(Inventory inventory, Player player, GUI gui, ItemStack[] contents, ItemStack[] added, ItemStack[] removed, InventoryAction action, int clickedSlot) {
		this.inventory = inventory;
		this.player = player;
		this.gui = gui;
		this.added = added;
		this.removed = removed;
		this.contents = contents;
		this.action = action;
		this.clickedSlot = clickedSlot;
	}
	
}
