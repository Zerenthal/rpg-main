package net.bobmandude9889.rpg.gui.event;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import net.bobmandude9889.rpg.gui.GUI;

public class GUICloseEvent {

	public Inventory inventory;
	public Player player;
	public GUI gui;
	public boolean playerClosed;
	private boolean canceled = false;
	
	public GUICloseEvent(Inventory inventory, Player player, GUI gui, boolean playerClosed) {
		this.inventory = inventory;
		this.player = player;
		this.gui = gui;
		this.playerClosed = playerClosed;
	}

	public boolean isCanceled() {
		return canceled;
	}

	public void setCanceled(boolean canceled) {
		this.canceled = canceled;
	}
}
