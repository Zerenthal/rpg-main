package net.bobmandude9889.rpg.gui.action;

import net.bobmandude9889.rpg.gui.event.GUICloseEvent;

public interface GUICloseAction {

	public void onClose(GUICloseEvent event);
	
}
