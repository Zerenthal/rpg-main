package net.bobmandude9889.rpg.gui.action;

import net.bobmandude9889.rpg.gui.event.GUIChangeEvent;

public interface GUIChangeAction {

	public void onChange(GUIChangeEvent event);
	
}
