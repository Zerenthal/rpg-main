package net.bobmandude9889.rpg.gui.action;

import org.bukkit.event.inventory.InventoryClickEvent;

public interface GUIOnClickAction {

	public void onClick(InventoryClickEvent e);
	
}
