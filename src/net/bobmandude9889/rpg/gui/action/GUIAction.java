package net.bobmandude9889.rpg.gui.action;

import net.bobmandude9889.rpg.gui.event.GUIClickEvent;

public interface GUIAction {

	public static GUIAction blank = new GUIAction() {@Override public void onClick(GUIClickEvent event) {}};
	
	public void onClick(GUIClickEvent event);
	
}
