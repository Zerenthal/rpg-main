package net.bobmandude9889.rpg.gui;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import net.bobmandude9889.rpg.gui.action.GUIAction;
import net.bobmandude9889.rpg.gui.action.GUIChangeAction;
import net.bobmandude9889.rpg.gui.action.GUICloseAction;
import net.bobmandude9889.rpg.gui.action.GUIOnClickAction;
import net.bobmandude9889.rpg.gui.event.GUICloseEvent;

public class GUI {

	public GUITemplate template;
	public Player player;
	public int size;
	public InventoryType inventoryType;
	public String name;
	public ItemStack[] contents;
	public GUIAction[] actions;
	public GUICloseAction onClose;
	public GUIChangeAction onChange;
	public GUIOnClickAction onClick;
	public boolean allowChange = false;
	public boolean allowDrag = false;
	protected boolean updating = false;

	public Inventory inventory;

	protected GUI(int size, String name, ItemStack[] contents, GUIAction[] actions, Player player, GUITemplate template) {
		this.size = size;
		this.inventoryType = InventoryType.CHEST;
		this.actions = actions;
		this.player = player;
		this.template = template;
		this.contents = new ItemStack[size];
		for (int i = 0; i < size; i++) {
			if (contents[i] != null)
				this.contents[i] = contents[i].clone();
		}

		setName(name);
	}
	
	protected GUI(int size, String name, ItemStack[] contents, GUIAction[] actions, Player player, GUITemplate template, InventoryType inventoryType) {
		this.size = size;
		this.inventoryType = inventoryType;
		this.actions = actions;
		this.player = player;
		this.template = template;
		this.contents = new ItemStack[size];
		for (int i = 0; i < size; i++) {
			if (contents[i] != null)
				this.contents[i] = contents[i].clone();
		}

		setName(name);
	}

	public void setOnClose(GUICloseAction onClose) {
		this.onClose = onClose;
	}

	public void setOnChange(GUIChangeAction onChange) {
		this.onChange = onChange;
	}

	public void setOnClick(GUIOnClickAction onClick) {
		this.onClick = onClick;
	}

	public void setContents(ItemStack[] contents) {
		this.contents = new ItemStack[size];
		for (int i = 0; i < contents.length; i++) {
			setItem(i, contents[i]);
		}
	}

	public void setItem(int i, ItemStack item) {
		if (item != null) {
			inventory.setItem(i, item.clone());
			contents[i] = item.clone();
		} else {
			inventory.setItem(i, null);
			contents[i] = null;
		}
	}

	public void setAction(int i, GUIAction action) {
		actions[i] = action;
	}

	public void setButton(int i, ItemStack item, GUIAction action) {
		setItem(i, item);
		setAction(i, action);
	}

	public void setAllowChange(boolean allow) {
		this.allowChange = allow;
	}

	public void setAllowDrag(boolean allow) {
		this.allowDrag = allow;
	}

	public void setName(String name) {
		this.name = name;
		if (!inventoryType.equals(InventoryType.CHEST)) {
			inventory = Bukkit.createInventory(player, inventoryType, ChatColor.translateAlternateColorCodes('&', name));
		} else {
			inventory = Bukkit.createInventory(player, size, ChatColor.translateAlternateColorCodes('&', name));
		}
		for (int i = 0; i < contents.length; i++) {
			if (contents[i] != null)
				inventory.setItem(i, contents[i].clone());
		}

		updating = true;
		ItemStack cursor = player.getOpenInventory().getCursor().clone();
		player.getOpenInventory().setCursor(null);
		player.openInventory(inventory);
		GUIHandler.instance.guis.put(player, this);
		updating = false;
		player.getOpenInventory().setCursor(cursor);
	}

	public void closeInventory() {
		updating = true;
		player.closeInventory();
		if (onClose != null) 
			onClose.onClose(new GUICloseEvent(inventory, player, this, false));
	}

	public GUIAction getAction(int i) {
		if (actions.length > i)
			return actions[i];
		return null;
	}

}
