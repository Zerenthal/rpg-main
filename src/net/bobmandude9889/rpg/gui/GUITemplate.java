package net.bobmandude9889.rpg.gui;

import net.bobmandude9889.rpg.gui.action.GUIAction;
import net.bobmandude9889.rpg.gui.action.GUIChangeAction;
import net.bobmandude9889.rpg.gui.action.GUICloseAction;
import net.bobmandude9889.rpg.gui.action.GUIOnClickAction;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

public class GUITemplate {

	ItemStack[] contents;
	GUIAction[] actions;
	int size;
	String name;
	InventoryType inventoryType;
	GUICloseAction onClose;
	GUIChangeAction onChange;
	GUIOnClickAction onClick;
	boolean allowChange = false;
	boolean allowDrag = false;

	public GUITemplate(int size, String name) {
		this.size = size;
		this.name = name;
		this.inventoryType = InventoryType.CHEST;
		contents = new ItemStack[size];
		actions = new GUIAction[size];
	}
	
	public GUITemplate(int size, String name, InventoryType inventoryType) {
		this.size = size;
		this.name = name;
		this.inventoryType = inventoryType;
		contents = new ItemStack[size];
		actions = new GUIAction[size];
	}

	public void setOnClose(GUICloseAction onClose) {
		this.onClose = onClose;
	}

	public void setOnChange(GUIChangeAction onChange) {
		this.onChange = onChange;
	}

	public void setOnClick(GUIOnClickAction onClick) {
		this.onClick = onClick;
	}

	public void setContents(ItemStack[] contents) {
		this.contents = contents;
	}

	public void setItem(int i, ItemStack item) {
		contents[i] = item;
	}

	public void setAction(int i, GUIAction action) {
		actions[i] = action;
	}

	public void setButton(int i, ItemStack item, GUIAction action) {
		setItem(i, item);
		setAction(i, action);
	}

	public void setAllowChange(boolean allow) {
		this.allowChange = allow;
	}

	public void setAllowDrag(boolean allow) {
		this.allowDrag = allow;
	}

	protected GUI createInstance(Player player) {
		GUI gui = new GUI(size, name, contents.clone(), actions.clone(), player, this, inventoryType);
		copyToGUI(gui);
		return gui;
	}

	public void copyToGUI(GUI gui) {
		gui.size = size;
		gui.inventoryType = inventoryType;
		gui.setName(name);
		gui.setContents(this.contents);
		gui.actions = actions.clone();
		gui.template = this;
		gui.onClose = onClose;
		gui.allowChange = allowChange;
		gui.allowDrag = allowDrag;
		gui.onClick = onClick;
		gui.onChange = onChange;
		
	}

	public GUI open(Player player) {
		return GUIHandler.instance.openGUI(player, this);
	}

}