package net.bobmandude9889.rpg.gui;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

import net.bobmandude9889.rpg.RPG;
import net.bobmandude9889.rpg.gui.action.GUIAction;
import net.bobmandude9889.rpg.gui.event.GUIChangeEvent;
import net.bobmandude9889.rpg.gui.event.GUIClickEvent;
import net.bobmandude9889.rpg.gui.event.GUICloseEvent;

public class GUIHandler implements Listener {

	public HashMap<Player, GUI> guis;

	public static GUIHandler instance;

	public GUIHandler() {
		guis = new HashMap<Player, GUI>();
		instance = this;
	}

	public GUI openGUI(Player player, GUITemplate template) {
		GUI gui = null;

		if (guis.containsKey(player)) {
			gui = guis.get(player);
			template.copyToGUI(gui);
		} else
			gui = template.createInstance(player);

		guis.put(player, gui);
		return gui;
	}

	public void closeGUI(Player player) {
		if(guis.containsKey(player)) {
			guis.get(player).closeInventory();
		}
	}
	
	@EventHandler
	public void onDrag(InventoryDragEvent e){
		if(guis.containsKey(e.getWhoClicked())) {
			GUI gui = guis.get(e.getWhoClicked());
			e.setCancelled(!gui.allowDrag);
		}
	}
	
	@EventHandler
	public void onClick(final InventoryClickEvent e) {
		if (guis.containsKey(e.getWhoClicked()) && !e.getAction().equals(InventoryAction.NOTHING) && !e.getResult().equals(Result.DENY)) {
			final GUI gui = guis.get(e.getWhoClicked());
			InventoryType invType = e.getClickedInventory().getType();
			e.setCancelled(!gui.allowChange);
			if(gui.onClick != null)
				gui.onClick.onClick(e);
			if (e.getClickedInventory() != null && (invType.equals(gui.inventoryType))) {
				int slot = e.getSlot();
				GUIAction action = gui.getAction(slot);
				if (action != null) {
					e.setCancelled(true);
					GUIClickEvent event = new GUIClickEvent(e.getCurrentItem(), slot, e.getClickedInventory(), (Player) e.getWhoClicked(), gui, e.isLeftClick(), e.isShiftClick());
					action.onClick(event);
				}
			}
			if (gui.onChange != null && !e.isCancelled()) {
				Bukkit.getScheduler().scheduleSyncDelayedTask(RPG.instance, new Runnable() {
					@Override
					public void run() {
						ItemStack[] added = new ItemStack[gui.size];
						ItemStack[] removed = new ItemStack[gui.size];
						ItemStack[] contents = new ItemStack[gui.size];

						for (int i = 0; i < gui.size; i++) {
							ItemStack invItem = gui.inventory.getContents()[i];
							ItemStack guiItem = gui.contents[i];

							if(invItem == null && guiItem != null)
								removed[i] = guiItem.clone();
							
							if (invItem != null && guiItem == null) {
								added[i] = invItem.clone();
							}
							
							if(invItem != null && guiItem != null) {
								
								if(invItem.getType().equals(guiItem.getType()) && invItem.getAmount() != guiItem.getAmount()) {
									ItemStack item = invItem.clone();
									if(invItem.getAmount() < guiItem.getAmount()) {
										item.setAmount(guiItem.getAmount() - invItem.getAmount());
										removed[i] = item;
									} else {
										item.setAmount(invItem.getAmount() - guiItem.getAmount());
										added[i] = item;
									}
								} else if(!invItem.getType().equals(guiItem.getType())) {
									added[i] = invItem;
									removed[i] = guiItem;
								}
								
							}
							contents[i] = invItem;
						}
						
						added[e.getSlot()] = gui.inventory.getContents()[e.getSlot()];
						
						GUIChangeEvent event = new GUIChangeEvent(gui.inventory, (Player) e.getWhoClicked(), gui, contents, added, removed, e.getAction(), e.getRawSlot());
						gui.onChange.onChange(event);

						gui.setContents(gui.inventory.getContents());
					}
				});
			}
		}
	}

	@EventHandler
	public void onClose(InventoryCloseEvent e) {
		if (guis.containsKey(e.getPlayer())) {
			GUI gui = guis.get(e.getPlayer());
			GUICloseEvent guiCloseEvent = null;
			if (gui.onClose != null && !gui.updating) {
				guiCloseEvent = new GUICloseEvent(gui.inventory, gui.player, gui, true);
				gui.onClose.onClose(guiCloseEvent);
			}
			if (guiCloseEvent == null || !guiCloseEvent.isCanceled()) {
				guis.remove(e.getPlayer());
			}
			
			
		}
	}

}
