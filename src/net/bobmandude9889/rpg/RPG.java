package net.bobmandude9889.rpg;

import java.io.File;

import net.bobmandude9889.rpg.command.CommandEnum;
import net.bobmandude9889.rpg.command.TabCompleteHandler;
import net.bobmandude9889.rpg.duel.Arena;
import net.bobmandude9889.rpg.duel.DuelManager;
import net.bobmandude9889.rpg.gui.GUIHandler;
import net.bobmandude9889.rpg.mobRegion.MobRegionManager;
import net.bobmandude9889.rpg.npc.NPCEntity;
import net.bobmandude9889.rpg.npc.NPCHandler;
import net.bobmandude9889.rpg.npc.SkinCacher;
import net.bobmandude9889.rpg.player.AFKManager;
import net.bobmandude9889.rpg.player.RPGPlayerManager;
import net.bobmandude9889.rpg.quest.TriggerListener;
import net.bobmandude9889.rpg.shop.ShopManager;
import net.bobmandude9889.rpg.sidebar.SidebarDisplay;
import net.bobmandude9889.rpg.sidebar.SidebarInfoCommand;
import net.bobmandude9889.rpg.sidebar.SidebarManager;
import net.bobmandude9889.rpg.skills.BlockRespawner;
import net.bobmandude9889.rpg.skills.SkillDataManager;
import net.bobmandude9889.rpg.skills.archery.BowType;
import net.bobmandude9889.rpg.skills.farming.SeedManager;
import net.bobmandude9889.rpg.skills.fishing.FishingRegionManager;
import net.bobmandude9889.rpg.trade.TradeManager;
import net.bobmandude9889.rpg.util.MobHealthDisplay;
import net.bobmandude9889.rpg.util.Util;
import net.bobmandude9889.rpg.util.sql.DBConn;
import net.bobmandude9889.rpg.vault.VaultManager;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class RPG extends JavaPlugin implements Listener {

	public static RPG instance;

	public static FileConfiguration config;

	public static Arena arena;

	public static File dataFolder;
	
	public static DBConn sqlForumConn;
	public static DBConn sqlMCConn;

	@Override
	public void onEnable() {
		
		instance = this;
		dataFolder = new File(this.getDataFolder(), "data");
		if (!dataFolder.exists())
			dataFolder.mkdir();

		this.saveDefaultConfig();

		config = this.getConfig();

		Util.registerGlowEnchantment();

		SeedManager.init();
		BlockRespawner.init();
		SkillDataManager.init();
		DuelManager.init();
		TradeManager.init();
		SidebarManager.init();
		SidebarInfoCommand.init();
		ShopManager.init();
		SkinCacher.init();
		MobRegionManager.init();

		Bukkit.getPluginManager().registerEvents(new VaultManager(), this);
		Bukkit.getPluginManager().registerEvents(new NPCHandler(), this);
		Bukkit.getPluginManager().registerEvents(new RPGPlayerManager(), this);
		Bukkit.getPluginManager().registerEvents(new AFKManager(), this);
		Bukkit.getPluginManager().registerEvents(new GUIHandler(), this);
		Bukkit.getPluginManager().registerEvents(this, this);
		Bukkit.getPluginManager().registerEvents(new MobHealthDisplay(), this);
		Bukkit.getPluginManager().registerEvents(new MobRegionManager(), this);
		Bukkit.getPluginManager().registerEvents(new TriggerListener(), this);
		Bukkit.getPluginManager().registerEvents(new TabCompleteHandler(), this);

		BowType.registerCrafting();

		Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
			@Override
			public void run() {
				BlockRespawner.attemptBlockRespawn();
				AFKManager.checkAFK();
			}
		}, 20, 20);
		TabCompleteHandler.init();
		CommandEnum.registerCommands();
		sqlForumConn = new DBConn(config.getString("db.ip"), config.getString("db.port"), "zerentha_forums", config.getString("db.username"), config.getString("db.password"));
		sqlMCConn = new DBConn(config.getString("db.ip"), config.getString("db.port"), "zerentha_minecraft", config.getString("db.username"), config.getString("db.password"));
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		SidebarManager.setDisplay(e.getPlayer(), SidebarDisplay.SKILL_ALL);
	}

	@Override
	public void onDisable() {
		for (Player player : Bukkit.getOnlinePlayers()) {
			RPGPlayerManager.getPlayer(player).saveConfig();
			VaultManager.savePlayerVault(player);
			for (NPCEntity npc : NPCHandler.NPCs.keySet()) {
				npc.remove(player);
			}
		}
		SeedManager.save();
		FishingRegionManager.save();
	}

	public static void reload() {
		instance.reloadConfig();
	}
	
}
