package net.bobmandude9889.rpg.dialogue;

import java.lang.reflect.Field;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import net.bobmandude9889.rpg.player.RPGPlayerManager;
import net.bobmandude9889.rpg.quest.Quest;
import net.bobmandude9889.rpg.util.Title;
import net.bobmandude9889.rpg.util.Util;

public class DialogueAction {
	
	long open_menu = -1;
	List<String> commands;
	String message;
	String title;
	String subtitle;
	long title_fade_in = -1;
	long title_fade_out = -1;
	long title_length = -1;
	String flag;
	String remove_flag;
	long start_quest = -1;
	
	public void set(String key, Object value) {
		try {
			Field field = DialogueAction.class.getDeclaredField(key);
			field.setAccessible(true);
			field.set(this, value);
		} catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		}
	}
	
	public void execute(Player player) {
		if(commands != null) {
			for(String command : commands) {
				if(command.startsWith("/"))
					command = command.substring(1);
				command = command.replace("%player%", player.getName());
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command);
			}
		}
		if(open_menu != -1) {
			DialogueManager.openDialogue(player, ((Long)open_menu).intValue());
		}
		if(message != null) {
			player.sendMessage(Util.parseColors(message).replace("%player%", player.getName()));
		}
		if(title != null) {
			Title titleObj = new Title();
			titleObj.setTitle(title);
			if(subtitle != null) {
				titleObj.setSubtitle(subtitle);
			}
			titleObj.setTimingsToSeconds();
			if (title_fade_in != -1)
				titleObj.setFadeInTime(((Long)title_fade_in).intValue());
			if (title_fade_out != -1)
				titleObj.setFadeOutTime(((Long)title_fade_out).intValue());
			if (title_length != -1)
				titleObj.setStayTime(((Long)title_length).intValue());
			titleObj.send(player);
		}
		if(flag != null) {
			RPGPlayerManager.getPlayer(player).setFlag(flag);
		}
		if(remove_flag != null) {
			RPGPlayerManager.getPlayer(player).removeFlag(remove_flag);
		}
		if(start_quest != -1) {
			RPGPlayerManager.getPlayer(player).startQuest(Quest.getQuest(((Long)start_quest).intValue()));
		}
	}
	
}
