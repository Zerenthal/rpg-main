package net.bobmandude9889.rpg.dialogue;

import org.bukkit.entity.Player;

import net.bobmandude9889.rpg.command.Command;

public class DialogueReload {

	@Command
	public void reload(Player player) {
		DialogueManager.loadConfig();
		player.sendMessage("Reloaded dialogue");
	}
	
}
