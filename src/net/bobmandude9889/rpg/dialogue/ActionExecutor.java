package net.bobmandude9889.rpg.dialogue;

import org.bukkit.entity.Player;

import net.bobmandude9889.rpg.command.Command;

public class ActionExecutor {

	@Command
	public void onClickAction(String actionId, Player player) {
		DialogueManager.actionMap.get(actionId).execute(player);
	}
	
}
