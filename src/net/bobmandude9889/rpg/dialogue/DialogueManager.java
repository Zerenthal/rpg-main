package net.bobmandude9889.rpg.dialogue;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import net.bobmandude9889.rpg.RPG;
import net.bobmandude9889.rpg.command.CommandEnum;
import net.bobmandude9889.rpg.util.BookUtil;
import net.bobmandude9889.rpg.util.Util;
import net.bobmandude9889.rpg.util.sql.DBConn;
import net.bobmandude9889.rpg.util.sql.SqlStatement;
import net.bobmandude9889.rpg.util.tellraw.TellrawBuilder;
import net.bobmandude9889.rpg.util.tellraw.TellrawChatText;

public class DialogueManager {

	static HashMap<Integer, String> dialogueMap;
	static HashMap<String, DialogueAction> actionMap;
	static String[] actionOptions = {"open_menu","commands","message","title","subtitle","title_fade_in","title_fade_out","title_length","flag","remove_flag","start_quest"};
	
	static {
		loadConfig();
	}
	
	public static void loadConfig() {
		if(actionMap != null) {
			for(String key : actionMap.keySet()) {
				CommandEnum.unregisterCommand(key);
			}
		}
		dialogueMap = new HashMap<>();
		actionMap = new HashMap<>();
		
		DBConn conn = RPG.sqlMCConn;
		
		SqlStatement stmt = new SqlStatement(conn);
		ResultSet results = stmt.select("Dialogue");
		
		try {
			while(results.next()) {
				TellrawBuilder b = new TellrawBuilder().addText(results.getString("Text"));
				
				ResultSet options = stmt.select("DialogueOptions","ParentDialogue=" + results.getInt("Id") + " ORDER BY Id");
				
				while(options.next()) {
					b.addText("\n\n");
					TellrawChatText text = new TellrawChatText(Util.parseColors(options.getString("Text")));
					
					JSONObject actionObj = (JSONObject) new JSONParser().parse(options.getString("Action"));
					
					if(actionObj.size() > 1) {
						String actionId = UUID.randomUUID().toString();
						text.setRunCommandOnClick("/" + actionId);
						CommandEnum.registerNewCommand(actionId, "", "", "", ActionExecutor.class);
						DialogueAction action = new DialogueAction();
						for(String act : actionOptions) {
							if(actionObj.containsKey(act)) {
								action.set(act, actionObj.get(act));
							}
						}
						actionMap.put(actionId, action);
					}
					b.addText(text);
				}
				dialogueMap.put(results.getInt("Id"), b.getJSON());
			}
		} catch (SQLException | ParseException e) {
			e.printStackTrace();
		}
	}
	
	public static ItemStack getBookItem(int id) {
		return BookUtil.createBook(dialogueMap.get(id));
	}

	public static boolean openDialogue(Player player, int id) {
		return BookUtil.openBook(getBookItem(id), player);
	}

}
