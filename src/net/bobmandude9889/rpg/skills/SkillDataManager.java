package net.bobmandude9889.rpg.skills;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import net.bobmandude9889.rpg.RPG;
import net.bobmandude9889.rpg.player.RPGPlayerManager;
import net.bobmandude9889.rpg.sidebar.SidebarDisplay;
import net.bobmandude9889.rpg.sidebar.SidebarManager;

public class SkillDataManager {

	static HashMap<Player, SkillData> data;

	public static File dataFolder;

	public static void init() {
		data = new HashMap<Player, SkillData>();

		dataFolder = new File(RPG.instance.getDataFolder(), "player_data");
		dataFolder.mkdir();

		for (Player player : Bukkit.getOnlinePlayers()) {
			loadData(player);
		}
	}

	public static File getPlayerFile(Player player) {
		File playerFile = new File(dataFolder, player.getUniqueId().toString() + ".yml");
		try {
			playerFile.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return playerFile;
	}

	public static void loadData(Player player) {
		YamlConfiguration playerConfig = RPGPlayerManager.getPlayer(player).config;

		SkillData sData = new SkillData(player);

		ConfigurationSection section = playerConfig.getConfigurationSection("skills");

		if (section == null)
			section = playerConfig.createSection("skills");

		for (SkillType type : SkillType.values()) {
			if (section.contains(type.name()))
				sData.xp.put(type, section.getDouble(type.name()));
			else
				sData.xp.put(type, 0d);
		}

		data.put(player, sData);
	}

	public static void saveData(Player player) {
		YamlConfiguration playerConfig = RPGPlayerManager.getPlayer(player).config;

		SkillData sData = data.get(player);

		ConfigurationSection section = playerConfig.getConfigurationSection("skills");

		if (section == null)
			section = playerConfig.createSection("skills");

		for (SkillType type : sData.xp.keySet()) {
			section.set(type.name(), sData.getXP(type));
		}
	}

	public static SkillData getData(Player player) {
		if (!data.containsKey(player))
			loadData(player);
		return data.get(player);
	}

	public static class SkillData {

		HashMap<SkillType, Double> xp;

		private Player player;

		public SkillData(Player player) {
			xp = new HashMap<SkillType, Double>();
			this.player = player;
		}

		public void setXP(SkillType type, double playerXP) {
			xp.put(type, playerXP);
			saveData(player);
			SidebarManager.setSkillDisplay(player, type);
			SidebarManager.setDisplay(player, SidebarDisplay.SKILL_INFO);
		}

		public double getXP(SkillType type) {
			if (xp.containsKey(type))
				return xp.get(type);
			return -1;
		}

	}
	
}
