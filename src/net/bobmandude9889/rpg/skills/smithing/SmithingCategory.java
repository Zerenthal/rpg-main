package net.bobmandude9889.rpg.skills.smithing;

import org.bukkit.Material;

public class SmithingCategory {
	String name;
	Material mainItem;
	Material[] clickableItems;
	Material[] contents;
	
	public SmithingCategory(String name, Material mainItem, Material[] clickableItems, Material[] contents){
		this.name = name;
		this.mainItem = mainItem;
		this.clickableItems = clickableItems;
		this.contents = contents;
	}

	public String getName() {		
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Material getMainItem() {
		return mainItem;
	}

	public void setMainItem(Material mainItem) {
		this.mainItem = mainItem;
	}

	public Material[] getClickableItems() {
		return clickableItems;
	}

	public void setClickableItems(Material[] clickableItems) {
		this.clickableItems = clickableItems;
	}

	public Material[] getContents() {
		return contents;
	}

	public void setContents(Material[] contents) {
		this.contents = contents;
	}


}
