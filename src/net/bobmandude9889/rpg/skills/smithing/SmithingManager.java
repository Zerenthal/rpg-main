package net.bobmandude9889.rpg.skills.smithing;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.bobmandude9889.rpg.RPG;
import net.bobmandude9889.rpg.gui.GUI;
import net.bobmandude9889.rpg.gui.GUITemplate;
import net.bobmandude9889.rpg.player.RPGPlayer;
import net.bobmandude9889.rpg.player.RPGPlayerManager;
import net.bobmandude9889.rpg.skills.SkillManager;
import net.bobmandude9889.rpg.skills.SkillType;
import net.bobmandude9889.rpg.util.ItemBuilder;
import net.bobmandude9889.rpg.util.Util;
import net.minecraft.server.v1_12_R1.BlockPosition;
import net.minecraft.server.v1_12_R1.ChatMessage;
import net.minecraft.server.v1_12_R1.ContainerAnvil;
import net.minecraft.server.v1_12_R1.EntityHuman;
import net.minecraft.server.v1_12_R1.EntityPlayer;
import net.minecraft.server.v1_12_R1.PacketPlayOutOpenWindow;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;

public class SmithingManager extends SkillManager {

	static Material[] others = { Material.BOW, Material.FLINT_AND_STEEL, Material.FISHING_ROD, Material.CARROT_STICK, Material.ELYTRA };
	static Material[] leather = { Material.LEATHER_BOOTS, Material.LEATHER_CHESTPLATE, Material.LEATHER_HELMET, Material.LEATHER_LEGGINGS };
	static Material[] wood = { Material.WOOD_AXE, Material.WOOD_HOE, Material.WOOD_PICKAXE, Material.WOOD_SPADE, Material.WOOD_SWORD };
	static Material[] stone = { Material.STONE_AXE, Material.STONE_HOE, Material.STONE_PICKAXE, Material.STONE_SPADE, Material.STONE_SWORD };
	static Material[] iron = { Material.IRON_AXE, Material.IRON_HOE, Material.IRON_PICKAXE, Material.IRON_SPADE, Material.IRON_SWORD, Material.IRON_BOOTS, Material.IRON_CHESTPLATE, Material.IRON_HELMET, Material.IRON_LEGGINGS, Material.IRON_INGOT };
	static Material[] diamond = { Material.DIAMOND_AXE, Material.DIAMOND_HOE, Material.DIAMOND_PICKAXE, Material.DIAMOND_SPADE, Material.DIAMOND_SWORD, Material.DIAMOND_BOOTS, Material.DIAMOND_CHESTPLATE, Material.DIAMOND_HELMET, Material.DIAMOND_LEGGINGS, Material.DIAMOND };
	static Material[] gold = { Material.GOLD_AXE, Material.GOLD_HOE, Material.GOLD_PICKAXE, Material.GOLD_SPADE, Material.GOLD_SWORD, Material.GOLD_BOOTS, Material.GOLD_CHESTPLATE, Material.GOLD_HELMET, Material.GOLD_LEGGINGS, Material.GOLD_INGOT };
	static Material[] chain = { Material.CHAINMAIL_BOOTS, Material.CHAINMAIL_CHESTPLATE, Material.CHAINMAIL_HELMET, Material.CHAINMAIL_LEGGINGS };

	public static GUITemplate gui;

	static SmithingCategory[] categories = { new SmithingCategory("Iron", Material.IRON_INGOT, iron, iron), new SmithingCategory("Gold", Material.GOLD_INGOT, gold, gold), new SmithingCategory("Diamond", Material.DIAMOND, diamond, diamond), };

	static {

		gui = new GUITemplate(9, "Smithing");

		gui.setAllowChange(false);

		// 0, 1, "2", 3, "4", 5, "6", 7, 8

		gui.setButton(2, new ItemBuilder(Material.IRON_INGOT).setName("&rIron").setLore("Create your iron items here.").getItem(), (event) -> {
			openCategory(getCategoryFromName("Iron"), event.player);
		});

		gui.setButton(4, new ItemBuilder(Material.GOLD_INGOT).setName("&rGold").setLore("Create your gold items here.").getItem(), (event) -> {
			openCategory(getCategoryFromName("Gold"), event.player);
		});

		gui.setButton(6, new ItemBuilder(Material.DIAMOND).setName("&rDiamond").setLore("Create your diamond items here.").getItem(), (event) -> {
			openCategory(getCategoryFromName("Diamond"), event.player);
		});

	}

	public SmithingManager(RPGPlayer player, SkillType type) {
		super(player, type);
	}

	@Override
	public void openInfoGUI(Player player) {

	}

	@EventHandler
	public void onSmith(PlayerInteractEvent e) {
		if (e.getPlayer().equals(player.player) && e.getAction().equals(org.bukkit.event.block.Action.RIGHT_CLICK_BLOCK) && (!e.getPlayer().isSneaking() || e.getPlayer().getInventory().getItemInMainHand().getType().equals(Material.AIR)) && e.getClickedBlock().getType().equals(Material.ANVIL)) {
			e.setCancelled(true);
			Material itemInHand = e.getPlayer().getInventory().getItemInMainHand().getType();
			boolean clickedWithItem = false;

			categoryloop: for (SmithingCategory category : categories) {
				for (Material clickableItem : category.clickableItems) {

					if (clickableItem.equals(itemInHand)) {
						clickedWithItem = true;
						openCategory(category, player.player);
						break categoryloop;
					}
				}
			}
			if (!clickedWithItem) {
				GUI openedGUI = gui.open(player.player);
				if (player.player.hasPermission("zerenthal.anvil"))
					openedGUI.setButton(8, new ItemBuilder(Material.ANVIL).setName("&6Open Anvil").getItem(), (event) -> {
						openAnvil(player.player);
					});
			}
		}
	}

	public static void openAnvil(Player player) {
		player.closeInventory();

		EntityPlayer p = ((CraftPlayer) player).getHandle();
		AnvilContainer container = new AnvilContainer(p);
		int c = p.nextContainerCounter();
		p.playerConnection.sendPacket(new PacketPlayOutOpenWindow(c, "minecraft:anvil", new ChatMessage("Repairing", new Object[] {}), 0));
		p.activeContainer = container;
		p.activeContainer.windowId = c;
		p.activeContainer.addSlotListener(p);
	}

	public static class AnvilContainer extends ContainerAnvil {
		public AnvilContainer(EntityHuman entity) {
			super(entity.inventory, entity.world, new BlockPosition(0, 0, 0), entity);
		}

		@Override
		public boolean canUse(EntityHuman entityhuman) {
			return true;
		}
	}

	public static SmithingCategory getCategoryFromName(String name) {
		for (SmithingCategory category : categories)
			if (category.name.equals(name))
				return category;

		return null;
	}

	public static void openCategory(SmithingCategory category, Player player) {
		ConfigurationSection config = RPG.config.getConfigurationSection(SkillType.SMITHING.name() + "." + category.name.toUpperCase());

		RPGPlayer rpgPlayer = RPGPlayerManager.getPlayer(player);
		int skillLevel = rpgPlayer.getSkill(SkillType.SMITHING).getLevel();
		GUI categoryGui = new GUITemplate(45, "Smithing | " + category.name).open(player);
		categoryGui.setAllowChange(false);

		// 0 ,1 ,2 ,3 ,4, 5, 6, 7, 8,
		// 9 ,10,11,12,13,14,15,16,17,
		// 18,19,20,21,22,23,24,25,26,
		// 27,28,29,30,31,32,33,34,35,
		// 36,37,38,39,40,41,42,43,44
		ItemBuilder iron = new ItemBuilder(Material.IRON_INGOT).setName("&rIron").setLore("Create your iron items here.");
		iron = category.name.equals("Iron") ? iron.glow() : iron;
		categoryGui.setButton(2, iron.getItem(), (event) -> {
			openCategory(getCategoryFromName("Iron"), event.player);
		});
		ItemBuilder gold = new ItemBuilder(Material.GOLD_INGOT).setName("&rGold").setLore("Create your gold items here.");
		gold = category.name.equals("Gold") ? gold.glow() : gold;
		categoryGui.setButton(4, gold.getItem(), (event) -> {
			openCategory(getCategoryFromName("Gold"), event.player);
		});
		ItemBuilder diamond = new ItemBuilder(Material.DIAMOND).setName("&rDiamond").setLore("Create your diamond items here.");
		diamond = category.name.equals("Diamond") ? diamond.glow() : diamond;
		categoryGui.setButton(6, diamond.getItem(), (event) -> {
			openCategory(getCategoryFromName("Diamond"), event.player);
		});

		if (player.hasPermission("zerenthal.anvil"))
			categoryGui.setButton(8, new ItemBuilder(Material.ANVIL).setName("&6Open Anvil").getItem(), (event) -> {
				openAnvil(player);
			});
		// 0 : axe
		// 1 : hoe
		// 2 : pickaxe
		// 3 : shovel
		// 4 : sword
		// 5 : boots
		// 6 : chestplate
		// 7 : helmet
		// 8 : leggings

		setSmithingItemGuiButton(10, category, 7, "HELMET", skillLevel, config, player, categoryGui);
		setSmithingItemGuiButton(19, category, 6, "CHESTPLATE", skillLevel, config, player, categoryGui);
		setSmithingItemGuiButton(28, category, 8, "LEGGINGS", skillLevel, config, player, categoryGui);
		setSmithingItemGuiButton(37, category, 5, "BOOTS", skillLevel, config, player, categoryGui);

		setSmithingItemGuiButton(21, category, 4, "SWORD", skillLevel, config, player, categoryGui);
		setSmithingItemGuiButton(22, category, 2, "PICKAXE", skillLevel, config, player, categoryGui);
		setSmithingItemGuiButton(23, category, 0, "AXE", skillLevel, config, player, categoryGui);
		setSmithingItemGuiButton(24, category, 3, "SHOVEL", skillLevel, config, player, categoryGui);
		setSmithingItemGuiButton(25, category, 1, "HOE", skillLevel, config, player, categoryGui);
	}

	public static void setSmithingItemGuiButton(int guiLocation, SmithingCategory category, int contentIndex, String typeName, int skillLevel, ConfigurationSection config, Player player, GUI categoryGui) {
		RPGPlayer rpgPlayer = RPGPlayerManager.getPlayer(player);
		ItemBuilder item = new ItemBuilder(category.getContents()[contentIndex]);
		Boolean itemCraftable = true;
		String itemLore = "";
		itemLore = (skillLevel >= config.getInt(typeName + ".req_level") ? ChatColor.GREEN : ChatColor.RED) + "Required Level is " + config.getInt(typeName + ".req_level");
		itemCraftable = skillLevel >= config.getInt(typeName + ".req_level") ? itemCraftable : false;
		final int itemAmount = getOreAmountRequired(category.getContents()[contentIndex]);
		itemLore += "\n" + (getAmountInInventory(category.getMainItem(), player) >= itemAmount ? ChatColor.GREEN : ChatColor.RED) + "Required Materials are " + itemAmount + "x " + category.getName() + (itemAmount != 1 ? "s" : "");
		itemCraftable = getAmountInInventory(category.getMainItem(), player) >= itemAmount ? itemCraftable : false;
		final double itemXpGain = config.getDouble(typeName + ".xp_gained");
		itemLore += "\n" + ChatColor.GRAY + "XP gained: " + itemXpGain;
		item.setLore(itemLore.split("\n"));
		item.setName((itemCraftable ? ChatColor.GREEN : ChatColor.RED) + item.getName());
		final boolean finalItemCraftable = itemCraftable;
		categoryGui.setButton(guiLocation, item.getItem(), (event) -> {
			if (finalItemCraftable) {
				Util.takeItem(player, category.getMainItem(), itemAmount);
				rpgPlayer.giveItem(new ItemStack(category.getContents()[contentIndex]));
				rpgPlayer.getSkill(SkillType.SMITHING).addXP(itemXpGain);
				openCategory(category, player);
			}
		});
	}

	public static int getAmountInInventory(Material item, Player player) {
		int amount = 1;
		while (player.getInventory().contains(item, amount)) {
			amount++;
		}
		amount--;
		return amount;
	}

	public static int getOreAmountRequired(Material item) {
		List<ItemStack> materials = getRequiredMaterials(new ItemStack(item));
		int amount = 0;
		for (ItemStack material : materials) {

			if (material != null && !material.getType().equals(Material.STICK)) {
				amount++;
			}
		}
		return amount;

	}

	@SuppressWarnings("unchecked")
	public static List<ItemStack> getRequiredMaterials(ItemStack item) {
		Recipe recipe = Bukkit.getServer().getRecipesFor(item).get(0);
		if (recipe instanceof ShapedRecipe) {
			try {
				Field field = ShapedRecipe.class.getDeclaredField("ingredients");
				field.setAccessible(true);
				Map<Character, ItemStack> ingredients = (Map<Character, ItemStack>) field.get(recipe);
				return new ArrayList<ItemStack>(ingredients.values());
			} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		} else {
			try {
				Field field = ShapelessRecipe.class.getDeclaredField("ingredients");

				field.setAccessible(true);
				List<ItemStack> ingredients = (List<ItemStack>) field.get(recipe);
				return ingredients;
			} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
}
