package net.bobmandude9889.rpg.skills.archery;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ShapedRecipe;

import net.bobmandude9889.rpg.RPG;
import net.bobmandude9889.rpg.util.ItemBuilder;
import net.bobmandude9889.rpg.util.Util;

public enum BowType {

	BASIC(Material.STICK, 0, null),
	IRON(Material.IRON_INGOT, 1, ChatColor.WHITE),
	GOLD(Material.GOLD_INGOT, 2, ChatColor.GOLD),
	DIAMOND(Material.DIAMOND, 3, ChatColor.AQUA);
	
	public Material material;
	public int power;
	public ChatColor color;
	
	private BowType(Material material, int power, ChatColor color) {
		this.material = material;
		this.power = power;
		this.color = color;
	}
	
	public static void registerCrafting() {
		List<BowType> skip = new ArrayList<BowType>();
		Collections.addAll(skip, new BowType[] {BowType.BASIC});
		for(BowType bow : BowType.values()) {
			if(!skip.contains(bow)) {
				NamespacedKey key = new NamespacedKey(RPG.instance, RPG.instance.getDescription().getName());
				ShapedRecipe bowRec = new ShapedRecipe(key, new ItemBuilder(Material.BOW).setName(ChatColor.RESET + "" + bow.color + Util.capName(bow.name()) + " Bow").addEnchantment(Enchantment.ARROW_DAMAGE, bow.power).getItem());
				bowRec.shape("sd ","s d","sd ");
				bowRec.setIngredient('s', Material.STRING);
				bowRec.setIngredient('d', bow.material);
				try {
					Bukkit.getServer().addRecipe(bowRec);
				} catch (IllegalStateException e) {
					
				}
			}
		}
	}
	
}
