package net.bobmandude9889.rpg.skills.archery;

import java.util.HashMap;

import org.bukkit.Particle;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageModifier;

import net.bobmandude9889.rpg.player.RPGPlayer;
import net.bobmandude9889.rpg.skills.CombatUtil;
import net.bobmandude9889.rpg.skills.SkillManager;
import net.bobmandude9889.rpg.skills.SkillType;
import net.md_5.bungee.api.ChatColor;

@SuppressWarnings("deprecation")
public class ArcheryManager extends SkillManager implements Listener{

	private HashMap<Arrow, BowType> shotBy;
	
	public ArcheryManager(RPGPlayer player, SkillType type) {
		super(player, type);
		shotBy = new HashMap<Arrow, BowType>();
	}

	@Override
	public void openInfoGUI(Player player) {
	}

	@EventHandler
	public void onArrowShoot(EntityShootBowEvent e){
		if(e.getEntityType().equals(EntityType.PLAYER) && e.getEntity().equals(player.player)) {
			BowType type = CombatUtil.getBowType(e.getBow());
			ConfigurationSection bowSect = config.getConfigurationSection(type.name());
			if(getLevel() >= bowSect.getDouble("req_level")) {
				shotBy.put((Arrow) e.getProjectile(), type);
			} else {
				e.setCancelled(true);
				player.player.sendMessage(ChatColor.RED + "You must be level " + ChatColor.DARK_RED
						+ bowSect.getInt("req_level") + ChatColor.RED + " to use that bow! You are only level " + ChatColor.DARK_RED + getLevel());
			}
		}
	}
	
	@EventHandler
	public void onArrowHit(EntityDamageByEntityEvent e) {
		if(shotBy.containsKey(e.getDamager())) {
			BowType type = shotBy.get(e.getDamager());
			ConfigurationSection bowSect = config.getConfigurationSection(type.name());
			
			int crit = CombatUtil.getCrit(bowSect.getDouble("crit_chance"), bowSect.getInt("crit_max"), player.player);
			
			if(crit > 0) {
				player.player.spawnParticle(Particle.HEART, e.getEntity().getLocation().add(0, 1.3, 0),crit,0.3,0,0.3);
				if(e.getEntity().getType().equals(EntityType.PLAYER)) {
					((Player) e.getEntity()).spawnParticle(Particle.HEART, e.getEntity().getLocation().add(0, 1.3, 0),crit,0.3,0,0.3);
				}
			}
			double damageWithArmor = e.getFinalDamage();
			e.setDamage(DamageModifier.ARMOR, 0);
			e.setDamage(damageWithArmor + crit);
			addXP(Math.round(e.getFinalDamage()));
		}
	}
	
}
