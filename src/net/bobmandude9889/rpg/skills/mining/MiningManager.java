package net.bobmandude9889.rpg.skills.mining;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import net.bobmandude9889.rpg.gui.GUI;
import net.bobmandude9889.rpg.gui.GUITemplate;
import net.bobmandude9889.rpg.player.RPGPlayer;
import net.bobmandude9889.rpg.shop.MaterialData;
import net.bobmandude9889.rpg.skills.BlockRespawner;
import net.bobmandude9889.rpg.skills.SkillManager;
import net.bobmandude9889.rpg.skills.SkillType;
import net.bobmandude9889.rpg.util.ItemBuilder;
import net.md_5.bungee.api.ChatColor;
import net.milkbowl.vault.item.Items;

public class MiningManager extends SkillManager implements Listener {

	ConfigurationSection oreConfig;
	ConfigurationSection pickConfig;

	public MiningManager(RPGPlayer player, SkillType type) {
		super(player, type);
		oreConfig = config.getConfigurationSection("ores");
		pickConfig = config.getConfigurationSection("picks");
	}

	@EventHandler
	public void onBreak(BlockBreakEvent e) {
		String typeStr = e.getBlock().getType().name();
		Player player = e.getPlayer();
		if (e.getPlayer().equals(this.player.player) && oreConfig.contains(typeStr)
				&& player.getGameMode().equals(GameMode.SURVIVAL)) {
			e.setCancelled(true);

			if (player.getInventory().getItemInMainHand() != null
					&& pickConfig.contains(player.getInventory().getItemInMainHand().getType().name())) {

				ConfigurationSection pickSect = pickConfig
						.getConfigurationSection(player.getInventory().getItemInMainHand().getType().name());

				if (getLevel() >= pickSect.getDouble("req_level")) {

					ConfigurationSection typeSect = oreConfig.getConfigurationSection(typeStr);

					if (getLevel() >= typeSect.getInt("req_level")) {

						Location loc = e.getBlock().getLocation();
						Material type = e.getBlock().getType();

						int delay = typeSect.getInt("respawn_time");
						double xp = typeSect.getDouble("xp_per_ore");

						String[] dropTypeSplit = typeSect.getString("dropped_item").split(":");
						Material dropType = Material.getMaterial(dropTypeSplit[0]);
						ItemStack drop = new ItemStack(dropType);
						if(dropTypeSplit.length > 1) {
							drop.setDurability(Short.parseShort(dropTypeSplit[1]));
						}

						BlockRespawner.addBlock(loc, new MaterialData(type,(short) 0), delay);
						e.getBlock().setType(Material.STONE);
						this.player.giveItem(drop);
						addXP(xp);
					} else {
						player.sendMessage(ChatColor.RED + "You must be level " + ChatColor.DARK_RED
								+ typeSect.getInt("req_level") + ChatColor.RED + " to mine that ore! You are only level " + ChatColor.DARK_RED + getLevel());
					}
				} else {
					player.sendMessage(ChatColor.RED + "You must be level " + ChatColor.DARK_RED
							+ pickSect.getInt("req_level") + ChatColor.RED + " to use that pickaxe! You are only level " + ChatColor.DARK_RED + getLevel());
				}
			}
		}
	}

	private ItemStack getItem(Material material) {
		ItemBuilder builder = new ItemBuilder(material);
		builder.setName("&e" + Items.itemByType(material).getName());
		
		String action = "";
		int reqLevel = -1;
		
		if(oreConfig.contains(material.name())) {
			action = "mine this ore";
			reqLevel = oreConfig.getInt(material.name() + ".req_level");
		} else if(pickConfig.contains(material.name())) {
			action = "use this tool";
			reqLevel = pickConfig.getInt(material.name() + ".req_level");
		}
		
		String message = "Must be level " + reqLevel + " to " + action + ".";
		ChatColor color = ChatColor.RED;
		
		String unlocked = "LOCKED.";
		
		if(getLevel() >= reqLevel) {
			color = ChatColor.GREEN;
			unlocked = "UNLOCKED.";
		}
		
		builder.setLore(color + message, "", color + unlocked);
		return builder.getItem();
	}
	
	@Override
	public void openInfoGUI(Player player) {
		GUI gui = new GUITemplate(36, type.name).open(player);
		
		gui.setAllowChange(false);
		
		for(int i = 0; i < gui.size; i++) {
			gui.setItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 7).setName(" ").getItem());
		}
		
		//Pickaxes
		gui.setItem(11, getItem(Material.STONE_PICKAXE));
		gui.setItem(12, getItem(Material.IRON_PICKAXE));
		gui.setItem(14, getItem(Material.GOLD_PICKAXE));
		gui.setItem(15, getItem(Material.DIAMOND_PICKAXE));
		
		//Ores
		gui.setItem(19, getItem(Material.COAL_ORE));
		gui.setItem(20, getItem(Material.IRON_ORE));
		gui.setItem(21, getItem(Material.LAPIS_ORE));
		gui.setItem(22, getItem(Material.GOLD_ORE));
		gui.setItem(23, getItem(Material.REDSTONE_ORE));
		gui.setItem(24, getItem(Material.DIAMOND_ORE));
		gui.setItem(25, getItem(Material.EMERALD_ORE));
	}

}
