package net.bobmandude9889.rpg.skills.fishing;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import net.bobmandude9889.rpg.RPG;
import net.bobmandude9889.rpg.util.Util;

public class FishingRegionManager {

	private static File dataFile;
	private static YamlConfiguration data;
	
	public static HashMap<String, FishingRegion> regions;
	
	static {
		dataFile = new File(RPG.dataFolder, "fishing_regions.yml");
		if (!dataFile.exists()) {
			try {
				dataFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		data = YamlConfiguration.loadConfiguration(dataFile);
		
		regions = new HashMap<>();
		
		for (String key : data.getKeys(false)) {
			ConfigurationSection sect = data.getConfigurationSection(key);
			
			Location min = Util.stringToLocation(sect.getString("min"));
			Location max = Util.stringToLocation(sect.getString("max"));
			
			List<String> allowed = sect.getStringList("allowed");
			List<FishType> types = new ArrayList<>();
			
			for (String typeStr : allowed) {
				types.add(FishType.valueOf(typeStr));
			}
			
			FishingRegion region = new FishingRegion(min, max, types);
			regions.put(key, region);
		}
	}
	
	public static void save() {
		try {
			for (String key : data.getKeys(false)) {
				data.set(key, null);
			}
			
			for (String key : regions.keySet()) {
				FishingRegion region = regions.get(key);
				ConfigurationSection sect = data.createSection(key);
				sect.set("min", Util.locationToString(region.min));
				sect.set("max", Util.locationToString(region.max));
				
				List<String> allowed = new ArrayList<>();
				
				for (FishType type : region.allowed) {
					allowed.add(type.name());
				}
				
				sect.set("allowed", allowed);
			}
			
			data.save(dataFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static FishingRegion createRegion(String name, Location min, Location max, List<FishType> allowed) {
		FishingRegion region = new FishingRegion(min, max, allowed);
		regions.put(name, region);
		return region;
	}
	
	public static FishingRegion createRegion(String name, Location min, Location max, FishType ... allowed) {
		return createRegion(name, min, max, Arrays.asList(allowed));
	}
	
}
