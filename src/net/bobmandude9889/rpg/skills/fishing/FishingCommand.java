package net.bobmandude9889.rpg.skills.fishing;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.bukkit.selections.Selection;

import net.bobmandude9889.rpg.command.Command;
import net.md_5.bungee.api.ChatColor;

public class FishingCommand {
	
	@Command
	public static void fishingRegion(Player player, String[] args) {
		try {
			if (args[0].equalsIgnoreCase("create")) {
				if (player.hasPermission("fr.create")) {
					WorldEditPlugin worldEdit = (WorldEditPlugin) Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");
					Selection selection = worldEdit.getSelection(player);
					if (selection != null) {
						FishingRegionManager.createRegion(args[1], selection.getMinimumPoint(), selection.getMaximumPoint());
						player.sendMessage(ChatColor.GREEN + "Region created");
					} else {
						player.sendMessage(ChatColor.RED + "Please make a selection with WorldEdit.");
					}
				}
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			player.sendMessage("/fr create <name>");
		}
	}
	
}
