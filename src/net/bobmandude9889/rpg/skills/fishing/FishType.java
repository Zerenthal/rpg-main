package net.bobmandude9889.rpg.skills.fishing;

public enum FishType {

	CLOWN((short)2),
	COD((short)0),
	SALMON((short)1),
	PUFFER((short)3);
	
	private short durability;
	
	private FishType(short durability) {
		this.durability = durability;
	}
	
	public short getDurability() {
		return this.durability;
	}
	
}
