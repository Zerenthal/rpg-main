package net.bobmandude9889.rpg.skills.fishing;

import java.util.List;

import org.bukkit.Location;

public class FishingRegion {

	Location min;
	Location max;

	List<FishType> allowed;
	
	protected FishingRegion(Location min, Location max, List<FishType> allowed) {
		this.min = min;
		this.max = max;
		this.allowed = allowed;
	}
	
	public boolean isIn(Location loc) {
		return (loc.getX() > min.getX() && loc.getX() < max.getX()
				&& loc.getY() > min.getY() && loc.getY() < max.getY()
				&& loc.getZ() > min.getZ() && loc.getZ() < max.getZ());
	}
	
}