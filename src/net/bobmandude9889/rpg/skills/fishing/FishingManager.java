package net.bobmandude9889.rpg.skills.fishing;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.bobmandude9889.rpg.player.RPGPlayer;
import net.bobmandude9889.rpg.skills.SkillManager;
import net.bobmandude9889.rpg.skills.SkillType;

public class FishingManager extends SkillManager {
	
	HashMap<String, Long> fishingTime;
	
	public FishingManager(RPGPlayer player, SkillType type) {
		super(player, type);
		fishingTime = new HashMap<String, Long>();
	}

	@Override
	public void openInfoGUI(Player player) {
	}

	@EventHandler
	public void onPickupRod(EntityPickupItemEvent e) {
		if (((Player) (e.getEntity())).equals(player.player) && e.getEntityType().equals(EntityType.PLAYER) && e.getItem().getItemStack().getType().equals(Material.FISHING_ROD)) {
			updateRod(e.getItem().getItemStack());
		}
	}
	
	@EventHandler
	public void onItemHeld(PlayerItemHeldEvent e) {
		ItemStack item = e.getPlayer().getInventory().getItem(e.getNewSlot());
		if (e.getPlayer().equals(player.player) && item != null && item.getType().equals(Material.FISHING_ROD)) {
			updateRod(item);
		}
	}
	
	private void updateRod(ItemStack rod) {
		int level = 0;
		for (String key : config.getConfigurationSection("LURE").getKeys(false)) {
			if (getLevel() >= Integer.parseInt(key)) {
				level = config.getInt("LURE." + key);
			}
		}
		rod.removeEnchantment(Enchantment.LURE);
		if (level != 0)
			rod.addEnchantment(Enchantment.LURE, level);
		
		ItemMeta meta = rod.getItemMeta();
		meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
		
		rod.setItemMeta(meta);
	}
	
	@EventHandler
	public void onFish(PlayerFishEvent e) {
		if (e.getState().equals(PlayerFishEvent.State.FISHING)) {
			if (fishingTime.get(player.player.getUniqueId().toString()) != null) {
				e.getPlayer().sendMessage("" + (double)(System.currentTimeMillis() - fishingTime.get(player.player.getUniqueId().toString())) / (double)1000);
				fishingTime.remove(player.player.getUniqueId().toString());
			}
			fishingTime.put(player.player.getUniqueId().toString(), System.currentTimeMillis());
		}
		if (e.getState().equals(PlayerFishEvent.State.BITE)) {
			e.getPlayer().sendMessage("" + (double)(System.currentTimeMillis() - fishingTime.get(player.player.getUniqueId().toString())) / (double)1000);
			if (fishingTime.get(player.player.getUniqueId().toString()) != null) {
				e.getPlayer().sendMessage("" + (double)(System.currentTimeMillis() - fishingTime.get(player.player.getUniqueId().toString())) / (double)1000);

				fishingTime.remove(player.player.getUniqueId().toString());
				fishingTime.put(player.player.getUniqueId().toString(), System.currentTimeMillis());
			}
			

		}
		if (e.getState().equals(PlayerFishEvent.State.FAILED_ATTEMPT) || e.getState().equals(PlayerFishEvent.State.CAUGHT_FISH) || e.getState().equals(PlayerFishEvent.State.CAUGHT_ENTITY) || e.getState().equals(PlayerFishEvent.State.IN_GROUND)) {
			if (fishingTime.get(player.player.getUniqueId().toString()) != null) {
				e.getPlayer().sendMessage("" + (double)(System.currentTimeMillis() - fishingTime.get(player.player.getUniqueId().toString())) / (double)1000);

				fishingTime.remove(player.player.getUniqueId().toString());
				fishingTime.put(player.player.getUniqueId().toString(), System.currentTimeMillis());
			}

		}
		if (e.getState().equals(PlayerFishEvent.State.CAUGHT_FISH)) {
			List<FishType> types = new CopyOnWriteArrayList<>();
			boolean inRegion = false;
			for (String key : FishingRegionManager.regions.keySet()) {
				FishingRegion region = FishingRegionManager.regions.get(key);
				if (region.isIn(e.getHook().getLocation())) {
					types.addAll(region.allowed);
					inRegion = true;
					break;
				}
			}
			
			e.getPlayer().sendMessage("hook in region:" + inRegion);
			if (inRegion) {
				addXP(10);
			}
			
			for (FishType type : types) {
				if (config.getConfigurationSection(type.name()).getInt("req_level") > getLevel()) {
					types.remove(type);
				}
			}
			
			int i = (int) Math.floor(Math.random() * types.size());
			
			Item caught = (Item) e.getCaught();
			caught.setItemStack(new ItemStack(Material.RAW_FISH, 1, types.get(i).getDurability()));
			e.getPlayer().sendMessage(caught.getName());
		} else if (e.getState().equals(PlayerFishEvent.State.FISHING)) {
			
		}
		e.getPlayer().sendMessage(e.getState().name());
	}
	
}
