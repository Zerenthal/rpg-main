package net.bobmandude9889.rpg.skills;

import org.bukkit.Material;

import net.bobmandude9889.rpg.skills.archery.ArcheryManager;
import net.bobmandude9889.rpg.skills.cooking.CookingManager;
import net.bobmandude9889.rpg.skills.farming.FarmingManager;
import net.bobmandude9889.rpg.skills.fishing.FishingManager;
import net.bobmandude9889.rpg.skills.melee.MeleeManager;
import net.bobmandude9889.rpg.skills.mining.MiningManager;
import net.bobmandude9889.rpg.skills.smithing.SmithingManager;
import net.bobmandude9889.rpg.skills.woodcutting.WoodcuttingManager;

public enum SkillType {

	MELEE(MeleeManager.class, "Melee", Material.IRON_SWORD, 0),
	ARCHERY(ArcheryManager.class, "Archery", Material.BOW, 0),
	SMITHING(SmithingManager.class, "Smithing", Material.ANVIL, 0),
	MINING(MiningManager.class, "Mining", Material.IRON_PICKAXE, 0),
	WOODCUTTING(WoodcuttingManager.class, "Woodcutting", Material.IRON_AXE, 0),
	FARMING(FarmingManager.class, "Farming", Material.IRON_HOE, 0),//not done
	FISHING(FishingManager.class, "Fishing", Material.RAW_FISH, 0),//not done
	COOKING(CookingManager.class, "Cooking", Material.COOKED_BEEF, 0);//not done
	
	public Class<?> skillManager;
	public String name;
	public Material displayType;
	public short displayData;
	
	private SkillType(Class<?> skillManager, String name, Material type, int data){
		this.skillManager = skillManager;
		this.name = name;
		this.displayType = type;
		this.displayData = (short) data;
		
		
	}
	
}
