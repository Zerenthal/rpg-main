package net.bobmandude9889.rpg.skills.melee;

import org.bukkit.Particle;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageModifier;
import org.bukkit.inventory.ItemStack;

import net.bobmandude9889.rpg.player.RPGPlayer;
import net.bobmandude9889.rpg.skills.CombatUtil;
import net.bobmandude9889.rpg.skills.SkillManager;
import net.bobmandude9889.rpg.skills.SkillType;
import net.md_5.bungee.api.ChatColor;

@SuppressWarnings("deprecation")
public class MeleeManager extends SkillManager implements Listener{

	public MeleeManager(RPGPlayer player, SkillType type) {
		super(player, type);
	}

	@Override
	public void openInfoGUI(Player player) {
	}
	
	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e) {
		if(e.getDamager().getType().equals(EntityType.PLAYER) && e.getDamager().equals(player.player)) {
			ItemStack holding = player.player.getInventory().getItemInMainHand();
			int crit = 0;
			if(holding != null) {
				if(config.contains(holding.getType().name())) {
					ConfigurationSection weapon = config.getConfigurationSection(holding.getType().name());
					if(getLevel() >= weapon.getInt("req_level")) {
						crit = CombatUtil.getCrit(weapon.getDouble("crit_chance"), weapon.getInt("crit_max"), player.player);
					} else {
						e.setCancelled(true);
						player.player.sendMessage(ChatColor.RED + "You must be level " + ChatColor.DARK_RED
								+ weapon.getInt("req_level") + ChatColor.RED + " to use that weapon! You are only level " + ChatColor.DARK_RED + getLevel());
					}
				} else {
					e.setCancelled(true);
				}
			}
			
			if(!e.isCancelled()) {
				if(crit > 0) {
					player.player.spawnParticle(Particle.HEART, e.getEntity().getLocation().add(0, 1.3, 0),crit,0.3,0,0.3);
					if(e.getEntity().getType().equals(EntityType.PLAYER)) {
						((Player) e.getEntity()).spawnParticle(Particle.HEART, e.getEntity().getLocation().add(0, 1.3, 0),crit,0.3,0,0.3);
					}
				}
				double damageWithArmor = e.getFinalDamage();
				e.setDamage(DamageModifier.ARMOR, 0);
				e.setDamage(damageWithArmor + crit);
				addXP(Math.round(e.getFinalDamage()));
			}
		}
	}

}
