package net.bobmandude9889.rpg.skills.woodcutting;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;

import net.bobmandude9889.rpg.player.RPGPlayer;
import net.bobmandude9889.rpg.shop.MaterialData;
import net.bobmandude9889.rpg.skills.BlockRespawner;
import net.bobmandude9889.rpg.skills.SkillManager;
import net.bobmandude9889.rpg.skills.SkillType;
import net.md_5.bungee.api.ChatColor;

public class WoodcuttingManager extends SkillManager{

	ConfigurationSection logConfig;
	ConfigurationSection axeConfig;

	public WoodcuttingManager(RPGPlayer player, SkillType type) {
		super(player, type);
		logConfig = config.getConfigurationSection("logs");
		axeConfig = config.getConfigurationSection("axes");
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onBreak(BlockBreakEvent e) {
		String typeStr = e.getBlock().getType().name() + "-" + e.getBlock().getData();
		Player player = e.getPlayer();
		if (e.getPlayer().equals(this.player.player) && logConfig.contains(typeStr)
				&& player.getGameMode().equals(GameMode.SURVIVAL)) {
			e.setCancelled(true);

			if (player.getInventory().getItemInMainHand() != null
					&& axeConfig.contains(player.getInventory().getItemInMainHand().getType().name())) {

				ConfigurationSection axeSect = axeConfig
						.getConfigurationSection(player.getInventory().getItemInMainHand().getType().name());

				if (getLevel() >= axeSect.getDouble("req_level")) {

					ConfigurationSection typeSect = logConfig.getConfigurationSection(typeStr);

					if (getLevel() >= typeSect.getInt("req_level")) {

						Location loc = e.getBlock().getLocation();
						Material type = e.getBlock().getType();
						byte data = e.getBlock().getData();

						int delay = typeSect.getInt("respawn_time");
						double xp = typeSect.getDouble("xp_per_log");

						ItemStack drop = new ItemStack(e.getBlock().getType());
						drop.setDurability((short) (e.getBlock().getData() - 12));

						BlockRespawner.addBlock(loc, new MaterialData(type, (short) data), delay);
						e.getBlock().setType(Material.WOOD);
						e.getBlock().setData((byte) typeSect.getInt("replace_with"));
						this.player.giveItem(drop);
						addXP(xp);
					} else {
						player.sendMessage(ChatColor.RED + "You must be level " + ChatColor.DARK_RED
								+ typeSect.getInt("req_level") + ChatColor.RED + " to chop that log! You are only level " + ChatColor.DARK_RED + getLevel());
					}
				} else {
					player.sendMessage(ChatColor.RED + "You must be level " + ChatColor.DARK_RED
							+ axeSect.getInt("req_level") + ChatColor.RED + " to use that axe! You are only level " + ChatColor.DARK_RED + getLevel());
				}
			}
		}
	}

	@Override
	public void openInfoGUI(Player player) {
		
	}

}
