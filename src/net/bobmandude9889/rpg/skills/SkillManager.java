package net.bobmandude9889.rpg.skills;

import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import net.bobmandude9889.rpg.RPG;
import net.bobmandude9889.rpg.player.RPGPlayer;
import net.bobmandude9889.rpg.util.Title;

public abstract class SkillManager implements Listener {

	public RPGPlayer player;

	public SkillType type;
	
	public ConfigurationSection config;

	public SkillManager(RPGPlayer player, SkillType type) {
		this.player = player;
		this.type = type;
		config = RPG.config.getConfigurationSection(type.name());
	}

	public double getXP() {
		return SkillDataManager.getData(player.player).getXP(type);
	}

	public int getLevel(){
		return getLevelAtXP(getXP());
	}
	
	public void setXP(double xp) {
		int level = getLevel();
		SkillDataManager.getData(player.player).setXP(type, xp);
		int newLevel = getLevel();
		if(newLevel > level) {
			Title title = new Title("&a&lCongrats","&a&l" + type.name + " &7&lskill level &a&l" + newLevel + "!", 2, 2, 2);
			title.send(player.player);
			player.player.playSound(player.player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
		}
	}

	public void addXP(double xp) {
		setXP(SkillDataManager.getData(player.player).getXP(type) + xp);
	}

	public static int getLevelAtXP(double xp) {
		for (int i = 0; i < 100; i++) {
			if (getXPRequired(i + 1) > xp)
				return i;
		}
		return -1;
	}

	public static void main(String[] args) {
		for(int i = 1; i <= 101; i++) {
			System.out.println("level " + i + ": " + (int) getXPRequired(i));
		}
	}
	
	public static double getXPRequired(int level) {
		return Math.ceil(500 * (Math.pow(1.1, level - 1)-1));
	}

	public abstract void openInfoGUI(Player player);

}
