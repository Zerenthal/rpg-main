package net.bobmandude9889.rpg.skills;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.bobmandude9889.rpg.command.Command;
import net.bobmandude9889.rpg.command.CommandEnum;
import net.bobmandude9889.rpg.command.TabComplete;
import net.bobmandude9889.rpg.command.TabCompleteHandler;
import net.bobmandude9889.rpg.command.TabCompleteNode;

public class XPCommand implements TabComplete {

	@Command
	public void XP(CommandSender sender, String[] args) {

		if (args.length == 0) {
			sender.sendMessage(CommandEnum.XP.getUsage());
			return;
		}
		try {
			switch (args[0].toLowerCase()) {
			case "get":
				double xp = SkillDataManager.getData(Bukkit.getPlayer(args[1])).getXP(SkillType.valueOf(args[2].toUpperCase()));
				sender.sendMessage(args[1] + "s " + args[2] + " xp is " + xp + " or level " + SkillManager.getLevelAtXP(xp));
				break;
			case "set":
				Player player = Bukkit.getPlayer(args[1]);
				SkillDataManager.getData(player).setXP(SkillType.valueOf(args[2].toUpperCase()), Double.parseDouble(args[3]));
				sender.sendMessage("Set " + args[1] + "s " + args[2] + " xp to " + args[3] + " or level " + SkillManager.getLevelAtXP(Double.parseDouble(args[3])));
				break;
			case "add":
				player = Bukkit.getPlayer(args[1]);
				SkillType type = SkillType.valueOf(args[2].toUpperCase());
				xp = SkillDataManager.getData(Bukkit.getPlayer(args[1])).getXP(type);
				SkillDataManager.getData(player).setXP(type, Double.parseDouble(args[3]) + xp);
				sender.sendMessage("Set " + args[1] + "s " + args[2] + " xp to " + (Double.parseDouble(args[3]) + xp) + " or level " + SkillManager.getLevelAtXP(Double.parseDouble(args[3]) + xp));
				break;
			default:
				sender.sendMessage(CommandEnum.XP.getUsage());
				return;
			}
		} catch (Exception e) {
			sender.sendMessage(CommandEnum.XP.getUsage());
		}
	}

	public TabCompleteNode tabComplete() {
		TabCompleteNode parent = new TabCompleteNode(new String[] {});
		parent.addChild("set", "add", "get").addChild((player) -> {
			return TabCompleteHandler.getOnlinePlayers();
		}).addChild(TabCompleteHandler.objectsToStrings(SkillType.values()));
		return parent;
	}

}
