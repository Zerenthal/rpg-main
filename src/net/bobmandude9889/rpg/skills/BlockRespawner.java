package net.bobmandude9889.rpg.skills;

import java.io.File;
import java.io.IOException;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import net.bobmandude9889.rpg.RPG;
import net.bobmandude9889.rpg.shop.MaterialData;
import net.bobmandude9889.rpg.util.Util;

public class BlockRespawner {

	static File file;
	static YamlConfiguration config;

	public static void init() {
		file = new File(RPG.dataFolder, "blocks.yml");
		config = YamlConfiguration.loadConfiguration(file);
	}

	public static void addBlock(Location location, MaterialData block, int delay) {
		ConfigurationSection sect = config.createSection(System.currentTimeMillis() + "");
		
		sect.set("material", block.material.name());
		sect.set("data", block.data);
		sect.set("location", Util.locationToString(location));
		sect.set("delay", delay);

		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("deprecation")
	public static void attemptBlockRespawn() {
		for (String key : config.getKeys(false)) {
			ConfigurationSection sect = config.getConfigurationSection(key);
			if ((System.currentTimeMillis() / 1000) - (Long.parseLong(key) / 1000) >= sect.getInt("delay")) {
				Location loc = Util.stringToLocation(sect.getString("location"));
				loc.getBlock().setType(Material.getMaterial(sect.getString("material")));
				loc.getBlock().setData((byte) sect.getInt("data"));
				config.set(key, null);
			}
		}
	}
	
}
