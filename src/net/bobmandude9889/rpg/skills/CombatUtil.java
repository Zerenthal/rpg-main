package net.bobmandude9889.rpg.skills;

import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.bobmandude9889.rpg.RPG;
import net.bobmandude9889.rpg.skills.archery.BowType;

public class CombatUtil {

	public static int getCrit(double chance, int max, Player player) {
		ItemStack[] armor = player.getInventory().getArmorContents();
		ConfigurationSection armorSect = RPG.config.getConfigurationSection("CRIT_ARMOR_CHANCE");
		for(int i = 0; i < armor.length; i++) {
			ItemStack item = armor[i];
			if(item != null) {
				String name = item.getType().name().split("_")[0];
				if(armorSect.contains(name)) {
					chance+=armorSect.getIntegerList(name).get(i);
				}
			}
		}
		Random r = new Random();
		if(r.nextInt(100) <= chance) {
			return r.nextInt(max - 1) + 1;
		} else {
			return 0;
		}
	}
	
	public static BowType getBowType(ItemStack item) {
		if(item == null || !item.getType().equals(Material.BOW))
			return null;
		ItemMeta meta = item.getItemMeta();
		if(meta == null)
			return BowType.BASIC;
		String name = meta.getDisplayName();
		if(name == null)
			return BowType.BASIC;
		name = ChatColor.stripColor(name).toUpperCase().replace(" BOW", "");
		BowType type = BowType.valueOf(name);
		if(type == null)
			return BowType.BASIC;
		return type;
	}
	
}
