package net.bobmandude9889.rpg.skills;

import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import net.bobmandude9889.rpg.command.Command;
import net.bobmandude9889.rpg.gui.GUI;
import net.bobmandude9889.rpg.gui.GUIHandler;
import net.bobmandude9889.rpg.gui.GUITemplate;
import net.bobmandude9889.rpg.gui.action.GUIAction;
import net.bobmandude9889.rpg.gui.event.GUIClickEvent;
import net.bobmandude9889.rpg.player.RPGPlayerManager;
import net.bobmandude9889.rpg.skills.SkillDataManager.SkillData;
import net.bobmandude9889.rpg.util.ItemBuilder;

public class SkillsCommand {

	private static GUITemplate skillsTemp;
	
	private static GUITemplate miningTemp;

	public SkillsCommand() {
		skillsTemp = new GUITemplate(27, "Skill Statistics");

		skillsTemp.setAllowChange(false);

		ItemStack border = new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 7).setName(" ").getItem();
		for (int i = 0; i < 9; i++) {
			skillsTemp.setItem(i, border);
			skillsTemp.setItem(i + 18, border);
		}
		
		miningTemp = new GUITemplate(36, "Mining");
		
		miningTemp.setAllowChange(false);
		
		
	}

	@Command
	public void openGUI(CommandSender sender) {
		if (sender instanceof Player) {
			final Player player = (Player) sender;

			SkillData data = SkillDataManager.getData(player);
			
			GUI gui = GUIHandler.instance.openGUI(player, skillsTemp);

			int playerLevel = SkillManager.getLevelAtXP(data.getXP(SkillType.ARCHERY)) + SkillManager.getLevelAtXP(data.getXP(SkillType.MELEE));
			playerLevel /= 2;
			
			gui.setItem(9, new ItemBuilder(Material.IRON_CHESTPLATE).setName("&aPlayer Level").setLore("&6Level: &e" + playerLevel).getItem());
			
			for (int i = 0; i < SkillType.values().length; i++) {
				final SkillType type = SkillType.values()[i];
				ItemBuilder builder = new ItemBuilder(type.displayType, 1, type.displayData);
				int level = SkillManager.getLevelAtXP(data.getXP(type));
				double xp = data.getXP(type);
				double nextXp = SkillManager.getXPRequired(level + 1);

				builder.setName("&a" + type.name);
				builder.setLore("&6Level: &e" + level + "&7/&e100","&6XP: &e" + xp + "&7/&e" + nextXp, "&6Next Level: &e" + (nextXp - xp));
				gui.setButton(i + 10, builder.getItem(), new GUIAction() {

					@Override
					public void onClick(GUIClickEvent e) {
						SkillManager skillManager = RPGPlayerManager.getPlayer(player).getSkill(type);
						if(skillManager != null) 
							skillManager.openInfoGUI(player);
					}
				});
			}
		}
	}

}
