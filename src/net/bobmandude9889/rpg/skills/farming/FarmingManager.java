package net.bobmandude9889.rpg.skills.farming;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.BlockPosition;

import net.bobmandude9889.rpg.RPG;
import net.bobmandude9889.rpg.player.RPGPlayer;
import net.bobmandude9889.rpg.skills.SkillManager;
import net.bobmandude9889.rpg.skills.SkillType;
import net.bobmandude9889.rpg.util.Util;
import net.md_5.bungee.api.ChatColor;

public class FarmingManager extends SkillManager {

	public FarmingManager(RPGPlayer player, SkillType type) {
		super(player, type);

		// harvest
		ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(RPG.instance, PacketType.Play.Client.BLOCK_DIG) {
			@Override
			public void onPacketReceiving(PacketEvent event) {

				if (event.getPlayer().equals(player.player)) {
					BlockPosition blockPos = event.getPacket().getBlockPositionModifier().read(0);
					@SuppressWarnings("unused")
					Location location = blockPos.toLocation(player.player.getWorld());
				}
			}
		});
	}

	@Override
	public void openInfoGUI(Player player) {
		// TODO Auto-generated method stub

	}

	@EventHandler
	public void onPlant(BlockPlaceEvent e) {
		if (e.getPlayer().equals(player.player)) {
			Location location = e.getBlock().getLocation();
			PlayerInventory inventory = player.player.getInventory();
			ItemStack hand = inventory.getItemInMainHand();

			if (hand == null)
				hand = inventory.getItemInOffHand();

			if (hand == null)
				return;

			SeedType seedType = SeedType.valueOf(hand.getType());

			if (seedType == null) {
				hand = inventory.getItemInOffHand();
				seedType = SeedType.valueOf(hand.getType());
			}

			if (seedType == null)
				return;
			
			e.setCancelled(true);

			if(config.getInt(seedType + ".req_level") > getLevel()) {
				player.player.sendMessage(ChatColor.DARK_RED + "You must be level " + ChatColor.RED + getLevel() + ChatColor.DARK_RED + " to use that seed.");
				return;
			}
			
			int limit = 0;
			int maxLevel = 0;
			
			for(String key : config.getConfigurationSection("crop_limits").getKeys(false)) {
				int level = Integer.parseInt(key);
				if(level <= getLevel() && level > maxLevel) {
					maxLevel = level;
					limit = config.getInt("crop_limits." + key);
				}
			}
			
			if(SeedManager.getSeeds(player.player).size() >= limit) {
				player.player.sendMessage(ChatColor.DARK_RED + "You may only have " + ChatColor.RED + limit + ChatColor.DARK_RED + " seed" + (limit == 1 ? "" : "s") + " planted at once.");
				return;
			}
			
			if (SeedManager.addSeed(location, player.player, seedType)) {
				for(Seed seed : SeedManager.getSeeds(player.player)) {
					Bukkit.getOnlinePlayers().forEach((player)->seed.removeBlock(player));
				}
				if (!e.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
					if (hand.getAmount() == 1) {
						inventory.setItemInMainHand(null);
					} else {
						hand.setAmount(hand.getAmount() - 1);
					}
				}
			}
		}
	}

	@EventHandler
	public void onJoin(ChunkLoadEvent e) {
		List<Seed> seeds = SeedManager.getSeeds(player.player);
		for (Seed seed : seeds) {
			if (Util.isIn(seed.location, e.getChunk())) {
				seed.sendToPlayer(player.player);
			}
		}
	}

	@EventHandler
	public void onTrample(PlayerInteractEvent e) {
		if (e.getAction().equals(Action.PHYSICAL)) {
			Block block = e.getClickedBlock();

			if (block == null)
				return;

			if (block.getType().equals(Material.SOIL)) {
				e.setUseInteractedBlock(org.bukkit.event.Event.Result.DENY);
				e.setCancelled(true);
			}
		}
	}

}