package net.bobmandude9889.rpg.skills.farming;

import org.bukkit.Material;

import com.comphenix.protocol.wrappers.WrappedBlockData;

public enum SeedType {
	
	WHEAT(Material.SEEDS, new BlockDrop[] {new BlockDrop(Material.WHEAT, 2, 4), new BlockDrop(Material.SEEDS, 1, 2)}, 3, 5, createBlock(Material.CROPS, 0), createBlock(Material.CROPS, 1), createBlock(Material.CROPS, 2), createBlock(Material.CROPS, 3), createBlock(Material.CROPS, 4), createBlock(Material.CROPS, 5), createBlock(Material.CROPS, 6), createBlock(Material.CROPS, 7)),
	CARROT(Material.CARROT_ITEM, new BlockDrop(Material.CARROT_ITEM, 2, 4), 3, 5, createBlock(Material.CARROT, 0), createBlock(Material.CARROT, 1), createBlock(Material.CARROT, 2), createBlock(Material.CARROT, 3), createBlock(Material.CARROT, 4), createBlock(Material.CARROT, 5), createBlock(Material.CARROT, 6), createBlock(Material.CARROT, 7)),
	POTATO(Material.POTATO_ITEM, new BlockDrop(Material.POTATO_ITEM, 2, 4), 3, 5, createBlock(Material.POTATO, 0), createBlock(Material.POTATO, 1), createBlock(Material.POTATO, 2), createBlock(Material.POTATO, 3), createBlock(Material.POTATO, 4), createBlock(Material.POTATO, 5), createBlock(Material.POTATO, 6), createBlock(Material.POTATO, 7)),

	PUMPKIN(Material.PUMPKIN_SEEDS, new BlockDrop(Material.PUMPKIN, 1, 1), 3, 5, createBlock(Material.PUMPKIN_STEM, 0), createBlock(Material.PUMPKIN_STEM, 1), createBlock(Material.PUMPKIN_STEM, 2), createBlock(Material.PUMPKIN_STEM, 3), createBlock(Material.PUMPKIN_STEM, 4), createBlock(Material.PUMPKIN_STEM, 5), createBlock(Material.PUMPKIN_STEM, 6), createBlock(Material.PUMPKIN_STEM, 7), createBlock(Material.PUMPKIN, 0)),
	MELON(Material.MELON_SEEDS, new BlockDrop(Material.MELON, 2, 4), 3, 5, createBlock(Material.MELON_STEM, 0), createBlock(Material.MELON_STEM, 1), createBlock(Material.MELON_STEM, 2), createBlock(Material.MELON_STEM, 3), createBlock(Material.MELON_STEM, 4), createBlock(Material.MELON_STEM, 5), createBlock(Material.MELON_STEM, 6), createBlock(Material.MELON_STEM, 7), createBlock(Material.MELON_BLOCK, 0)),

	BEETROOT(Material.BEETROOT_SEEDS, new BlockDrop(Material.BEETROOT, 2, 4), 3, 5, createBlock(Material.BEETROOT_BLOCK, 0), createBlock(Material.BEETROOT_BLOCK, 1), createBlock(Material.BEETROOT_BLOCK, 2), createBlock(Material.BEETROOT_BLOCK, 3));

	int low;
	int high;
	WrappedBlockData[] states;
	Material seedItem;
	BlockDrop[] drops;

	private SeedType(Material seedItem, BlockDrop drop, int low, int high, WrappedBlockData... states){
		this.seedItem = seedItem;
		this.low = low;
		this.high = high;
		this.states = states;
		this.drops = new BlockDrop[] {drop};
	}
	
	private SeedType(Material seedItem, BlockDrop[] drops, int low, int high, WrappedBlockData... states){
		this.seedItem = seedItem;
		this.low = low;
		this.high = high;
		this.states = states;
		this.drops = drops;
	}
	
	private static WrappedBlockData createBlock(Material type, int data){
		WrappedBlockData wrappedBlock = WrappedBlockData.createData(type);
		wrappedBlock.setData(data);
		return wrappedBlock;
	}
	
	public Material getSeedItem() {
		return seedItem;
	}

	public int getLow() {
		return low;
	}

	public int getHigh() {
		return high;
	}

	public WrappedBlockData[] getStates() {
		return states;
	}

	public static SeedType valueOf(Material material) {
		for(SeedType type : values()) {
			if(type.seedItem.equals(material))
				return type;
		}
		return null;
	}
	
}
