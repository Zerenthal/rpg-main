package net.bobmandude9889.rpg.skills.farming;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitScheduler;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.reflect.StructureModifier;
import com.comphenix.protocol.wrappers.WrappedBlockData;

import net.bobmandude9889.rpg.RPG;
import net.bobmandude9889.rpg.player.RPGPlayer;
import net.bobmandude9889.rpg.player.RPGPlayerManager;
import net.bobmandude9889.rpg.skills.SkillManager;
import net.bobmandude9889.rpg.skills.SkillType;
import net.bobmandude9889.rpg.util.Util;

public class SeedManager {
	static File file;
	static YamlConfiguration seeds;
	static int removalTime = 300;

	public static void init() {
		file = new File(RPG.dataFolder, "seeds.yml");
		try {
			file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		seeds = YamlConfiguration.loadConfiguration(file);

		BukkitScheduler scheduler = Bukkit.getScheduler();
		scheduler.scheduleSyncRepeatingTask(RPG.instance, new Runnable() {
			@Override
			public void run() {
				for (Player player : Bukkit.getOnlinePlayers()) {
					List<Seed> seedList = getSeeds(player);
					
					for(Seed seed : seedList) {
						if(System.currentTimeMillis() - seed.timeStamp >= seed.delay * 1000 && seed.type.getStates().length > seed.state + 1) {
							updateSeed(player, seed.state + 1, seed.location);
						}
						
						if(System.currentTimeMillis() - seed.timeStamp >= RPG.config.getInt("FARMING.crop_death_time") * 1000) {
							removeSeed(player, seed.location);
						}
					}
				}
			}
		}, 0L, 20L);
		
		ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(RPG.instance, PacketType.Play.Server.BLOCK_CHANGE, PacketType.Play.Server.MAP_CHUNK, PacketType.Play.Client.BLOCK_DIG) {
			@Override
			public void onPacketSending(PacketEvent event) {
				if(event.getPacketType().equals(PacketType.Play.Server.BLOCK_CHANGE)) {
					PacketContainer packet = event.getPacket();
					Location location = packet.getBlockPositionModifier().read(0).toLocation(event.getPlayer().getWorld());
					boolean hasSeed = false;
					for(Seed seed : getSeeds(event.getPlayer())) {
						if(seed.location.equals(location)) {
							packet.getBlockData().write(0, seed.type.states[seed.state]);
							hasSeed = true;
						}
					}
					
					for(Player player : Bukkit.getOnlinePlayers()) {
						if(player != event.getPlayer()) {
							for(Seed seed : getSeeds(player)) {
								if(seed.location.equals(location) && !hasSeed) {
									packet.getBlockData().write(0, WrappedBlockData.createData(Material.AIR));
								}
							}
						}
					}
				} else if (event.getPacketType().equals(PacketType.Play.Server.MAP_CHUNK)) {
					Bukkit.getScheduler().scheduleSyncDelayedTask(RPG.instance, () -> {
						Player player = event.getPlayer();
						List<Seed> seedList = getSeeds(player);
						StructureModifier<Integer> ints = event.getPacket().getIntegers();
						Chunk chunk = player.getWorld().getChunkAt(ints.read(0), ints.read(1));
						for(Seed seed : seedList) {
							if(Util.isIn(seed.location, chunk)) {
								seed.sendToPlayer(player);
							}
						}
					}, 20l);
				}
			}
			
			@Override
			public void onPacketReceiving(PacketEvent event) {
				Player player = event.getPlayer();
				RPGPlayer rPlayer = RPGPlayerManager.getPlayer(player);
				PacketContainer packet = event.getPacket();
				Location location = packet.getBlockPositionModifier().read(0).toLocation(player.getWorld());
				List<Seed> seedList = getSeeds(player);
				for(Seed seed : seedList) {
					if(seed.location.equals(location)) {
						removeSeed(player, location);
						
						if(seed.state == seed.type.states.length - 1) {
							for(BlockDrop drop : seed.type.drops) {
								ItemStack item = new ItemStack(drop.material, new Random().nextInt(drop.high - drop.low) + drop.low);
								rPlayer.giveItem(item);
							}
							SkillManager farming = rPlayer.getSkill(SkillType.FARMING);
							Bukkit.getScheduler().scheduleSyncDelayedTask(RPG.instance, () -> farming.addXP(farming.config.getInt(seed.type + ".xp_gained")));
						} else {
							rPlayer.giveItem(new ItemStack(seed.type.seedItem, 1));
						}
						break;
					}
				}
			}
		});
	}
	
	public static void save() {
		try {
			seeds.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static List<Seed> getSeeds(Player player) {
		List<Seed> seedList = new ArrayList<>();
		
		String playerKey = player.getUniqueId().toString();
		if(!seeds.contains(playerKey))
			seeds.createSection(playerKey);
		ConfigurationSection playerSect = seeds.getConfigurationSection(playerKey);
		
		for(String timeKey : playerSect.getKeys(false)) {
			ConfigurationSection seedSect = playerSect.getConfigurationSection(timeKey);
			Seed seed = new Seed(
					Long.parseLong(timeKey), 
					SeedType.valueOf(seedSect.getString("seed-type")), 
					seedSect.getInt("growth"), 
					Util.stringToLocation(seedSect.getString("location")),
					seedSect.getInt("delay")
				);
			seedList.add(seed);
		}
		
		return seedList;
	}

	public static void setSeeds(Player player, List<Seed> seedList) {
		String playerKey = player.getUniqueId().toString();
		if(!seeds.contains(playerKey))
			seeds.createSection(playerKey);
		ConfigurationSection playerSect = seeds.getConfigurationSection(playerKey);
		
		for(String key : playerSect.getKeys(false)) {
			playerSect.set(key, null);
		}
		
		for(Seed seed : seedList) {
			ConfigurationSection seedSect = playerSect.createSection(seed.timeStamp + "");
			seedSect.set("seed-type", seed.type.name());
			seedSect.set("growth", seed.state);
			seedSect.set("location", Util.locationToString(seed.location));
			seedSect.set("delay", seed.delay);
		}
	}
	
	/*
	 * player-id:
	 *     'timestamp':
	 *         seed-type: 'WHEAT'
	 *         growth: 0
	 *         location: location string
	 *         delay: 3
	 * 
	 */

	public static boolean addSeed(Location location, Player player, SeedType seedType) {
		List<Seed> seedList = getSeeds(player);
		
		int delay = new Random().nextInt(seedType.high - seedType.low) + seedType.low;
		seedList.add(new Seed(System.currentTimeMillis(), seedType, 0, location, delay));
		setSeeds(player, seedList);
		save();
		return true;
	}
	
	public static void updateSeed(Player player, int state, Location location) {
		List<Seed> seedList = getSeeds(player);
		
		Seed send = null;
		
		for(Seed seed : seedList) {
			if(seed.location.equals(location)) {
				seed.state = state;
				seed.delay = new Random().nextInt(seed.type.high - seed.type.low) + seed.type.low;
				seed.timeStamp = System.currentTimeMillis();
				send = seed;
				break;
			}
		}
		setSeeds(player, seedList);
		
		if(send != null)
			send.sendToPlayer(player);
		
		save();
	}
	
	public static void removeSeed(Player player, Location location) {
		List<Seed> seedList = getSeeds(player);
		Seed remove = null;
		
		for(Seed seed : seedList) {
			if(seed.location.equals(location)) {
				remove = seed;
				break;
			}
		}
		
		if(remove != null) {
			seedList.remove(remove);
		}
		
		setSeeds(player, seedList);
		
		if(remove != null) {
			remove.removeBlock(player);
		}
		
		save();
	}
	
}
