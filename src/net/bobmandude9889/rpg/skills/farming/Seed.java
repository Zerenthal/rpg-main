package net.bobmandude9889.rpg.skills.farming;

import java.lang.reflect.InvocationTargetException;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.BlockPosition;
import com.comphenix.protocol.wrappers.WrappedBlockData;

public class Seed {

	public long timeStamp;
	public SeedType type;
	public int state;
	public Location location;
	public int delay;
	
	public Seed(long timeStamp, SeedType type, int state, Location location, int delay) {
		this.timeStamp = timeStamp;
		this.type = type;
		this.state = state;
		this.location = location;
		this.delay = delay;
	}
	
	@Override
	public String toString() {
		return type.name() + ":" + state + "\ntime:" + timeStamp;
	}
	
	public void sendToPlayer(Player player) {
		PacketContainer packet = new PacketContainer(PacketType.Play.Server.BLOCK_CHANGE);
		packet.getBlockPositionModifier().write(0, new BlockPosition(location.toVector()));
		try {
			ProtocolLibrary.getProtocolManager().sendServerPacket(player, packet);
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}
	
	public void removeBlock(Player player) {
		PacketContainer packet = new PacketContainer(PacketType.Play.Server.BLOCK_CHANGE);
		packet.getBlockPositionModifier().write(0, new BlockPosition(location.toVector()));
		packet.getBlockData().write(0, WrappedBlockData.createData(Material.AIR));
		try {
			ProtocolLibrary.getProtocolManager().sendServerPacket(player, packet);
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}
	
}
