package net.bobmandude9889.rpg.skills.cooking;

import net.bobmandude9889.rpg.RPG;
import net.bobmandude9889.rpg.gui.GUITemplate;
import net.bobmandude9889.rpg.player.RPGPlayer;
import net.bobmandude9889.rpg.skills.SkillManager;
import net.bobmandude9889.rpg.skills.SkillType;
import net.bobmandude9889.rpg.util.ItemBuilder;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;

public class CookingManager extends SkillManager implements Listener {

	public static GUITemplate gui;
	
	public CookingManager(RPGPlayer player, SkillType type) {
		super(player, type);
	}

	@Override
	public void openInfoGUI(Player player) {
	}

	@EventHandler
	public void onFurnaceClick(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if ((p.hasPermission("cooking.admin") && p.isSneaking()) || !e.getClickedBlock().getType().equals(Material.SPONGE)) {
			return;
		}
		e.setCancelled(true);
		
		gui = new GUITemplate(5, "Cooking", InventoryType.HOPPER);
		gui.setButton(1, new ItemBuilder(Material.BREAD).setName("&rCooking").setLore("Use recipes to create food.").getItem(), event -> {
			openCookingGUI(p);
		});

		gui.setButton(3, new ItemBuilder(Material.FURNACE).setName("&rBaking").setLore("Heat up food to make it edible.").getItem(), event -> {
			GUITemplate categoryGui = new GUITemplate(5, "Baking", InventoryType.HOPPER);
			categoryGui.open(p);
		});
		
		gui.open(p);
		
	}
	
	private void openCookingGUI(Player p) {
		GUITemplate cookingGUI = new GUITemplate(5, "Cooking", InventoryType.HOPPER);
		
		cookingGUI.setButton(0, new ItemBuilder(Material.WORKBENCH).setName("&rRecipe").getItem(), (clickEvent) -> {
			
			GUITemplate recipeGUI = new GUITemplate(9, "Cooking", InventoryType.DROPPER);
			
			recipeGUI.setOnClose(event3 -> {
				event3.setCanceled(true);//remind to remove this
				RPG.instance.getServer().getScheduler().runTask(RPG.instance, ()->{
					openCookingGUI(p);
				});
				
			});
			recipeGUI.open(p);
		});
		
		cookingGUI.setButton(2, new ItemBuilder(Material.COAL).setName("&rFuel").getItem(), (event2) -> {
			
			GUITemplate fuelGUI = new GUITemplate(5, "Fuel", InventoryType.HOPPER);
			
			fuelGUI.setOnClose(event3 -> {
				event3.setCanceled(true);//remind to remove this
				RPG.instance.getServer().getScheduler().runTask(RPG.instance, ()->{
					openCookingGUI(p);
				});
				
			});
			fuelGUI.open(p);
		});
		
		cookingGUI.setButton(4, new ItemBuilder(Material.BREAD).setName("&rBaked").getItem(), (event2) -> {
			
			GUITemplate bakedGUI = new GUITemplate(9, "Baked");
			
			bakedGUI.setOnClose(event3 -> {
				event3.setCanceled(true);//remind to remove this
				RPG.instance.getServer().getScheduler().runTask(RPG.instance, ()->{
					openCookingGUI(p);
				});
				
			});
			bakedGUI.open(p);
		});
		cookingGUI.open(p);
	}
}
