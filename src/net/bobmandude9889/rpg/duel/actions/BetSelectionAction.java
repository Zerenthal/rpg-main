package net.bobmandude9889.rpg.duel.actions;

import java.math.BigInteger;

import net.bobmandude9889.rpg.duel.DuelCommand;
import net.bobmandude9889.rpg.duel.DuelRequest;
import net.bobmandude9889.rpg.gui.action.GUIAction;
import net.bobmandude9889.rpg.gui.event.GUIClickEvent;
import net.bobmandude9889.rpg.player.RPGPlayer;
import net.bobmandude9889.rpg.player.RPGPlayerManager;

public class BetSelectionAction implements GUIAction{
	
	BigInteger bet;
	DuelRequest request;
	
	public BetSelectionAction(BigInteger bet, DuelRequest request) {
		this.bet = bet;
		this.request = request;
	}

	@Override
	public void onClick(GUIClickEvent e) {
		try{
		if(e.leftClick)
			request.bet = request.bet.add(bet);
		if(e.rightClick)
			request.bet = request.bet.subtract(bet);
		
		if(request.bet.compareTo(BigInteger.ZERO) == -1)
			request.bet = BigInteger.ZERO;
		
		RPGPlayer rpgSender = RPGPlayerManager.getPlayer(request.sender);
		RPGPlayer rpgReceiver = RPGPlayerManager.getPlayer(request.receiver);
		
		if(!(rpgSender.hasEnough(request.bet) && rpgReceiver.hasEnough(request.bet))) {
			BigInteger min = rpgSender.getCoins();
			if(!rpgReceiver.hasEnough(min)) {
				min = rpgReceiver.getCoins();
			}
			
			request.bet = min;
		}
		
		e.gui.setName(DuelCommand.getTitle(request));
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
	

}
