package net.bobmandude9889.rpg.duel.actions;

import net.bobmandude9889.rpg.duel.DuelCommand;
import net.bobmandude9889.rpg.duel.DuelKit;
import net.bobmandude9889.rpg.duel.DuelRequest;
import net.bobmandude9889.rpg.gui.action.GUIAction;
import net.bobmandude9889.rpg.gui.event.GUIClickEvent;

public class KitSelectionAction implements GUIAction{

	DuelKit kit;
	DuelRequest request;
	
	public KitSelectionAction(DuelKit kit, DuelRequest request) {
		this.kit = kit;
		this.request = request;
	}
	
	@Override
	public void onClick(GUIClickEvent e) {
		request.kit = kit;
		e.gui.setName(DuelCommand.getTitle(request));
	}

}
