package net.bobmandude9889.rpg.duel;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionType;

import net.bobmandude9889.rpg.util.ItemBuilder;

public enum DuelKit {

	//@formatter:off
	BOXING(ChatColor.AQUA, "Boxing", "&aBare fist fight.\n&aHealth potions.", new ItemStack[] { 
			health2(), 
			health2(), 
			health2(), 
			health2(), 
			health2() 
			}, new ItemStack[] {
	}), 
	
	NORMAL(ChatColor.AQUA, "Normal Duel", "&aDiamond armor and sword.\n&aNo potions.", new ItemStack[] { 
			new ItemStack(Material.DIAMOND_SWORD) 
			}, new ItemStack[] { 
					new ItemStack(Material.DIAMOND_BOOTS),  
					new ItemStack(Material.DIAMOND_LEGGINGS),
					new ItemStack(Material.DIAMOND_CHESTPLATE), 
					new ItemStack(Material.DIAMOND_HELMET) 
	}),
	
	STRONG(ChatColor.AQUA, "Strong Duel", "&aProtection 4 diamond armor.\n&aSharpness 4 diamond sword.\n&aHealth potions.", new ItemStack[] {
			new ItemBuilder(Material.DIAMOND_SWORD).addEnchantment(Enchantment.DAMAGE_ALL, 3).addEnchantment(Enchantment.DURABILITY, 2).getItem(),
			health2(),
			health2(),
			health2(),
			health2(),
			health2(),
			health2(),
			health2(),
			health2(),
			health2(),
			health2(),
			health2(),
			health2(),
			health2(),
			health2(),
			health2(),
			health2(),
			health2(),
	}, new ItemStack[] {
			new ItemBuilder(Material.DIAMOND_BOOTS).addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3).getItem(),
			new ItemBuilder(Material.DIAMOND_LEGGINGS).addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3).getItem(),
			new ItemBuilder(Material.DIAMOND_CHESTPLATE).addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3).getItem(),
			new ItemBuilder(Material.DIAMOND_HELMET).addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3).getItem()
	});
	//@formatter:on

	private static ItemStack health2() {
		return new ItemBuilder(Material.SPLASH_POTION).setPotion(PotionType.INSTANT_HEAL, false, true).getItem();
	}

	ChatColor nameColor;
	String name;
	String description;
	ItemStack[] contents;
	ItemStack[] armor;

	private DuelKit(ChatColor nameColor, String name, String description, ItemStack[] contents, ItemStack[] armor) {
		this.nameColor = nameColor;
		this.name = name;
		this.description = description;
		
		this.contents = new ItemStack[36];
		for(int i = 0; i < contents.length; i++) {
			this.contents[i] = contents[i];
		}
		
		this.armor = new ItemStack[4];
		for(int i = 0; i < armor.length; i++) {
			this.armor[i] = armor[i];
		}
	}
	
	public DuelKit getByName(String name) {
		for(DuelKit kit : values()) {
			if(kit.name.equalsIgnoreCase(name))
				return kit;
		}
		return null;
	}
	
}
