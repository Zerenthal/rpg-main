package net.bobmandude9889.rpg.duel;

import java.math.BigInteger;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import net.bobmandude9889.rpg.RPG;
import net.bobmandude9889.rpg.player.RPGPlayer;
import net.bobmandude9889.rpg.player.RPGPlayerManager;
import net.bobmandude9889.rpg.util.tellraw.TellrawBuilder;
import net.bobmandude9889.rpg.util.tellraw.TellrawChatText;
import net.bobmandude9889.rpg.util.tellraw.TellrawText;

public class DuelRequest {

	public Player sender;
	public Player receiver;
	public DuelKit kit;
	public BigInteger bet;
	public ItemStack[] contents;
	
	public boolean senderAccepted = false;
	public boolean receiverAccepted = false;
	
	public boolean confirmed = false;
	
	public long timeSent = Long.MAX_VALUE;

	public DuelRequest(Player sender, Player receiver, DuelKit kit, BigInteger bet) {
		this.sender = sender;
		this.receiver = receiver;
		this.kit = kit;
		this.bet = bet;
	}
	
	public void sendToBoth(String message) {
		receiver.sendMessage(message);
		sender.sendMessage(message);
	}
	
	public void sendMessage() {
		timeSent = System.currentTimeMillis();
		
		TellrawBuilder builder = new TellrawBuilder();
		builder.addText(new TellrawChatText(sender.getName()).setColor(ChatColor.AQUA).setColor(ChatColor.BOLD));
		builder.addText(new TellrawChatText(" wants to duel you in a ").setColor(ChatColor.GRAY));
		
		String[] descRaw = kit.description.split("\n");
		TellrawText[] text = new TellrawText[descRaw.length];
		for(int i = 0; i < descRaw.length; i++) {
			text[i] = new TellrawText(descRaw[i] + (i != descRaw.length - 1 ? "\n" : "")).setColor(ChatColor.AQUA);
		}
		
		builder.addText(new TellrawChatText(kit.name.toLowerCase()).setColor(ChatColor.GREEN).setColor(ChatColor.BOLD).setColor(ChatColor.UNDERLINE).setShowTextOnHover(text));
		
		builder.addText(new TellrawChatText(" match for ").setColor(ChatColor.GRAY));
		builder.addText(new TellrawChatText("$" + bet).setColor(ChatColor.GREEN));
		builder.addText(new TellrawChatText("!").setColor(ChatColor.GRAY));
		
		builder.sendToPlayer(receiver);
		builder = new TellrawBuilder();
		
		builder.addText(new TellrawChatText("You have ").setColor(ChatColor.GRAY));
		builder.addText(new TellrawChatText(RPG.config.getInt("request_timeout") + " seconds").setColor(ChatColor.GREEN));
		builder.addText(new TellrawChatText(" to accept.").setColor(ChatColor.GRAY));
		
		builder.sendToPlayer(receiver);
		builder = new TellrawBuilder();
		
		builder.addText(new TellrawChatText("ACCEPT").setColor(ChatColor.GREEN).setColor(ChatColor.BOLD).setRunCommandOnClick("/duel accept " + sender.getName()).setShowTextOnHover(new TellrawText("Click to accept!").setColor(ChatColor.GREEN)));
		builder.addText(new TellrawChatText(" DENY").setColor(ChatColor.RED).setColor(ChatColor.BOLD).setRunCommandOnClick("/duel deny " + sender.getName()).setShowTextOnHover(new TellrawText("Click to deny!").setColor(ChatColor.RED)));
		
		builder.sendToPlayer(receiver);
	}
	
	public void expire() {
		sender.sendMessage(ChatColor.RED + receiver.getName() + ChatColor.GRAY + " did not accept your request in time!");
		receiver.sendMessage(ChatColor.GRAY + "The duel request from " + ChatColor.RED + sender.getName() + ChatColor.GRAY + " has expired.");
	}
	
	public void returnItems() {
		RPGPlayer rSender = RPGPlayerManager.getPlayer(sender);
		RPGPlayer rReceiver = RPGPlayerManager.getPlayer(receiver);
		for(int i = 0; i < contents.length; i++) {
			ItemStack item = contents[i];
			if(item != null)
				item = item.clone();
			if((i % 9) < 4) {
				rSender.giveItem(item);
			} else if((i % 9 > 4)) {
				rReceiver.giveItem(item);
			}
		}
	}
	
}
