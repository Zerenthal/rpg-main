package net.bobmandude9889.rpg.duel;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import net.bobmandude9889.rpg.RPG;
import net.bobmandude9889.rpg.gui.GUIHandler;
import net.bobmandude9889.rpg.util.Util;
import net.md_5.bungee.api.ChatColor;

public class DuelManager implements Listener{

	private static HashMap<Player, DuelRequest> requests;
	public static List<Arena> arenas;
	
	private static File arenaFile;
	private static YamlConfiguration arenaConfig;
	
	private static HashMap<Player, Long> lastRequest;
	
	private static List<DuelRequest> queue;
	
	public static void init() {
		Bukkit.getPluginManager().registerEvents(new DuelManager(), RPG.instance);
		requests = new HashMap<Player, DuelRequest>();
		arenas = new ArrayList<Arena>();
		arenaFile = new File(RPG.dataFolder, "arenas.yml");
		try {
			arenaFile.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		arenaConfig = YamlConfiguration.loadConfiguration(arenaFile);
		lastRequest = new HashMap<Player, Long>();
		queue = new CopyOnWriteArrayList<DuelRequest>();
		Bukkit.getScheduler().scheduleSyncRepeatingTask(RPG.instance, new Runnable() {
			@Override
			public void run() {
				checkForExpiredRequests();
				checkQueue();
			}
		}, 20, 20);
		loadArenas();
	}
	
	private static void checkForExpiredRequests() {
		for(Player player : requests.keySet()) {
			DuelRequest request = requests.get(player);
			if((System.currentTimeMillis() - request.timeSent) / 1000 >= RPG.config.getInt("request_timeout")) {
				request.expire();
				requests.remove(player);
			}
		}
	}
	
	public static void setLastRequest(Player player) {
		lastRequest.put(player, System.currentTimeMillis());
	}
	
	public static long getLastRequest(Player player) {
		return lastRequest.get(player);
	}
	
	public static boolean canSendRequest(Player player) {
		if(!lastRequest.containsKey(player))
			return true;
		return (System.currentTimeMillis() - lastRequest.get(player)) / 1000 >= RPG.config.getInt("duel_request_cooldown");
	}
	
	public static int getRemainingRequestCooldown(Player player) {
		if(!lastRequest.containsKey(player))
			return 0;
		return RPG.config.getInt("duel_request_cooldown") - (int) ((System.currentTimeMillis() - lastRequest.get(player)) / 1000);
	}
	
	public static Arena getArena(Player player) {
		for(Arena arena : arenas) {
			for(DuelPlayer dPlayer : arena.players) {
				if(dPlayer != null && dPlayer.player.equals(player))
					return arena;
			}
		}
		return null;
	}
	
	public static void saveArenas() {
		for(String key : arenaConfig.getKeys(false)) {
			arenaConfig.set(key, null);
		}
		
		for(int i = 0; i < arenas.size(); i++) {
			Arena arena = arenas.get(i);
			ConfigurationSection section = arenaConfig.createSection(i + "");
			section.set("spawn1", Util.locationToString(arena.spawn1));
			section.set("spawn2", Util.locationToString(arena.spawn2));
		}
		
		try {
			arenaConfig.save(arenaFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void loadArenas() {
		for(String key : arenaConfig.getKeys(false)) {
			ConfigurationSection section = arenaConfig.getConfigurationSection(key);
			Arena arena = new Arena(Util.stringToLocation(section.getString("spawn1")), Util.stringToLocation(section.getString("spawn2")));
			arenas.add(arena);
		}
	}
	
	public static void registerArena(Arena arena) {
		arenas.add(arena);
		saveArenas();
	}
	
	public static void removeArena(int id) {
		arenas.remove(id);
		saveArenas();
	}
	
	public static void joinQueue(DuelRequest request) {
		if(!queue.contains(request))
			queue.add(request);
		String message = ChatColor.GRAY + "You are in position " + ChatColor.GREEN + queue.size() + ChatColor.GRAY + " in the queue.";
		request.sender.sendMessage(message);
		request.receiver.sendMessage(message);
	}
	
	public static void leaveQueue(Player player) {
		for(DuelRequest request : queue) {
			if(request.sender.equals(player) || request.receiver.equals(player)) {
				queue.remove(request);
				request.returnItems();
				Player other = request.sender.equals(player) ? request.receiver : request.sender;
				other.sendMessage(ChatColor.RED + player.getName() + ChatColor.GRAY + " canceled the duel! You are no longer in the queue.");
				player.sendMessage(ChatColor.RED + "You have left the queue!");
			} 
		}
	}
	
	public static boolean isInQueue(Player player) {
		for(DuelRequest request : queue) {
			if(request.sender.equals(player) || request.receiver.equals(player))
				return true;
		}
		return false;
	}
	
	public static void checkQueue() {
		boolean changed = false;
		for(final Arena arena : arenas) {
			if(arena.available && queue.size() > 0) {
				DuelRequest request = queue.get(0);
				arena.setup(request);
				request.sendToBoth(ChatColor.GRAY + "It's your turn to duel! The duel will start in " + ChatColor.GREEN + "5 seconds.");
				Bukkit.getScheduler().scheduleSyncDelayedTask(RPG.instance, new Runnable() {
					@Override
					public void run() {
						arena.startDuel();
					}
				}, 100);
				queue.remove(0);
				changed = true;
			}
		}
		if(changed)
			sendQueuePosMessage();
	}
	
	public static void sendQueuePosMessage() {
		for(int i = 0; i < queue.size(); i++) {
			DuelRequest request = queue.get(i);
			String message = ChatColor.GRAY + "You are in position " + ChatColor.GREEN + (i + 1) + ChatColor.GRAY + " in the queue.";
			request.sendToBoth(message);
		}
	}
	
	public static void sendRequest(DuelRequest request) {
		requests.put(request.sender, request);
		request.sendMessage();
	}
	
	public static DuelRequest getRequest(Player sender) {
		if(!requests.containsKey(sender))
			return null;
		return requests.get(sender);
	}
	
	public static void cancelRequest(Player sender, boolean message) {
		DuelRequest request = requests.get(sender);
		if(request != null && request.confirmed) {
			if(message) {
				request.receiver.sendMessage(ChatColor.RED + sender.getName() + ChatColor.GRAY + " has canceled their duel request.");
				request.sender.sendMessage(ChatColor.RED + "Your request has been canceled.");
			}
			request.confirmed = false;
			GUIHandler.instance.closeGUI(sender);
			GUIHandler.instance.closeGUI(request.receiver);
		}
		requests.remove(sender);
	}
	
	public static void denyRequest(Player sender) {
		DuelRequest request = requests.get(sender);
		if(request != null && request.confirmed) {
			request.confirmed = false;
			request.receiver.sendMessage(ChatColor.RED + "Request denied.");
			request.sender.sendMessage(ChatColor.RED + request.receiver.getName() + ChatColor.GRAY + " denied your request.");
			GUIHandler.instance.closeGUI(sender);
			GUIHandler.instance.closeGUI(request.receiver);
		}
		requests.remove(sender);
	}
	
	public static void removeRequest(Player sender) {
		requests.remove(sender);
	}
	
	@EventHandler
	public void onLeave(PlayerQuitEvent e) {
		Arena arena = getArena(e.getPlayer());
		if(arena != null) {
			arena.onLeave(e.getPlayer());
		}
		leaveQueue(e.getPlayer());
	}
	
	@EventHandler
	public void onDeath(EntityDamageEvent e) {
		if(e.getEntityType().equals(EntityType.PLAYER)) {
			Player player = (Player) e.getEntity();
			Arena arena = getArena(player);
			if(arena != null) {
				if(player.getHealth() - e.getDamage() <= 0) {
					e.setDamage(0);
					arena.onLeave(player);
				}
			}
		}
	}
	
}
