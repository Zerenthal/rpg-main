package net.bobmandude9889.rpg.duel;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class DuelPlayer {
	
	Player player;

	ItemStack[] contents;
	ItemStack[] armor;
	Double health;
	Location location;
	
	public DuelPlayer(Player player) {
		this.player = player;
		
		this.contents = player.getInventory().getContents().clone();
		this.armor = player.getInventory().getArmorContents().clone();
		this.health = player.getHealth();
		this.location = player.getLocation().clone();
	}
	
	public void setKit(DuelKit kit) {
		player.getInventory().clear();
		player.setHealth(20);
		
		player.getInventory().setContents(kit.contents);
		player.getInventory().setArmorContents(kit.armor);
	}
	
	public void restorePlayer() {
		player.teleport(location);
		player.setHealth(health);
		player.getInventory().setContents(contents);
		player.getInventory().setArmorContents(armor);
	}
	
}
