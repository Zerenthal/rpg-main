package net.bobmandude9889.rpg.duel;

import java.math.BigInteger;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;

import net.bobmandude9889.rpg.command.Command;
import net.bobmandude9889.rpg.command.TabComplete;
import net.bobmandude9889.rpg.command.TabCompleteHandler;
import net.bobmandude9889.rpg.command.TabCompleteNode;
import net.bobmandude9889.rpg.duel.actions.BetSelectionAction;
import net.bobmandude9889.rpg.duel.actions.KitSelectionAction;
import net.bobmandude9889.rpg.gui.GUI;
import net.bobmandude9889.rpg.gui.GUIHandler;
import net.bobmandude9889.rpg.gui.GUITemplate;
import net.bobmandude9889.rpg.gui.action.GUIAction;
import net.bobmandude9889.rpg.gui.action.GUIChangeAction;
import net.bobmandude9889.rpg.gui.action.GUICloseAction;
import net.bobmandude9889.rpg.gui.action.GUIOnClickAction;
import net.bobmandude9889.rpg.gui.event.GUIChangeEvent;
import net.bobmandude9889.rpg.gui.event.GUIClickEvent;
import net.bobmandude9889.rpg.gui.event.GUICloseEvent;
import net.bobmandude9889.rpg.player.RPGPlayerManager;
import net.bobmandude9889.rpg.util.ItemBuilder;
import net.bobmandude9889.rpg.util.Util;
import net.bobmandude9889.rpg.util.tellraw.TellrawBuilder;
import net.bobmandude9889.rpg.util.tellraw.TellrawChatText;
import net.bobmandude9889.rpg.util.tellraw.TellrawText;

public class DuelCommand implements TabComplete {

	GUITemplate kitTemp;
	GUITemplate duelTemp;

	List<InventoryAction> cancel;

	public DuelCommand() {
		cancel = Lists.newArrayList(InventoryAction.COLLECT_TO_CURSOR, InventoryAction.DROP_ALL_CURSOR, InventoryAction.DROP_ALL_SLOT, InventoryAction.DROP_ONE_CURSOR, InventoryAction.DROP_ONE_SLOT, InventoryAction.HOTBAR_MOVE_AND_READD, InventoryAction.HOTBAR_SWAP);

		kitTemp = new GUITemplate(27, "&aSelect a kit!");

		duelTemp = new GUITemplate(54, "Duel");

		for (int i = 0; i < 5; i++) {
			duelTemp.setButton(4 + (i * 9), new ItemBuilder(Material.IRON_FENCE).setName(" ").getItem(), GUIAction.blank);
		}
	}

	private void updateInventories(final GUI senderGUI, final GUI receiverGUI, final DuelRequest request, GUIChangeEvent event) {
		String waiting = "&cWaiting for opponent to accept!";
		String accept = "&aOpponent has accepted!";

		ItemStack acceptButton = new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 5).setName("&aAccept").setLore("&cIf your inventory is full,", "&citems will be dropped at your location.").getItem();
		ItemStack cancelButton = new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 14).setName("&cCancel").setLore("&aChange your mind about the items?").getItem();

		final GUIAction senderAcceptAction = new GUIAction() {
			@Override
			public void onClick(GUIClickEvent e) {
				request.senderAccepted = true;
				updateInventories(senderGUI, receiverGUI, request, null);
			}
		};

		final GUIAction senderCancelAcceptAction = new GUIAction() {
			@Override
			public void onClick(GUIClickEvent e) {
				request.senderAccepted = false;
				updateInventories(senderGUI, receiverGUI, request, null);
			}
		};

		final GUIAction receiverAcceptAction = new GUIAction() {
			@Override
			public void onClick(GUIClickEvent e) {
				request.receiverAccepted = true;
				updateInventories(senderGUI, receiverGUI, request, null);
			}
		};

		final GUIAction receiverCancelAcceptAction = new GUIAction() {
			@Override
			public void onClick(GUIClickEvent e) {
				request.receiverAccepted = false;
				updateInventories(senderGUI, receiverGUI, request, null);
			}
		};

		if (event == null) {

			if (request.receiverAccepted) {
				senderGUI.setName(accept);
				receiverGUI.setButton(49, cancelButton, receiverCancelAcceptAction);
			} else {
				senderGUI.setName(waiting);
				receiverGUI.setButton(49, acceptButton, receiverAcceptAction);
			}

			if (request.senderAccepted) {
				receiverGUI.setName(accept);
				senderGUI.setButton(49, cancelButton, senderCancelAcceptAction);
			} else {
				receiverGUI.setName(waiting);
				senderGUI.setButton(49, acceptButton, senderAcceptAction);
			}

			if (request.receiverAccepted && request.senderAccepted) {
				ItemStack[] contents = new ItemStack[senderGUI.inventory.getSize()];
				for (int i = 0; i < senderGUI.size; i++) {
					ItemStack item = senderGUI.inventory.getItem(i);
					if ((i % 9) != 4) {
						contents[i] = item;
					}
				}
				request.contents = contents;
				senderGUI.closeInventory();
				receiverGUI.closeInventory();
				DuelManager.removeRequest(request.sender);
				DuelManager.joinQueue(request);
			}
		}

		if (event != null) {
			GUI gui = event.gui;
			GUI other = gui.player.equals(senderGUI.player) ? receiverGUI : senderGUI;

			boolean changedRight = false;
			for (int i = 0; i < event.inventory.getSize(); i++) {
				if ((i % 9) > 4) {
					if (event.added[i] != null || event.removed[i] != null)
						changedRight = true;
				}
			}

			ItemStack[] leftContents = new ItemStack[24];
			ItemStack[] rightAdded = new ItemStack[24];
			ItemStack[] rightContents = new ItemStack[24];

			for (int i = 0; i < event.inventory.getSize(); i++) {
				ItemStack item = event.contents[i];
				if ((i % 9) < 4) {
					int index = i - (Math.floorDiv(i, 9) * 5);
					leftContents[index] = item;
				} else if ((i % 9) > 4) {
					int index = (i - 5) - (Math.floorDiv(i, 9) * 5);
					rightAdded[index] = event.added[i];
					rightContents[index] = event.contents[i];
				}
			}

			if (changedRight) {
				for (int i = 0; i < leftContents.length; i++) {
					int index = i + (Math.floorDiv(i, 4) * 5);
					gui.setItem(index, leftContents[i]);
					gui.setItem(index + 5, rightContents[i]);

				}
			}

			for (int i = 0; i < leftContents.length; i++) {
				ItemStack item = leftContents[i];
				int index = i + (Math.floorDiv(i, 4) * 5) + 5;
				if (Util.isCoin(item)) {
					gui.setItem(index - 5, null);
					leftContents[i] = null;
					RPGPlayerManager.getPlayer(gui.player).giveItem(item);
					item = null;
				}
				other.setItem(index, item);
			}
		}
	}

	public void acceptRequest(final Player sender, final Player receiver) {
		final DuelRequest request = DuelManager.getRequest(sender);

		if (request != null && request.receiver.equals(receiver)) {
			request.timeSent = Long.MAX_VALUE;
			final GUI senderGUI = duelTemp.open(sender);
			senderGUI.setAllowChange(true);
			final GUI receiverGUI = duelTemp.open(receiver);
			receiverGUI.setAllowChange(true);

			GUIChangeAction onChange = new GUIChangeAction() {

				@Override
				public void onChange(GUIChangeEvent e) {
					updateInventories(senderGUI, receiverGUI, request, e);
				}

			};

			GUIOnClickAction onClick = new GUIOnClickAction() {

				@Override
				public void onClick(InventoryClickEvent e) {
					e.setCancelled(cancel.contains(e.getAction()));
					if (e.getClickedInventory() != null && e.getClickedInventory().getType().equals(InventoryType.CHEST) && (e.getSlot() % 9) > 4) {
						e.setCancelled(true);
					}
					if (e.getClickedInventory() != null && e.getAction().equals(InventoryAction.MOVE_TO_OTHER_INVENTORY) && e.getClickedInventory().getType().equals(InventoryType.PLAYER)) {
						e.setCancelled(true);
						GUI gui = GUIHandler.instance.guis.get(e.getWhoClicked());
						GUI other = gui.player.equals(senderGUI.player) ? receiverGUI : senderGUI;
						for (int i = 0; i < gui.size; i++) {
							ItemStack item = gui.inventory.getItem(i);
							if ((i % 9) < 4 && item == null) {
								gui.setItem(i, e.getCurrentItem());
								other.setItem(i + 5, e.getCurrentItem());
								e.getClickedInventory().setItem(e.getSlot(), null);
								break;
							}
						}
					}
				}
			};

			GUICloseAction onClose = new GUICloseAction() {

				@Override
				public void onClose(GUICloseEvent e) {
					if (e.playerClosed) {
						if (e.player.getName().equals(sender.getName())) {
							cancelRequest(sender);
						} else {
							denyRequest(sender, receiver);
						}

						for (int i = 0; i < senderGUI.inventory.getSize(); i++) {
							ItemStack senderItem = senderGUI.inventory.getContents()[i];
							ItemStack receiverItem = receiverGUI.inventory.getContents()[i];
							if ((i % 9) < 4) {
								RPGPlayerManager.getPlayer(sender).giveItem(senderItem);
								RPGPlayerManager.getPlayer(receiver).giveItem(receiverItem);
							}
						}
					}
				}

			};

			senderGUI.setOnChange(onChange);
			receiverGUI.setOnChange(onChange);

			senderGUI.setOnClick(onClick);
			receiverGUI.setOnClick(onClick);

			senderGUI.setOnClose(onClose);
			receiverGUI.setOnClose(onClose);

			updateInventories(senderGUI, receiverGUI, request, null);
		} else {
			receiver.sendMessage(ChatColor.GRAY + "You do not have a request from " + ChatColor.RED + sender.getName());
		}
	}

	public void denyRequest(Player sender, Player receiver) {
		DuelRequest request = DuelManager.getRequest(sender);
		if (request != null && request.receiver.equals(receiver)) {
			DuelManager.denyRequest(sender);
		} else {
			receiver.sendMessage(ChatColor.GRAY + "You do not have a request from " + ChatColor.RED + sender.getName());
		}
	}

	public void cancelRequest(Player sender) {
		if (DuelManager.getRequest(sender) != null) {
			DuelManager.cancelRequest(sender, true);
		} else if (DuelManager.isInQueue(sender)) {
			DuelManager.leaveQueue(sender);
		} else {
			sender.sendMessage(ChatColor.RED + "You do not have a request you can cancel!");
		}
	}

	public void sendRequest(Player sender, Player receiver) {
		if (DuelManager.getRequest(sender) == null) {

			if (DuelManager.canSendRequest(sender)) {
				if (!DuelManager.isInQueue(sender)) {
					final DuelRequest request = new DuelRequest(sender, receiver, DuelKit.BOXING, BigInteger.ZERO);
					DuelManager.setLastRequest(sender);

					GUI kitGUI = kitTemp.open(sender);

					kitGUI.setOnClose(new GUICloseAction() {

						@Override
						public void onClose(GUICloseEvent e) {
							if (e.playerClosed) {
								e.player.sendMessage(ChatColor.RED + "Request canceled.");
							}
						}

					});

					kitGUI.setName(getTitle(request));

					kitGUI.setButton(0, new ItemBuilder(Material.WOOD_SWORD).setName(DuelKit.BOXING.nameColor + DuelKit.BOXING.name).setLore(DuelKit.BOXING.description.split("\\n")).getItem(), new KitSelectionAction(DuelKit.BOXING, request));
					kitGUI.setButton(4, new ItemBuilder(Material.IRON_SWORD).setName(DuelKit.NORMAL.nameColor + DuelKit.NORMAL.name).setLore(DuelKit.NORMAL.description.split("\\n")).getItem(), new KitSelectionAction(DuelKit.NORMAL, request));
					kitGUI.setButton(8, new ItemBuilder(Material.DIAMOND_SWORD).setName(DuelKit.STRONG.nameColor + DuelKit.STRONG.name).setLore(DuelKit.STRONG.description.split("\\n")).getItem(), new KitSelectionAction(DuelKit.STRONG, request));

					for (int i = 10; i < 17; i++) {
						BigInteger bet = new BigInteger(Integer.toString((int) Math.pow(10, i - 10)));
						kitGUI.setButton(i, new ItemBuilder(Material.GOLD_BLOCK).setName("&a$" + bet).setLore(ChatColor.RED + "Left click to add.", ChatColor.RED + "Right click to subtract.").getItem(), new BetSelectionAction(bet, request));
					}

					kitGUI.setButton(22, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 5).setName("&aConfirm").getItem(), new GUIAction() {

						@Override
						public void onClick(GUIClickEvent e) {
							try {
								DuelManager.sendRequest(request);
								request.confirmed = true;
								e.gui.closeInventory();
								e.player.sendMessage(ChatColor.GREEN + "Request sent!");
							} catch (Exception e1) {
								e1.printStackTrace();
							}
						}

					});
				} else {
					sender.sendMessage(ChatColor.GRAY + "You are already in the queue. Use " + ChatColor.RED + "/duel cancel" + ChatColor.GRAY + " to leave the queue.");
				}
			} else {
				sender.sendMessage(ChatColor.GRAY + "Please wait " + ChatColor.RED + DuelManager.getRemainingRequestCooldown(sender) + " seconds " + ChatColor.GRAY + "before sending another request.");
			}
		} else {
			TellrawBuilder builder = new TellrawBuilder();
			builder.addText(new TellrawChatText("You already have an open request. Use ").setColor(ChatColor.RED));
			builder.addText(new TellrawChatText("/duel cancel").setColor(ChatColor.GREEN).setRunCommandOnClick("/duel cancel").setShowTextOnHover(new TellrawText("Click to cancel.").setColor(ChatColor.AQUA)));
			builder.addText(new TellrawChatText(" to cancel your current request.").setColor(ChatColor.RED));
			builder.sendToPlayer(sender);
			sendHelp(sender);
		}
	}

	public void sendHelp(Player player) {
		player.sendMessage(ChatColor.GREEN + "\n\n---===[Duel Arenas]===---");
		player.sendMessage(ChatColor.GRAY + "/duel <player>\n" + ChatColor.GREEN + "Sends a player a duel request.");
		player.sendMessage(ChatColor.GRAY + "/duel accept <player>\n" + ChatColor.GREEN + "Accepts a duel request from a player.");
		player.sendMessage(ChatColor.GRAY + "/duel deny <player>\n" + ChatColor.GREEN + "Denys a duel request from a player.");
		player.sendMessage(ChatColor.GRAY + "/duel cancel\n" + ChatColor.GREEN + "Cancels your current request.");
		player.sendMessage(ChatColor.GRAY + "/duel leave\n" + ChatColor.GREEN + "Leaves the duel you are in.");
	}

	@Command
	public void duelCommand(CommandSender sender, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (args.length >= 2 && args[0].equalsIgnoreCase("accept")) {
				Player pSender = Bukkit.getPlayer(args[1]);
				if (pSender != null)
					acceptRequest(pSender, player);
				else
					player.sendMessage(ChatColor.GRAY + "Player not found: " + ChatColor.RED + args[1]);
			} else if (args.length >= 2 && args[0].equalsIgnoreCase("deny")) {
				Player pSender = Bukkit.getPlayer(args[1]);
				if (pSender != null)
					denyRequest(pSender, player);
				else
					player.sendMessage(ChatColor.GRAY + "Player not found: " + ChatColor.RED + args[1]);
			} else if (args.length >= 1 && args[0].equalsIgnoreCase("cancel")) {
				cancelRequest(player);
			} else if (args.length >= 1 && args[0].equalsIgnoreCase("leave")) {
				Arena arena = DuelManager.getArena(player);
				if (arena != null) {
					arena.onLeave(player);
				} else if(DuelManager.isInQueue(player)) {
					DuelManager.leaveQueue(player);
				}
			} else if (args.length >= 1) {
				Player receiver = Bukkit.getPlayer(args[0]);
				if (receiver != null)
					if (receiver.equals(player)) {
						sender.sendMessage(ChatColor.RED + "You can not duel yourself!");
					} else
						sendRequest(player, receiver);
				else {
					player.sendMessage(ChatColor.GRAY + "Player not found: " + ChatColor.RED + args[0]);
					sendHelp(player);
				}
			} else {
				sendHelp(player);
			}
		}
	}

	@Override
	public TabCompleteNode tabComplete() {
		TabCompleteNode parent = new TabCompleteNode();
		parent.addChild("accept","deny","cancel","leave");
		parent.addChild(player -> TabCompleteHandler.getOnlinePlayers());
		return parent;
	}
	
	public static String getTitle(DuelRequest request) {
		return "&2Bet: $" + request.bet + " with " + request.kit.name;
	}

}
