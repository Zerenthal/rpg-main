package net.bobmandude9889.rpg.duel;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.bobmandude9889.rpg.command.Command;

public class ArenaCommand {

	static Arena creating;

	@Command
	public void onCommand(CommandSender sender, String[] args) {
		if (sender instanceof Player) {
			if (args.length >= 1 && args[0].equalsIgnoreCase("create")) {
				if (creating == null) {
					creating = new Arena(null, null);
					int id = DuelManager.arenas.size();
					sender.sendMessage(ChatColor.GRAY + "Arena " + ChatColor.GREEN + id + ChatColor.GRAY + " created. \nUse\n" + ChatColor.GREEN + "/arena spawn1 " + id + ChatColor.GRAY + "\nand\n" + ChatColor.GREEN + "/arena spawn2 " + id + ChatColor.GRAY + "\nto set the spawn locations for the arena.");
				} else {
					sender.sendMessage(ChatColor.RED + "Please set the spawns for the unfinished arena first.");
				}
			} else if (args.length >= 2) {
				if (args[0].equalsIgnoreCase("spawn1") || args[0].equalsIgnoreCase("spawn2")) {
					Player player = (Player) sender;
					int id = Integer.parseInt(args[1]);
					Arena arena = creating;
					if (id < DuelManager.arenas.size()) {
						arena = DuelManager.arenas.get(id);
					}
					if (id > DuelManager.arenas.size()) {
						sender.sendMessage(ChatColor.RED + "That is not a valid arena!");
					} else {
						if (args[0].equalsIgnoreCase("spawn1")) {
							arena.spawn1 = player.getLocation().clone();
						} else {
							arena.spawn2 = player.getLocation().clone();
						}

						sender.sendMessage(ChatColor.GREEN + "Spawn point set!");
						if (id == DuelManager.arenas.size()) {
							if (arena.spawn1 != null && arena.spawn2 != null) {
								sender.sendMessage(ChatColor.GREEN + "Succesfully created arena " + id + "!");
								DuelManager.registerArena(arena);
								creating = null;
							}
						}
					}
				} else if(args[0].equals("remove")) {
					int id = Integer.parseInt(args[1]);
					DuelManager.removeArena(id);
					sender.sendMessage(ChatColor.GREEN + "Arena removed.");
				}
			}
		}
	}

}
