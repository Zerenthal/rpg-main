package net.bobmandude9889.rpg.duel;

import java.math.BigInteger;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import net.bobmandude9889.rpg.player.RPGPlayerManager;

public class Arena {

	Location spawn1;
	Location spawn2;

	DuelKit kit;
	
	DuelPlayer[] players = new DuelPlayer[2];

	DuelPlayer winner;
	DuelPlayer loser;
	
	BigInteger price;
	ItemStack[] items;
	
	boolean available = true;
	
	public Arena(Location spawn1, Location spawn2) {
		this.spawn1 = spawn1;
		this.spawn2 = spawn2;
	}

	public void setup(DuelRequest request) {
		this.kit = request.kit;
		this.price = request.bet;
		this.items = request.contents;
		setPlayer(request.sender, 0);
		setPlayer(request.receiver, 1);
		this.available = false;
	}
	
	public void setPlayer(Player player, int i) {
		players[i] = new DuelPlayer(player);
	}

	protected void reset() {
		players[0].restorePlayer();
		players[1].restorePlayer();
		players = new DuelPlayer[2];
		available = true;
	}
	
	public void startDuel() {
		DuelPlayer p1 = players[0];
		DuelPlayer p2 = players[1];
		
		p1.setKit(kit);
		p2.setKit(kit);
		
		p1.player.teleport(spawn1);
		p2.player.teleport(spawn2);
	}
	
	public DuelPlayer getPlayer(Player player) {
		if(players[0].player.equals(player))
			return players[0];
		if(players[1].player.equals(player))
			return players[1];
		return null;
	}
	
	public DuelPlayer getOther(Player player) {
		if(players[0].player.equals(player))
			return players[1];
		if(players[1].player.equals(player))
			return players[0];
		return null;
	}
	
	public void rewardWinner() {
		RPGPlayerManager.getPlayer(winner.player).addCoins(price);
		RPGPlayerManager.getPlayer(loser.player).removeCoins(price);
		for(ItemStack item : items) {
			RPGPlayerManager.getPlayer(winner.player).giveItem(item);
		}
	}
	
	public void endGame(DuelPlayer winner, DuelPlayer loser) {
		this.winner = winner;
		this.loser = loser;
		
		winner.player.sendMessage(ChatColor.GREEN + "You won! You have received $" + this.price + " and any items added to the bet!");
		loser.player.sendMessage(ChatColor.RED + "You lost. Better luck next time!");

		reset();
		rewardWinner();
	}
	
	public void onLeave(Player player) {
		endGame(getOther(player), getPlayer(player));
	}
	
}
