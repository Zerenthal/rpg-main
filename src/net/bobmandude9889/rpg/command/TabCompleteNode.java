package net.bobmandude9889.rpg.command;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

public class TabCompleteNode {

	TabCompleteMethod method;
	List<TabCompleteNode> children;
	
	public TabCompleteNode(TabCompleteMethod method) {
		this.method = method;
		children = new ArrayList<>();
	}
	
	public TabCompleteNode(String... values) {
		this.method = new StaticTabMethod(values);
		children = new ArrayList<>();
	}
	
	public TabCompleteNode addChild(TabCompleteMethod method) {
		TabCompleteNode node = new TabCompleteNode(method);
		children.add(node);
		return node;
	}
	
	public TabCompleteNode addChild(String... values) {
		TabCompleteNode node = new TabCompleteNode(values);
		children.add(node);
		return node;
	}
	
	public TabCompleteNode addChild(List<String> values) {
		TabCompleteNode node = new TabCompleteNode(values.toArray(new String[]{}));
		children.add(node);
		return node;
	}
	
	public TabCompleteNode getChild(String value, Player player) {
		for (TabCompleteNode node : children) {
			if (node.method.getList(player).contains(value)) {
				return node;
			}
		}
		return null;
	}
	
}
