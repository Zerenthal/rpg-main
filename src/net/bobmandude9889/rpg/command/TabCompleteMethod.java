package net.bobmandude9889.rpg.command;

import java.util.List;

import org.bukkit.entity.Player;

public interface TabCompleteMethod {

	public List<String> getList(Player player);
	
}
