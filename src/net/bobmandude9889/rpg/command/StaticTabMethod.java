package net.bobmandude9889.rpg.command;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

public class StaticTabMethod implements TabCompleteMethod{

	List<String> values;
	
	public StaticTabMethod(String... values) {
		this.values = new ArrayList<>();
		for (String s : values) {
			this.values.add(s);
		}
	}
	
	@Override
	public List<String> getList(Player player) {
		return values;
	}

}
