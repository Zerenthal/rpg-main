package net.bobmandude9889.rpg.command;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;

import net.bobmandude9889.rpg.dialogue.DialogueReload;
import net.bobmandude9889.rpg.duel.ArenaCommand;
import net.bobmandude9889.rpg.duel.DuelCommand;
import net.bobmandude9889.rpg.mobRegion.MobRegionManager;
import net.bobmandude9889.rpg.player.CoinCommand;
import net.bobmandude9889.rpg.quest.BookCommand;
import net.bobmandude9889.rpg.shop.ShopCommand;
import net.bobmandude9889.rpg.skills.SkillsCommand;
import net.bobmandude9889.rpg.skills.XPCommand;
import net.bobmandude9889.rpg.skills.fishing.FishingCommand;
import net.bobmandude9889.rpg.trade.TradeCommand;
import net.bobmandude9889.rpg.util.ChunkCommand;
import net.bobmandude9889.rpg.util.TokenCommand;
import net.bobmandude9889.rpg.vault.VaultCommand;

public enum CommandEnum {

	// @formatter:off
	DUEL("duel", "/duel <player>", "Fight another player 1 on 1.", DuelCommand.class),
	NPC("npc", "/npc spawn <NPC Type>, /npc remove, /npc skin <uuid>", "Spawn or modify an NPC", "npc.admin", null),
	VAULT("vault", "/vault", "Open your vault anywhere.", "vault.admin", VaultCommand.class),
	XP("xp", "/xp set,add,get <player> <type> <amount>", "Check or set players xp for specified skills.", "xp.admin", XPCommand.class),
	ARENA("arena","/arena remove <id>,create,spawn1 <id>,spawn2 <id>", "Create and modify arenas.", "arena.create", ArenaCommand.class),
	COIN("coins","/coins get <player>,add <amount> [player], remove <amount> [player],set <amount> [player]", "Change and view players coins", "coins.admin", CoinCommand.class),
	SKILLS("skills","/skills", "Shows GUI with information about each skill.", SkillsCommand.class),
	TRADE("trade","/trade <player>", "Trade with another player", TradeCommand.class),
	SHOP("shop", "/shop", "Buy items from a simple GUI.", ShopCommand.class, "buy"),
	SELL("sell", "/sell", "Sell items with a simple GUI.", ShopCommand.class),
	CHUNK("chunk", "/chunk", "Get chunk info", ChunkCommand.class),
	TOKEN("token", "/token", "Get your token for website sign up.", TokenCommand.class, "gettoken"),
	MOBREGION("mobregion", "/mobregion <add,remove> <region>\n/mobregion mob <region> <add,remove> <type>\n/mobregion setmax <region> <amount>\n/mobregion setcooldown <region> <amount>", "Set regions for mobs to spawn.", "mobregion.create", MobRegionManager.class, "mr"),
	RELOADDIALOGUE("reloaddialogue","/reloaddialogue","Reloads the dialogue options","dialogue.reload",DialogueReload.class,"rdial"),
	BOOKCOMMAND("book","/book","Opens quest book",BookCommand.class),
	FISHINGREGION("fishingregion", "/fr", "Main fishing region command.", FishingCommand.class, "fr");
	// @formatter:on

	private String name;
	private String perm;
	private String usage;
	private String description;
	private Class<?> commandClass;
	private String[] aliases;

	private CommandEnum(String name, String usage, String description, Class<?> commandClass, String... aliases) {
		this.name = name;
		this.usage = usage;
		this.description = description;
		this.commandClass = commandClass;
		this.aliases = aliases;
	}

	private CommandEnum(String name, String usage, String description, String perm, Class<?> commandClass, String... aliases) {
		this.name = name;
		this.perm = perm;
		this.usage = usage;
		this.description = description;
		this.commandClass = commandClass;
		this.aliases = aliases;
	}

	public String getName() {
		return name;
	}

	public String getPerm() {
		return perm;
	}

	public String getDescription() {
		return description;
	}

	public String getUsage() {
		return usage;
	}
	
	public String[] getAliases() {
		return aliases;
	}

	public static void registerCommands() {
		for (CommandEnum cmd : values()) {
			CommandExecuter exc = new CommandExecuter(cmd.name, cmd.description, cmd.usage, cmd.perm, cmd.commandClass == null ? DisabledCommand.class : cmd.commandClass);
			List<String> aliasList = new ArrayList<String>();
			Collections.addAll(aliasList, cmd.aliases);
			exc.setAliases(aliasList);
			exc.register();
			
			if (cmd.commandClass != null && TabComplete.class.isAssignableFrom(cmd.commandClass)) {
				try {
					TabCompleteHandler.addCommand(exc);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static org.bukkit.command.Command registerNewCommand(String name, String description, String usage, String perm, Class<?> commandClass, String... aliases) {
		CommandExecuter cmd = new CommandExecuter(name, description, usage, perm, commandClass);
		List<String> aliasList = new ArrayList<String>();
		Collections.addAll(aliasList, aliases);
		cmd.setAliases(aliasList);
		cmd.register();
		return cmd;
	}

	public static void unregisterCommand(String name) {
		Field cmdMap;
		try {
			cmdMap = Bukkit.getServer().getClass().getDeclaredField("commandMap");
			cmdMap.setAccessible(true);
			CommandMap map = ((CommandMap) cmdMap.get(Bukkit.getServer()));
			map.getCommand(name).unregister(map);
			System.out.println("Command " + name + " unregistered.");
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}

}
