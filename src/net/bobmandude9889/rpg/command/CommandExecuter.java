package net.bobmandude9889.rpg.command;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import net.bobmandude9889.rpg.RPG;
import net.md_5.bungee.api.ChatColor;

public class CommandExecuter extends org.bukkit.command.Command {

	Class<?> commandClass;

	Method commandMethod;

	Object commandObj;

	String name;

	JavaPlugin plugin;

	public CommandExecuter(String name, String description, String usage, String perm, Class<?> commandClass) {
		super(name);
		this.name = name;
		this.setUsage(usage);
		if (perm != null)
			this.setPermission(perm);
		this.setDescription(description);
		this.commandClass = commandClass;
		try {
			this.commandObj = commandClass.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	public void register() {
		for (Method method : commandClass.getMethods()) {
			Command[] annotations = method.getAnnotationsByType(Command.class);
			if (annotations.length > 0) {
				if (annotations[0].name().equals("") || annotations[0].name().equalsIgnoreCase(this.name)) {
					this.commandMethod = method;
					this.plugin = RPG.instance;

					Field cmdMap;
					try {
						cmdMap = Bukkit.getServer().getClass().getDeclaredField("commandMap");
						cmdMap.setAccessible(true);
						CommandMap map = ((CommandMap) cmdMap.get(Bukkit.getServer()));
						map.register(name, this);
					} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
						e.printStackTrace();
					}

					System.out.println("Registered command " + name + " with method " + method.getName() + ".");
					return;
				}
			}
		}
		System.err.println("Could not find command method! Not registering command " + name);
	}

	@Override
	public boolean execute(CommandSender sender, String label, String[] args) {
		if (this.testPermission(sender)) {
			try {
				Class<?>[] paramTypes = commandMethod.getParameterTypes();
				Object[] params = new Object[paramTypes.length];
				for (int i = 0; i < paramTypes.length; i++) {
					if (paramTypes[i].equals(String.class)) {
						params[i] = label;
					} else if (paramTypes[i].equals(String[].class)) {
						params[i] = args;
					} else if (paramTypes[i].equals(org.bukkit.command.Command.class)) {
						params[i] = this;
					} else if (paramTypes[i].equals(CommandSender.class)) {
						params[i] = sender;
					} else if (paramTypes[i].equals(Player.class)) {
						params[i] = sender;
					}
				}

				Object ret = commandMethod.invoke(commandObj, params);
				if(ret != null) {
					if(!(boolean) ret) {
						sender.sendMessage(ChatColor.RED + this.getUsage());
					}
				}
				return true;
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

}
