package net.bobmandude9889.rpg.command;

import org.bukkit.entity.Player;

import net.md_5.bungee.api.ChatColor;

public class DisabledCommand {

	@Command
	public void disabledCommand(Player player) {
		player.sendMessage(ChatColor.RED + "This command is disabled.");
	}
	
}
