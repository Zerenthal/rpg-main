package net.bobmandude9889.rpg.command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.TabCompleteEvent;

public class TabCompleteHandler implements Listener {

	public static List<CommandExecuter> tabCompleteCommands;
	
	@EventHandler
	public void onTabComplete(TabCompleteEvent e) {
		if (e.getBuffer().equals("/")) {
			e.setCompletions(new ArrayList<>());
			return;
		}
		if (e.getBuffer().contains(" ") && e.getBuffer().startsWith("/")) {
			Player player = (Player) e.getSender();
			
			String[] splitBuffer = e.getBuffer().split(" ");
			String commandName = splitBuffer[0].substring(1);
			String[] args = Arrays.copyOfRange(splitBuffer, 1, splitBuffer.length);

			for (CommandExecuter command : tabCompleteCommands) {
				List<String> aliases = new ArrayList<>();
				aliases.addAll(command.getAliases());
				aliases.add(command.name);
				if (aliases.contains(commandName.toLowerCase()) && player.hasPermission(command.getPermission())) {
					TabCompleteNode parent = ((TabComplete)command.commandObj).tabComplete();
					List<String> completions = new ArrayList<>();
					for (int i = 0; i < (e.getBuffer().endsWith(" ") ? args.length : args.length - 1); i++) {
						parent = parent.getChild(args[i], player);
						if (parent == null)
							break;
					}
					if (parent != null) {
						for (TabCompleteNode node : parent.children) {
							completions.addAll(node.method.getList(player));
						}
					}
					if (!e.getBuffer().endsWith(" ")) {
						List<String> newCompletions = new ArrayList<>();
						for (String completion : completions) {
							if (completion.toLowerCase().startsWith(args[args.length - 1].toLowerCase()))
								newCompletions.add(completion);
						}
						completions = newCompletions;
					}
					e.setCompletions(completions);
				}
			}
		}
	}

	public static void init() {
		tabCompleteCommands = new ArrayList<>();
	}
	
	public static void addCommand(CommandExecuter command) {
		tabCompleteCommands.add(command);
	}
	
	public static List<String> getOnlinePlayers() {
		List<String> onlinePlayers = new ArrayList<String>();
		for (Player onlinePlayer : Bukkit.getOnlinePlayers()) {
			onlinePlayers.add(onlinePlayer.getName());
		}
		return onlinePlayers;
	}
	
	public static List<String> objectsToStrings(List<?> values) {
		List<String> strings = new ArrayList<>();
		for (Object obj : values) {
			strings.add(obj.toString().toLowerCase());
		}
		return strings;
	}
	
	public static List<String> objectsToStrings(Object[] values) {
		List<String> strings = new ArrayList<>();
		for (Object obj : values) {
			strings.add(obj.toString().toLowerCase());
		}
		return strings;
	}

}
