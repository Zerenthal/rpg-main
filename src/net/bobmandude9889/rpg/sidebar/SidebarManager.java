package net.bobmandude9889.rpg.sidebar;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import net.bobmandude9889.rpg.RPG;
import net.bobmandude9889.rpg.skills.SkillDataManager;
import net.bobmandude9889.rpg.skills.SkillDataManager.SkillData;
import net.bobmandude9889.rpg.skills.SkillManager;
import net.bobmandude9889.rpg.skills.SkillType;

public class SidebarManager {

	public static HashMap<Player, SidebarDisplay> playerDisplay;
	public static HashMap<Player, SkillType> skillDisplay;
	public static HashMap<Player, Integer> displayResetTask;
	
	public static void init() {
		playerDisplay = new HashMap<Player, SidebarDisplay>();
		skillDisplay = new HashMap<Player, SkillType>();
		displayResetTask = new HashMap<Player, Integer>();
	}

	public static SkillType getSkillDisplay(Player player) {
		return skillDisplay.get(player);
	}

	public static SidebarDisplay getDisplay(Player player) {
		if (!playerDisplay.containsKey(player))
			playerDisplay.put(player, SidebarDisplay.SKILL_ALL);
		return playerDisplay.get(player);
	}
	
	public static void setSkillDisplay(Player player, SkillType type) {
		skillDisplay.put(player, type);
	}
	
	public static void setDisplay(final Player player, SidebarDisplay display) {
		playerDisplay.put(player, display);
		if(display.equals(SidebarDisplay.SKILL_ALL))
			updateSkillAll(player);
		else if(display.equals(SidebarDisplay.SKILL_INFO)) {
			updateSkillInfo(player);
			if(displayResetTask.containsKey(player)) {
				Bukkit.getScheduler().cancelTask(displayResetTask.get(player));
			}
			int i = Bukkit.getScheduler().scheduleSyncDelayedTask(RPG.instance, new Runnable() {
				@Override
				public void run() {
					setDisplay(player, SidebarDisplay.SKILL_ALL);
					displayResetTask.remove(player);
				}
			}, 200);
			displayResetTask.put(player, i);
		}
	}

	private static void updateSkillAll(Player player) {
		SidebarBuilder builder = new SidebarBuilder("skills", "dummy", "&6Skill Levels");

		SkillData skillData = SkillDataManager.getData(player);

		for (int i = 0; i < SkillType.values().length; i++) {
			SkillType skill = SkillType.values()[i];
			String name = "&a" + skill.name + "&7: &e" + SkillManager.getLevelAtXP(skillData.getXP(skill));
			builder.add(name, i);
		}
		builder.display(player);
	}
	
	private static void updateSkillInfo(Player player) {
		SidebarBuilder builder = new SidebarBuilder("skills", "dummy", "&a" + skillDisplay.get(player).name);

		SkillData skillData = SkillDataManager.getData(player);

		builder.add("&6Level: &e" + SkillManager.getLevelAtXP(skillData.getXP(getSkillDisplay(player))), 2);
		builder.add("&6XP: &e" + skillData.getXP(getSkillDisplay(player)), 1);
		builder.add("&6Next Level: &e" + (SkillManager.getXPRequired(SkillManager.getLevelAtXP(skillData.getXP(getSkillDisplay(player))) + 1) - skillData.getXP(getSkillDisplay(player))), 0);
		
		builder.display(player);
	}

}
