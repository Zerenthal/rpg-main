package net.bobmandude9889.rpg.sidebar;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import net.md_5.bungee.api.ChatColor;

public class SidebarBuilder {

	Scoreboard board;
	Objective obj;
	
	public SidebarBuilder(String name, String objective, String title) {
		ScoreboardManager manager = Bukkit.getScoreboardManager();
		board = manager.getNewScoreboard();
		obj = board.registerNewObjective(name, objective);
		obj.setDisplayName(ChatColor.translateAlternateColorCodes('&', title));
		obj.setDisplaySlot(DisplaySlot.SIDEBAR);
	}
	
	public SidebarBuilder add(String name, int value) {
		Score score = obj.getScore(ChatColor.translateAlternateColorCodes('&', name));
		score.setScore(value);
		return this;
	}
	
	public SidebarBuilder display(Player player) {
		player.setScoreboard(board);
		return this;
	}
	
}
