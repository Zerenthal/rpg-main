package net.bobmandude9889.rpg.sidebar;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.bobmandude9889.rpg.command.Command;
import net.bobmandude9889.rpg.command.CommandEnum;
import net.bobmandude9889.rpg.skills.SkillType;

public class SidebarInfoCommand {

	public static void init() {
		for (SkillType skill : SkillType.values()) {
			String name = skill.name().toLowerCase();
			CommandEnum.registerNewCommand(name, "Shows sidebar info about your " + name + " skill.", "/" + name, null, SidebarInfoCommand.class);
		}
	}

	@Command
	public void skillCommand(String name, CommandSender sender) {
		if (sender instanceof Player) {
			SkillType skill = SkillType.valueOf(name.toUpperCase());
			Player player = (Player) sender;
			SidebarManager.setSkillDisplay(player, skill);
			SidebarManager.setDisplay(player, SidebarDisplay.SKILL_INFO);
		}
	}

}
