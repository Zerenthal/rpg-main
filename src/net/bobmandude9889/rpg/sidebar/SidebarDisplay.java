package net.bobmandude9889.rpg.sidebar;

public enum SidebarDisplay {

	NONE,
	SKILL_INFO,
	SKILL_ALL,
	DUEL_QUEUE;
	
}
