package net.bobmandude9889.rpg.npc;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_12_R1.util.CraftChatMessage;
import org.bukkit.entity.Player;

import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.comphenix.protocol.wrappers.WrappedSignedProperty;
import com.mojang.authlib.GameProfile;

import net.bobmandude9889.rpg.RPG;
import net.bobmandude9889.rpg.util.Util;
import net.minecraft.server.v1_12_R1.DataWatcher;
import net.minecraft.server.v1_12_R1.DataWatcherObject;
import net.minecraft.server.v1_12_R1.DataWatcherRegistry;
import net.minecraft.server.v1_12_R1.EnumGamemode;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntity;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityHeadRotation;
import net.minecraft.server.v1_12_R1.PacketPlayOutEntityTeleport;
import net.minecraft.server.v1_12_R1.PacketPlayOutNamedEntitySpawn;
import net.minecraft.server.v1_12_R1.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_12_R1.PacketPlayOutPlayerInfo.PlayerInfoData;

public class NPCEntity {

	public DataWatcher watcher;
	public WrappedGameProfile profile;
	public String name;
	public UUID uuid;
	public UUID skinuuid;
	public int id;
	public Location location;

	public NPCEntity(String name, String skinuuid, int id, Location location) {
		this.location = location;
		this.id = id;
		this.uuid = UUID.randomUUID();
		this.skinuuid = UUID.fromString(skinuuid);
		this.watcher = new DataWatcher(null);
		this.profile = WrappedGameProfile.fromHandle(Util.getNonPlayerProfile("", uuid.toString(), name));
		this.name = Util.parseColors(name);
		watcher.register(new DataWatcherObject<>(6, DataWatcherRegistry.c), 20f);
		this.updateSkin();
	}
    
	private void setValue(Object instance, String field, Object value) throws Exception {
		Field f = instance.getClass().getDeclaredField(field);
		f.setAccessible(true);
		f.set(instance, value);
	}

	private Object getValue(Object instance, String field) throws Exception {
		Field f = instance.getClass().getDeclaredField(field);
		f.setAccessible(true);
		return f.get(instance);
	}

    public void updateSkin() {
		Bukkit.getScheduler().runTaskAsynchronously(RPG.instance, new Runnable() {
			@Override
			public void run() {
				Collection<WrappedSignedProperty> textures = SkinCacher.getTextures(skinuuid);
		        NPCEntity.this.profile.getProperties().removeAll("textures");
		        NPCEntity.this.profile.getProperties().putAll("textures",textures);
		        for(final Player player : Bukkit.getOnlinePlayers()) {
		        	NPCEntity.this.destroy(player);
		        	NPCEntity.this.sendToPlayer(player);
		        	Bukkit.getScheduler().scheduleSyncDelayedTask(RPG.instance, new Runnable() {
						@Override
						public void run() {
							NPCEntity.this.removeTab(player);
						}
					},20l);
		        }
			}
		});
    }

	public void sendToPlayer(final Player player) {
		try {
			PacketPlayOutNamedEntitySpawn packet = new PacketPlayOutNamedEntitySpawn();
			sendTab(player);

			setValue(packet, "a", id);
			setValue(packet, "b", profile.getUUID());
			setValue(packet, "c", location.getX());
			setValue(packet, "d", location.getY());
			setValue(packet, "e", location.getZ());
			setValue(packet, "f", (byte) (int) (location.getYaw() * 256.0F / 360.0F));
			setValue(packet, "g", (byte) 0);
			setValue(packet, "h", watcher);

			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
			turnHead(player);

			Bukkit.getScheduler().scheduleSyncDelayedTask(RPG.instance, new Runnable() {
				@Override
				public void run() {
					NPCEntity.this.removeTab(player);
				}
			},20l);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void destroy(Player player) {
		PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(this.id);
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
	}
	
	public void remove(Player player) {
		PacketPlayOutEntityDestroy packet = new PacketPlayOutEntityDestroy(id);
		((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		removeTab(player);
	}

	public void turnHead(Player player) {
		try {
			PacketPlayOutEntityHeadRotation packet1 = new PacketPlayOutEntityHeadRotation();

			double deltaX = location.getX() - player.getLocation().getX();
			double deltaZ = location.getZ() - player.getLocation().getZ();
			double deltaY = location.getY() - player.getLocation().getY();
			double yawh = Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaZ, 2));
			double pitchh = Math.sqrt(Math.pow(yawh, 2) + Math.pow(deltaY, 2));

			double yaw = Math.toDegrees(Math.asin(deltaX / yawh));
			double pitch = Math.toDegrees(Math.asin(yawh / pitchh));

			if (deltaZ > 0) {
				yaw = 90 + (90 - yaw);
			}

			if (yaw > 180) {
				yaw = -180 + (yaw - 180);
			}
			
			if(deltaY > 0) {
				pitch = 90 + (90 - pitch);
			}
			
			if (pitch > 180) {
				pitch = -180 + (pitch - 180);
			}
			
			pitch -= 90;
			
			setValue(packet1, "a", id);
			setValue(packet1, "b", (byte) ((yaw * (127f / 180f))));
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet1);
			
			PacketPlayOutEntity.PacketPlayOutRelEntityMoveLook packet = new PacketPlayOutEntity.PacketPlayOutRelEntityMoveLook(id, 0, 0, 0, (byte) ((yaw * (127f / 180f))), (byte) ((pitch * (127f / 180f))), true);
			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void teleport(Location location, Player player) {
		try {
			PacketPlayOutEntityTeleport packet = new PacketPlayOutEntityTeleport();

			setValue(packet, "a", id);
			setValue(packet, "b", location.getX());
			setValue(packet, "c", location.getY());
			setValue(packet, "d", location.getZ());
			setValue(packet, "e", (byte) (int) (location.getYaw() * 256.0F / 360.0F));
			setValue(packet, "f", (byte) (int) (location.getPitch() * 256.0F / 360.0F));
			setValue(packet, "g", true);

			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		} catch (Exception e) {

		}
	}

	public void sendTab(Player player) {
		try {
			PacketPlayOutPlayerInfo packet = new PacketPlayOutPlayerInfo();
			PacketPlayOutPlayerInfo.PlayerInfoData data = packet.new PlayerInfoData((GameProfile) this.profile.getHandle(), 1, EnumGamemode.NOT_SET, CraftChatMessage.fromString(name)[0]);
			@SuppressWarnings("unchecked")
			List<PacketPlayOutPlayerInfo.PlayerInfoData> players = (List<PacketPlayOutPlayerInfo.PlayerInfoData>) getValue(packet, "b");
			players.add(data);

			setValue(packet, "a", PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER);
			setValue(packet, "b", players);

			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void removeTab(Player player) {
		try {
			PacketPlayOutPlayerInfo packet = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER);
			PacketPlayOutPlayerInfo.PlayerInfoData data = packet.new PlayerInfoData((GameProfile) this.profile.getHandle(), -1, null, null);
			@SuppressWarnings("unchecked")
			List<PacketPlayOutPlayerInfo.PlayerInfoData> players = (List<PlayerInfoData>) getValue(packet, "b");
			players.add(data);

			setValue(packet, "b", players);

			((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
