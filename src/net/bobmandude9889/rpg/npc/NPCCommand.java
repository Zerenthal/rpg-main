package net.bobmandude9889.rpg.npc;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.bobmandude9889.rpg.command.Command;
import net.bobmandude9889.rpg.command.CommandEnum;
import net.md_5.bungee.api.ChatColor;

public class NPCCommand {

	@Command
	public void NPC(CommandSender sender, String[] args) {
		try {
			switch (args[0]) {
			case "spawn":
				String type = args[1].toUpperCase();
				if(type.length() > 16) {
					type = type.substring(0, 16);
				}
				NPCHandler.spawnNPC(type, ((Player) sender).getLocation());
				break;
			case "remove":
				NPCHandler.skin.remove(sender);
				NPCHandler.removing.add((Player) sender);
				sender.sendMessage(ChatColor.GREEN + "Left click an NPC to remove it!");
				break;
			case "skin":
				NPCHandler.removing.remove(sender);
				NPCHandler.skin.put((Player) sender, args[1]);
				sender.sendMessage(ChatColor.GREEN + "Left click an NPC to set it's skin.");
				break;
			default:
				throw (new Exception());
			}
		} catch (Exception e) {
			sender.sendMessage(CommandEnum.NPC.getUsage());
		}
	}
	
}
