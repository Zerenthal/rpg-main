package net.bobmandude9889.rpg.npc;

import org.bukkit.entity.Player;

import net.bobmandude9889.rpg.dialogue.DialogueManager;
import net.bobmandude9889.rpg.shop.ShopManager;
import net.bobmandude9889.rpg.vault.VaultManager;

public class NPCActions {

	@NPC
	public static void openDialogue(Player player, String id) {
		DialogueManager.openDialogue(player, Integer.parseInt(id));
	}

	@NPC
	public static void openVault(Player player) {
		VaultManager.open(player, 0);
	}
	
	@NPC
	public static void openShop(Player player, String section) {
		ShopManager.openPage(section.toLowerCase(), 0, player);
	}
	
	@NPC
	public static void openSellMenu(Player player) {
		ShopManager.openSell(player);
	}
	
}
