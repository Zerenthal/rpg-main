package net.bobmandude9889.rpg.npc;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Server;

import com.comphenix.protocol.wrappers.WrappedGameProfile;
import com.comphenix.protocol.wrappers.WrappedSignedProperty;

public class SkinCacher {

	private static HashMap<UUID, Collection<WrappedSignedProperty>> map;
	
	public static void init() {
		map = new HashMap<>();
	}
	
	public static Collection<WrappedSignedProperty> getTextures(UUID uuid) {
		if(map.containsKey(uuid)) {
			return map.get(uuid);
		}
		
		WrappedGameProfile profile;
		if(Bukkit.getPlayer(uuid) != null) {
			profile = WrappedGameProfile.fromPlayer(Bukkit.getPlayer(uuid));
		} else {
			profile = WrappedGameProfile.fromOfflinePlayer(Bukkit.getOfflinePlayer(uuid));
		}
        Object handle = profile.getHandle();
        Object sessionService = getSessionService();
        try
        {
            Method method = getFillMethod(sessionService);
            method.invoke(sessionService, handle, true);
        }
        catch (IllegalAccessException ex)
        {
            ex.printStackTrace();
            return null;
        }
        catch (InvocationTargetException ex)
        {
            ex.printStackTrace();
            return null;
        }
        profile = WrappedGameProfile.fromHandle(handle);
        Collection<WrappedSignedProperty> textures = profile.getProperties().get("textures");
        map.put(uuid, textures);
        return textures;
	}
	
	private static Object getSessionService()
    {
        Server server = Bukkit.getServer();
        try
        {
            Object mcServer = server.getClass().getDeclaredMethod("getServer").invoke(server);
            for (Method m : mcServer.getClass().getMethods())
            {
                if (m.getReturnType().getSimpleName().equalsIgnoreCase("MinecraftSessionService"))
                {
                    return m.invoke(mcServer);
                }
            }
        }
        catch (Exception ex)
        {
            throw new IllegalStateException("An error occurred while trying to get the session service", ex);
        }
        throw new IllegalStateException("No session service found :o");
    }

    private static Method getFillMethod(Object sessionService)
    {
        for(Method m : sessionService.getClass().getDeclaredMethods())
        {
            if(m.getName().equals("fillProfileProperties"))
            {
                return m;
            }
        }
        throw new IllegalStateException("No fillProfileProperties method found in the session service :o");
    }
	
}
