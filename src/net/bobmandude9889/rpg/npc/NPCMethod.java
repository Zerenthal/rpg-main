package net.bobmandude9889.rpg.npc;

import java.lang.reflect.Method;

public class NPCMethod {

	public Method method;
	public Object[] args;
	
	public NPCMethod(Method method, Object[] args) {
		this.method = method;
		this.args = args;
	}
	
}
