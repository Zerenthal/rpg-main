package net.bobmandude9889.rpg.npc;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.EnumWrappers.EntityUseAction;
import com.comphenix.protocol.wrappers.EnumWrappers.Hand;

import net.bobmandude9889.rpg.RPG;
import net.bobmandude9889.rpg.util.Util;

public class NPCHandler implements Listener {

	public static HashMap<NPCEntity, String> NPCs;

	static List<NPCEntity> removed;

	public static List<Player> removing;
	public static HashMap<Player, String> skin;

	public NPCHandler() {
		NPCs = new HashMap<NPCEntity, String>();
		removing = new ArrayList<Player>();
		removed = new ArrayList<NPCEntity>();
		skin = new HashMap<Player,String>();

		loadNPCs();

		ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(RPG.instance, PacketType.Play.Client.USE_ENTITY) {
			@Override
			public void onPacketReceiving(final PacketEvent event) {

				int id = event.getPacket().getIntegers().read(0);
				EntityUseAction action = event.getPacket().getEntityUseActions().read(0);

				for (int i = 0; i < NPCs.size(); i++) {
					final NPCEntity e = (NPCEntity) NPCs.keySet().toArray()[i];
					if (id == e.id) {
						boolean use = false;
						if (action.equals(EntityUseAction.ATTACK))
							use = true;
						else {
							Hand hand = event.getPacket().getHands().read(0);
							if (action.equals(EntityUseAction.INTERACT) && hand.equals(Hand.MAIN_HAND))
								use = true;
						}

						if (use) {
							Bukkit.getScheduler().scheduleSyncDelayedTask(RPG.instance, new Runnable() {
								@Override
								@SuppressWarnings("deprecation")
								public void run() {
									final Player player = event.getPlayer();
									if (removing.contains(player)) {
										removeNPC(e);
										removing.remove(player);
									} else if(skin.containsKey(player)) {
										try {
											e.skinuuid = UUID.fromString(skin.get(player));
										} catch(Exception ex) {
											OfflinePlayer refPlayer = Bukkit.getOfflinePlayer(skin.get(player));
											e.skinuuid = refPlayer.getUniqueId();
										}
										skin.remove(player);
										e.updateSkin();
										saveNPCs();
									} else {
										NPCType type = NPCType.get(NPCs.get(e));
										type.onInteract(player);
									}
								};
							});
						}
					}
				}
			}
		});
	}

	public static void saveNPCs() {
		File file = new File(RPG.dataFolder, "NPC.yml");
		
		try {
			file.createNewFile();
		} catch (IOException e2) {
			e2.printStackTrace();
		}

		YamlConfiguration config = YamlConfiguration.loadConfiguration(file);

		for (NPCEntity e : removed) {
			config.set(e.id + "", null);
		}

		removed.clear();

		for (NPCEntity e : NPCs.keySet()) {
			String id = Integer.toString(e.id);
			config.set(id + ".loc", Util.locationToString(e.location));
			config.set(id + ".type", NPCs.get(e).toUpperCase());
			config.set(id + ".skin", e.skinuuid.toString());
		}
		try {
			config.save(file);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	public static void loadNPCs() {
		File file = new File(RPG.dataFolder, "NPC.yml");

		if (file.exists()) {
			YamlConfiguration config = YamlConfiguration.loadConfiguration(file);

			NPCs.clear();
			
			for (String id : config.getKeys(false)) {
				String type = config.getString(id + ".type").toUpperCase();
				Location loc = Util.stringToLocation(config.getString(id + ".loc"));
				String uuid = config.getString(id + ".skin");
				NPCEntity npc = new NPCEntity(Util.capName(type), uuid, Integer.parseInt(id), loc);
				NPCs.put(npc, type.toUpperCase());
				try {
					config.save(file);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	public static void spawnNPC(String type, Location loc) {
		NPCEntity npc = new NPCEntity(Util.capName(type), "069a79f4-44e9-4726-a5be-fca90e38aaf5", new Random().nextInt(100000), loc);
		NPCs.put(npc, type.toUpperCase());
		for (Player player : Bukkit.getOnlinePlayers()) {
			npc.sendToPlayer(player);
		}
		saveNPCs();
	}

	public static void removeNPC(NPCEntity e) {
		NPCs.remove(e);
		removed.add(e);
		for (Player player : Bukkit.getOnlinePlayers()) {
			e.remove(player);
		}
		saveNPCs();
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		for (NPCEntity npc : NPCs.keySet()) {
			npc.sendToPlayer(e.getPlayer());
		}
	}

	@EventHandler
	public void onMove(final PlayerMoveEvent ev) {
		for (final NPCEntity e : NPCs.keySet()) {
			e.turnHead(ev.getPlayer());
		}
	}

}