package net.bobmandude9889.rpg.npc;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.bukkit.entity.Player;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import net.bobmandude9889.rpg.RPG;
import net.bobmandude9889.rpg.player.RPGPlayer;
import net.bobmandude9889.rpg.player.RPGPlayerManager;
import net.bobmandude9889.rpg.util.sql.DBConn;
import net.bobmandude9889.rpg.util.sql.SqlStatement;

public class NPCType {

	static HashMap<String, NPCType> types;

	static {
		types = new HashMap<>();

		DBConn conn = RPG.sqlMCConn;

		SqlStatement stmt = new SqlStatement(conn);
		ResultSet results = stmt.select("NPCTypes");

		try {
			while (results.next()) {

				JSONObject action = (JSONObject) new JSONParser().parse(results.getString("Action"));
				
				HashMap<String, NPCMethod> methods = new HashMap<>();
				NPCMethod defaultMethod = null;
				
				for(Object key : action.keySet()) {
					String[] type = ((String) action.get(key)).split(" ");
					String methodName = type[0];
					Object[] args = new Object[type.length];
					for (int i = 1; i < type.length; i++) {
						args[i] = type[i];
					}
					Method method = null;
					for(Method m : NPCActions.class.getMethods()) {
						if(m.getName().equals(methodName)) {
							method = m;
							break;
						}
					}
					
					NPCMethod npcMethod = new NPCMethod(method, args);
					if("default".equalsIgnoreCase((String) key))
						defaultMethod = npcMethod;
					else 
						methods.put((String) key, npcMethod);
				}

				NPCType npc = new NPCType(methods,defaultMethod);
				types.put(results.getString("Name"), npc);
				
			}
		} catch (SQLException | ParseException e) {
			e.printStackTrace();
		}
	}

	public static NPCType get(String name) {
		return types.get(name);
	}

	private HashMap<String, NPCMethod> methods;
	private NPCMethod defaultMethod;

	private NPCType(HashMap<String, NPCMethod> methods, NPCMethod defaultMethod) {
		this.methods = methods;
		this.defaultMethod = defaultMethod;
	}

	public void onInteract(Player player) {
		try {
 			RPGPlayer rPlayer = RPGPlayerManager.getPlayer(player);
			for (String flag : methods.keySet()) {
				if (rPlayer.hasFlag(flag)) {
					NPCMethod method = methods.get(flag);
					Object[] args = method.args.clone();
					args[0] = player;
					method.method.invoke(null, args);
					return;
				}
			}
			if (defaultMethod != null) {
				Object[] args = defaultMethod.args.clone();
				args[0] = player;
				defaultMethod.method.invoke(null, args);
			}
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
	}
}
