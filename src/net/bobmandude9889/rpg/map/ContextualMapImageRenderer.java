package net.bobmandude9889.rpg.map;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.map.MapCanvas;
import org.bukkit.map.MapRenderer;
import org.bukkit.map.MapView;

import net.bobmandude9889.rpg.RPG;

public class ContextualMapImageRenderer extends MapRenderer {

	private static BufferedImage MAP_IMAGE;
	private static BufferedImage ARROW;
	private static int xOff = 0;
	private static int yOff = 0;
	
	private boolean send = false;
	private int count = 0;
	
	static {
		try {
			MAP_IMAGE = ImageIO.read(new File(RPG.instance.getDataFolder(),"map.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		ARROW = new BufferedImage(7,8,BufferedImage.TYPE_INT_ARGB);
		Graphics g = ARROW.getGraphics();
		
		for(int y = 0; y < 6; y++) {
			int i = (int) Math.ceil((y + 1)/2d);
			g.setColor(Color.WHITE);
			g.fillRect(4 - i, y + 1, i * 2 - 1, 1);
			g.setColor(Color.BLACK);
			g.fillRect(4 - (i + 1), y + 1, 1, 1);
			g.fillRect(4 + (i - 1), y + 1, 1, 1);
		}
		g.fillRect(3, 0, 1, 1);
		g.fillRect(0, 7, 7, 1);
	}
	
	public ContextualMapImageRenderer() {
		super(true);
	}
	
	@Override
	public void render(MapView view, MapCanvas canvas, Player player) {
		if(send) {
			Location location = player.getLocation();
			for(int y = 0; y < 128; y++) {
				for(int x = 0; x < 128; x++) {
					canvas.setPixel(x, y, (byte) 32);
				}
			}
			int x = location.getBlockX() - xOff;
			int y = location.getBlockZ() - yOff;
			if(x < MAP_IMAGE.getWidth() && y < MAP_IMAGE.getHeight()) {
				BufferedImage sub = new BufferedImage(128,128,BufferedImage.TYPE_INT_ARGB);
				Graphics g1 = sub.getGraphics();
				g1.drawImage(MAP_IMAGE.getSubimage(x < 0 ? 0 : x, y < 0 ? 0 : y, MAP_IMAGE.getWidth() - x > 128 ? 128 : MAP_IMAGE.getWidth() - x, MAP_IMAGE.getHeight() - y > 128 ? 128 : MAP_IMAGE.getHeight() - y), 0, 0, null);
				BufferedImage img = new BufferedImage(9,10,BufferedImage.TYPE_INT_ARGB);
				Graphics2D g = img.createGraphics();
				AffineTransform trans = new AffineTransform();
				trans.setToIdentity();
				float yaw = player.getLocation().getYaw();
				trans.rotate(Math.toRadians(yaw < 0 ? (yaw - 180) : -(180 - yaw)),5,5);
				g.setTransform(trans);
				g.drawImage(ARROW, 1, 1, null);
				g.setTransform(new AffineTransform());
				g1.drawImage(img, 64 - img.getWidth(), 64 - img.getHeight(), null);
				canvas.drawImage(0, 0, sub);
			}
			send = false;
			count = 0;
		} else {
			count++;
			send = count >= 5;
		}
	}

}
