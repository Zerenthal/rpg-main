package net.bobmandude9889.rpg.mobRegion;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

import com.sk89q.worldguard.bukkit.RegionContainer;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import net.bobmandude9889.rpg.util.Util;

public class MobRegion {

	public String name;
	public List<EntityType> allowed;
	public int max;
	public int cooldown;
	
	private long spawnTime;

	public MobRegion(ConfigurationSection sect) {
		this.name = sect.getName();
		this.max = sect.getInt("max");
		this.cooldown = sect.getInt("cooldown");
		this.allowed = new ArrayList<>();
		for (String mob : sect.getStringList("allowed")) {
			allowed.add(EntityType.valueOf(mob));
		}
		this.spawnTime = 0;
	}

	public MobRegion(String name) {
		this.name = name;
		this.max = Integer.MAX_VALUE;
		this.cooldown = 0;
		this.allowed = new ArrayList<>();
		this.spawnTime = 0;
	}
	
	public boolean isAllowed(Entity e) {
		if(Util.isInRegion(e.getLocation(), this.name)) {
			if(this.countMobs(e.getWorld()) < this.max) {
				if(allowed.contains(e.getType())) {
					if(System.currentTimeMillis() > spawnTime) {
						spawnTime = System.currentTimeMillis() + new Random().nextInt(cooldown - 5) + 5;
						return true;
					} else {
						return false;
					}
				}
			}
			spawnTime = System.currentTimeMillis() + this.cooldown;
		}
		
		return false;
	}

	public void addType(EntityType type) {
		if (!allowed.contains(type))
			allowed.add(type);
	}

	public void removeType(EntityType type) {
		allowed.remove(type);
	}

	public void updateSection(ConfigurationSection sect) {
		List<String> mobs = new ArrayList<>();
		for (EntityType type : allowed) {
			mobs.add(type.name());
		}
		sect.set("allowed", mobs);
		sect.set("max", this.max);
		sect.set("cooldown", this.cooldown);
	}
	
	public int countMobs(World world) {
		RegionContainer container = Util.getWorldGuard().getRegionContainer();
		RegionManager manager = container.get(world);
		ProtectedRegion region = manager.getRegion(this.name);
		int count = 0;
		for(Entity e : world.getEntities()) {
			Location loc = e.getLocation();
			if(e.getType().isAlive() && !e.getType().equals(EntityType.PLAYER) && region.contains(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ())) {
				count++;
			}
		}
		return count;
	}
	
	public void spawn(World world) {
		if(allowed.size() > 0) {
			Random r = new Random();
			int i = r.nextInt(allowed.size());
			EntityType type = allowed.get(i);
			RegionContainer container = Util.getWorldGuard().getRegionContainer();
			RegionManager manager = container.get(world);
			ProtectedRegion region = manager.getRegion(this.name);
			if(region != null) {
				world.spawnEntity(Util.randomLocation(region.getMinimumPoint().getBlockX(), region.getMinimumPoint().getBlockZ(), region.getMaximumPoint().getBlockX(), region.getMaximumPoint().getBlockZ(), world), type);
			}
		}
	}
	
	@Override
	public String toString() {
		return name;
	}
	
}
