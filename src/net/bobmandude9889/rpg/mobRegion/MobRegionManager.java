package net.bobmandude9889.rpg.mobRegion;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;

import com.sk89q.worldedit.blocks.metadata.MobType;
import com.sk89q.worldguard.bukkit.RegionContainer;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import net.bobmandude9889.rpg.RPG;
import net.bobmandude9889.rpg.command.Command;
import net.bobmandude9889.rpg.command.TabComplete;
import net.bobmandude9889.rpg.command.TabCompleteHandler;
import net.bobmandude9889.rpg.command.TabCompleteMethod;
import net.bobmandude9889.rpg.command.TabCompleteNode;
import net.bobmandude9889.rpg.util.Util;
import net.md_5.bungee.api.ChatColor;

public class MobRegionManager implements Listener, TabComplete {

	// Need to add a repeating task to spawn mobs in a region
	
	private static YamlConfiguration config;
	private static File file;
	private static World world;
	
	private static List<MobRegion> regions;
	
	public static void init() {
		file = new File(RPG.dataFolder, "regions.yml");
		try {
			file.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		config = YamlConfiguration.loadConfiguration(file);
		loadConfig();
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(RPG.instance, new Runnable(){
			@Override
			public void run() {
				for(MobRegion region : regions) {
					region.spawn(world);
				}
			}
		}, 20l, 20l);
	}
	
	public static void loadConfig() {
		regions = new ArrayList<>();
		try {
			config.load(file);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
		world = Bukkit.getWorld(config.getString("world"));
		if(!config.isConfigurationSection("regions"))
			config.createSection("regions");
		ConfigurationSection sect = config.getConfigurationSection("regions");
		for(String region : sect.getValues(false).keySet()) {
			if(sect.isConfigurationSection(region)) {
				regions.add(new MobRegion(sect.getConfigurationSection(region)));
			}
		}
	}
	
	public static void saveConfig() {
		config.set("world", world.getName());
		ConfigurationSection sect = config.getConfigurationSection("regions");
		for(String key : sect.getKeys(false)) {
			sect.set(key, null);
		}
		for(MobRegion region : regions) {
			region.updateSection(sect.createSection(region.name));
		}
		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@EventHandler
	public void onMobSpawn(EntitySpawnEvent e) {
		if(e.getEntityType().equals(EntityType.DROPPED_ITEM))
			return;
		
		boolean allowed = false;
		for(MobRegion r : regions) {
			if(r.isAllowed(e.getEntity())) {
				allowed = true;
				break;
			}
		}
		e.setCancelled(!allowed);
	}
	
	@Command
	public boolean MobRegionCommand(String[] args, CommandSender sender) {
		try {
			switch(args[0].toLowerCase()) {
			case "add":
				regions.add(new MobRegion(args[1]));
				sender.sendMessage(ChatColor.GREEN + "Added region " + args[1]);
				saveConfig();
				break;
			case "remove":
				for(MobRegion region : regions) {
					if(region.name.equalsIgnoreCase(args[1])) {
						regions.remove(region);
						break;
					}
				}
				sender.sendMessage(ChatColor.RED + "Removed region " + args[1]);
				saveConfig();
				break;
			case "mob":
				for(MobRegion region : regions) {
					if(region.name.equalsIgnoreCase(args[1])) {
						if(args[2].equalsIgnoreCase("add")) {
							EntityType type = EntityType.valueOf(args[3].toUpperCase());
							region.addType(type);
							saveConfig();
							sender.sendMessage(ChatColor.GREEN + "Added mob type " + type + " to region " + region.name);
							break;
						} else if(args[2].equalsIgnoreCase("remove")) {
							EntityType type = EntityType.valueOf(args[3].toUpperCase());
							region.removeType(type);
							saveConfig();
							sender.sendMessage(ChatColor.RED + "Removed mob type " + type + " from region " + region.name);
							break;
						}
						break;
					}
				}
				break;
			case "setmax":
				for(MobRegion region : regions) {
					if(region.name.equalsIgnoreCase(args[1])) {
						region.max = Integer.parseInt(args[2]);
						saveConfig();
						sender.sendMessage(ChatColor.GREEN + "Set max mob count to " + args[2]);
						break;
					}
				}
				break;
			case "setcooldown":
				for(MobRegion region : regions) {
					if(region.name.equalsIgnoreCase(args[1])) {
						region.cooldown = Integer.parseInt(args[2]);
						saveConfig();
						sender.sendMessage(ChatColor.GREEN + "Set cooldown to " + args[2]);
						break;
					}
				}
				break;
			default:
				return false;
			}
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public TabCompleteNode tabComplete() {
		TabCompleteNode parent = new TabCompleteNode();
		parent.addChild("add").addChild((player) -> {
			RegionContainer container = Util.getWorldGuard().getRegionContainer();
			RegionManager manager = container.get(player.getWorld());
			List<String> regionStrings = new ArrayList<>();
			Map<String, ProtectedRegion> regions = manager.getRegions();
			for (ProtectedRegion region : regions.values()) {
				regionStrings.add(region.getId());
			}
			return regionStrings;
		});
		TabCompleteMethod regionMethod = player -> TabCompleteHandler.objectsToStrings(MobRegionManager.regions);
		parent.addChild("remove").addChild(regionMethod);
		parent.addChild("mob").addChild(regionMethod).addChild("add", "remove").addChild(TabCompleteHandler.objectsToStrings(MobType.values()));
		parent.addChild("setmax").addChild(regionMethod);
		parent.addChild("setcooldown").addChild(regionMethod);
		return parent;
	}
	
}
