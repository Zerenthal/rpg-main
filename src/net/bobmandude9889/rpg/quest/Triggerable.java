package net.bobmandude9889.rpg.quest;

import java.util.HashMap;
import java.util.List;

public class Triggerable {
	
	TriggerType[] triggers;
	List<HashMap<String,String>> paramMap;

	public Triggerable(TriggerData data) {
		this.triggers = data.triggers;
		this.paramMap = data.params;
	}
	
	public boolean canTrigger(TriggerType type, HashMap<String,String> params) {
		boolean canTrigger = false;
		for(int i = 0; i < triggers.length; i++) {
			if(triggers[i].equals(type)) {
				if(params.size() == paramMap.get(i).size()) {
					boolean foundCounter = false;
					for(String key : params.keySet()) {
						if(!params.get(key).equalsIgnoreCase(paramMap.get(i).get(key))) {
							foundCounter = true;
							break;
						}
					}
					if(!foundCounter)
						canTrigger = true;
				}
			}
		}
		
		return canTrigger;
	}
	
}
