package net.bobmandude9889.rpg.quest;

import java.util.HashMap;

import org.bukkit.configuration.ConfigurationSection;

import net.bobmandude9889.rpg.player.RPGPlayer;

public class QuestManager {
	
	HashMap<Quest,Integer> questGoals;
	
	RPGPlayer player;
	
	public QuestManager() {
		questGoals = new HashMap<>();
	}

	public void load(RPGPlayer player) {
		this.player = player;
		ConfigurationSection questSect = player.config.getConfigurationSection("quests");
		if(questSect != null) {
			for(String questKey : questSect.getKeys(false)) {
				Quest quest = Quest.getQuest(Integer.parseInt(questKey));
				questGoals.put(quest, questSect.getInt(questKey));
			}
		}
	}
	
	public void save() {
		if(!player.config.contains("quests")) 
			player.config.createSection("quests");
		ConfigurationSection questSect = player.config.getConfigurationSection("quests");
		for(Quest quest : questGoals.keySet()) {
			questSect.set(quest.id + "", questGoals.get(quest));
		}
	}
	
	public void attemptTrigger(TriggerType type, HashMap<String,String> params) {
		for(Quest quest : questGoals.keySet()) {
//			System.out.println("goal:" + questGoals.get(quest));
//			System.out.println("goal size:" + quest.goals.size());
//			System.out.println("index: " + questGoals.get(quest));
			Goal goal = quest.getGoal(questGoals.get(quest));
			if(goal != null && goal.canTrigger(type, params)) {
				goal.complete(player.player);
				if(quest.hasNextGoal(goal)) {
					int next = quest.getNextGoal(goal);
					questGoals.put(quest, next);
					quest.getGoal(next).start(player.player);
				} else {
					questGoals.put(quest, -1);
				}
			}
		}
		
		for(Quest quest : Quest.getQuests()) {
			if(!questGoals.containsKey(quest) && quest.canTrigger(type, params)) {
				questGoals.put(quest, 0);
				quest.startAction.execute(this.player.player);
			}
		}
	}
	
	public boolean startedQuest(Quest quest) {
		return questGoals.containsKey(quest);
	}
	
	public boolean finishedQuest(Quest quest) {
		if(!questGoals.containsKey(quest))
			return false;
		return getCurrentGoal(quest) == null;
	}
	
	public Goal getCurrentGoal(Quest quest) {
		return quest.getGoal(questGoals.get(quest));
	}
	
}
