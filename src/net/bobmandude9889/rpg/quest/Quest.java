package net.bobmandude9889.rpg.quest;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.bukkit.configuration.ConfigurationSection;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import net.bobmandude9889.rpg.RPG;
import net.bobmandude9889.rpg.dialogue.DialogueAction;
import net.bobmandude9889.rpg.util.sql.DBConn;
import net.bobmandude9889.rpg.util.sql.SqlStatement;

public class Quest extends Triggerable {

	static List<Quest> quests;

	public int id;
	public String name;
	public String description;
	public String startDescription;
	public List<Goal> goals;

	public DialogueAction startAction;
	public DialogueAction endAction;

	static {
		try {
			quests = new ArrayList<>();

			DBConn conn = RPG.sqlMCConn;
			SqlStatement stmt = new SqlStatement(conn);

			ResultSet results = stmt.select("Quests", "1 ORDER BY Id");
			while (results.next()) {
				ResultSet goalResults = stmt.select("Goals", "ParentQuest=" + results.getInt("Id") + " ORDER BY Id");
				List<Goal> goals = new ArrayList<>();

				String startTriggerString = results.getString("StartTrigger");
				TriggerData trigger = getTriggerData(startTriggerString);

				DialogueAction startAction = loadActionFromString(results.getString("StartAction"));
				DialogueAction endAction = loadActionFromString(results.getString("EndAction"));

				Quest quest = new Quest(results.getInt("Id"), results.getString("Name"), results.getString("Description"), results.getString("StartDescription"), goals, trigger, startAction, endAction);
				while (goalResults.next()) {
					String triggerString = goalResults.getString("EndTrigger");
					TriggerData type = getTriggerData(triggerString);

					DialogueAction start = loadActionFromString(goalResults.getString("StartAction"));
					DialogueAction complete = loadActionFromString(goalResults.getString("EndAction"));

					quest.goals.add(new Goal(goalResults.getInt("Id"), type, goalResults.getString("Name"), goalResults.getString("Description"), quest, start, complete));
				}
				quests.add(quest);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static TriggerData getTriggerData(String trigger) {
		JSONArray jsonObj = null;
		try {
			jsonObj = (JSONArray) new JSONParser().parse(trigger);
			TriggerType[] types = new TriggerType[jsonObj.size()];
			List<HashMap<String,String>> paramMap = new ArrayList<>();
			for (int i = 0; i < jsonObj.size(); i++) {
				JSONObject triggerObj = (JSONObject) jsonObj.get(i);
				types[i] = TriggerType.valueOf(triggerObj.get("action").toString());
				HashMap<String,String> params = new HashMap<>();
				for(Object key : triggerObj.keySet()) {
					if(!key.equals("action"))
						params.put((String) key, (String) triggerObj.get(key));
				}
				paramMap.add(params);
			}
			List<TriggerType> typeList = new ArrayList<>();
			Collections.addAll(typeList, types);
			System.out.println(typeList + " " + paramMap);
			TriggerData data = new TriggerData(types, paramMap);
			return data;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("unused")
	@Deprecated
	private static DialogueAction loadActionFromSection(ConfigurationSection sect) {
		if (sect != null) {
			DialogueAction action = new DialogueAction();
			for (String key : sect.getKeys(false)) {
				action.set(key, sect.get(key));
			}
			return action;
		}
		return null;
	}

	private static DialogueAction loadActionFromString(String raw) {
		if (raw == null)
			return null;

		DialogueAction action = new DialogueAction();
		JSONObject jsonObj;
		try {
			jsonObj = (JSONObject) new JSONParser().parse(raw);
			for (Object key : jsonObj.keySet()) {
				action.set(key.toString(), jsonObj.get(key));
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return action;
	}

	public static List<Quest> getQuests() {
		return quests;
	}

	public static Quest getQuest(int id) {
		for (Quest quest : quests) {
			if (quest.id == id)
				return quest;
		}
		return null;
	}

	public Quest(int id, String name, String description, String startDescription, List<Goal> goals, TriggerData trigger, DialogueAction startAction, DialogueAction endAction) {
		super(trigger);
		this.id = id;
		this.name = name;
		this.description = description;
		this.startDescription = startDescription;
		this.goals = goals;
		this.startAction = startAction;
		this.endAction = endAction;
	}

	public Goal getGoal(String name) {
		for (Goal goal : goals) {
			if (goal.name.equals(name))
				return goal;
		}
		return null;
	}

	public Goal getGoal(int i) {
		if(this.goals.size() > i && i >= 0) {
			return goals.get(i);
		}
		return null;
	}

	public boolean hasNextGoal(Goal current) {
		int index = goals.indexOf(current);
		return (index < goals.size() - 1);
	}

	public int getNextGoal(Goal current) {
		int index = goals.indexOf(current);
		if (index < goals.size() - 1) {
			return index + 1;
		}
		return -1;
	}

}
