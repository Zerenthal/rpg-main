package net.bobmandude9889.rpg.quest;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import net.bobmandude9889.rpg.command.Command;
import net.bobmandude9889.rpg.player.RPGPlayer;
import net.bobmandude9889.rpg.player.RPGPlayerManager;
import net.bobmandude9889.rpg.util.BookUtil;
import net.bobmandude9889.rpg.util.tellraw.TellrawBookText;
import net.bobmandude9889.rpg.util.tellraw.TellrawBuilder;
import net.bobmandude9889.rpg.util.tellraw.TellrawChatText;
import net.bobmandude9889.rpg.util.tellraw.TellrawText;

public class BookCommand {

	@Command
	public void openBook(CommandSender sender) {
		Player player = (Player) sender;
		RPGPlayer rPlayer = RPGPlayerManager.getPlayer(player);
		QuestManager manager = rPlayer.questManager;
		String[] pages = new String[Quest.quests.size() + 1];

		TellrawBuilder builder = new TellrawBuilder();
		builder.addText(new TellrawChatText("Current Quests\n\n").setColor(ChatColor.BOLD));
		for (Quest quest : Quest.quests) {
			if (manager.startedQuest(quest) && !manager.finishedQuest(quest)) {
				builder.addText(new TellrawChatText("\u25B6 ").setColor(ChatColor.BLACK));
				builder.addText(new TellrawBookText(quest.name).setGotoPageOnClick(Quest.quests.indexOf(quest) + 2).setShowTextOnHover(new TellrawText("Click to go to quest page.")).setColor(ChatColor.DARK_GRAY));
				builder.addText(new TellrawBookText(" (p." + (Quest.quests.indexOf(quest) + 2) + ")\n").setGotoPageOnClick(Quest.quests.indexOf(quest) + 2).setShowTextOnHover(new TellrawText("Click to go to quest page.")).setColor(ChatColor.GRAY));
			}
		}
		pages[0] = builder.getJSON();

		for (Quest quest : Quest.quests) {
			builder = new TellrawBuilder();
			builder.addText(new TellrawBookText("[Back Home]").setGotoPageOnClick(1).setColor(ChatColor.GRAY).setShowTextOnHover(new TellrawText("Click to go back to the first page.")));
			TellrawChatText name = new TellrawChatText("\n[" + quest.name + "]\n\n").setColor(ChatColor.GOLD).setColor(ChatColor.BOLD);
			if (manager.startedQuest(quest) && manager.finishedQuest(quest))
				name.setColor(ChatColor.STRIKETHROUGH);
			builder.addText(name);
			builder.addText(new TellrawChatText("\"" + quest.description + "\"\n\n").setColor(ChatColor.GRAY));
			if (!manager.startedQuest(quest)) {
				builder.addText(new TellrawChatText(quest.startDescription + "\n\n").setColor(ChatColor.DARK_GRAY));
				builder.addText(new TellrawChatText("Progress: ").setColor(ChatColor.DARK_GRAY).setColor(ChatColor.BOLD), new TellrawChatText("0%").setColor(ChatColor.RED));
				builder.addText(createBoxes(0));
			} else if (manager.finishedQuest(quest)) {
				builder.addText(new TellrawChatText("Completed\n\n").setColor(ChatColor.DARK_GREEN));
				builder.addText(new TellrawChatText("Progress: ").setColor(ChatColor.DARK_GRAY).setColor(ChatColor.BOLD), new TellrawChatText("100%").setColor(ChatColor.DARK_GREEN));
				builder.addText(createBoxes(100));
			} else {
				builder.addText(new TellrawChatText(manager.getCurrentGoal(quest).desc + "\n\n").setColor(ChatColor.DARK_GRAY));
				System.out.println(manager.questGoals.get(quest) + 1);
				System.out.println(quest.goals.size() + 1);
				double progress = (double) (manager.questGoals.get(quest) + 1) / (double) (quest.goals.size() + 1);
				progress *= 100d;
				TellrawChatText progressText = new TellrawChatText((int) progress + "%");
				if (progress <= 33) {
					progressText.setColor(ChatColor.YELLOW);
				} else if (progress <= 66) {
					progressText.setColor(ChatColor.GOLD);
				} else {
					progressText.setColor(ChatColor.DARK_GREEN);
				}
				builder.addText(new TellrawChatText("Progress: ").setColor(ChatColor.DARK_GRAY).setColor(ChatColor.BOLD), progressText);
				builder.addText(createBoxes(progress));
			}
			pages[Quest.quests.indexOf(quest) + 1] = builder.getJSON();
		}

		ItemStack item = BookUtil.createBook(pages);
		BookUtil.openBook(item, player);
	}

	public TellrawChatText createBoxes(double progress) {
		int boxes = 13;
		int completedBoxes = (int) Math.round((progress / 100.0) * boxes);
		String boxesOutput = ChatColor.DARK_GRAY + "[";
		for (int i = 0; i < boxes; i++) {
			if (completedBoxes > 0) {
				completedBoxes--;
				boxesOutput += ChatColor.GREEN + "\u2b1b";
			} else {
				boxesOutput += ChatColor.GRAY + "\u2b1b";

			}
		}
		boxesOutput += ChatColor.DARK_GRAY + "]";
		return new TellrawChatText(boxesOutput);
	}

}
