package net.bobmandude9889.rpg.quest;

import org.bukkit.entity.Player;

import net.bobmandude9889.rpg.dialogue.DialogueAction;

public class Goal extends Triggerable {

	Quest parent;
	
	public int id;
	public String desc;
	public String name;

	DialogueAction start;
	DialogueAction complete;
	
	public Goal(int id, TriggerData trigger, String name, String desc, Quest parent, DialogueAction start, DialogueAction complete) {
		super(trigger);
		this.name = name;
		this.desc = desc;
		this.parent = parent;
		this.start = start;
		this.complete = complete;
	}
	
	public void start(Player player) {
		start.execute(player);
	}
	
	public void complete(Player player) {
		complete.execute(player);
	}
	
}