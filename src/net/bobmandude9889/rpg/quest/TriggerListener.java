package net.bobmandude9889.rpg.quest;

import java.util.HashMap;
import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;

import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import net.bobmandude9889.rpg.player.RPGPlayer;
import net.bobmandude9889.rpg.player.RPGPlayerManager;
import net.bobmandude9889.rpg.util.Util;

@SuppressWarnings("deprecation")
public class TriggerListener implements Listener {

	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		RegionManager regionManager = Util.getWorldGuard().getRegionManager(e.getPlayer().getWorld());
		Set<ProtectedRegion> from = regionManager.getApplicableRegions(e.getFrom()).getRegions();
		Set<ProtectedRegion> to = regionManager.getApplicableRegions(e.getTo()).getRegions();
		RPGPlayer player = RPGPlayerManager.getPlayer(e.getPlayer());
		for (ProtectedRegion region : from) {
			if (!to.contains(region)) {
				HashMap<String,String> params = new HashMap<>();
				params.put("regionName", region.getId());
				player.questManager.attemptTrigger(TriggerType.leaveRegion, params);
			} else {
				to.remove(region);
				from.remove(region);
			}
		}
		for (ProtectedRegion region : to) {
			if (!from.contains(region)) {
				HashMap<String,String> params = new HashMap<>();
				params.put("regionName", region.getId());
				player.questManager.attemptTrigger(TriggerType.enterRegion, params);
			}
		}
	}

	@EventHandler
	public void onPickup(PlayerPickupItemEvent e) {
		RPGPlayer player = RPGPlayerManager.getPlayer(e.getPlayer());
		ItemStack item = e.getItem().getItemStack();
		
		HashMap<String,String> params = new HashMap<>();
		params.put("type", item.getType().name());
		
		player.questManager.attemptTrigger(TriggerType.receiveItem, params);
		
		if (item.getItemMeta() != null && item.getItemMeta().getDisplayName() != null) {
			params.put("name", ChatColor.stripColor(item.getItemMeta().getDisplayName()));
			player.questManager.attemptTrigger(TriggerType.receiveItem, params);
		}
	}

}
