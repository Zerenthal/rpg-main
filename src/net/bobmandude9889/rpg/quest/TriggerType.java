package net.bobmandude9889.rpg.quest;

public enum TriggerType {
	//enterRegion <name>
	enterRegion,
	//leaveRegion <name>
	leaveRegion,
	//gainFlag <name>
	gainFlag,
	//loseFlag <name>
	loseFlag,
	//receiveItem <id> [data] [name]
	receiveItem,
	//sellItem <id> [data] [name]
	sellItem,
	//buyItem <id> [data] [name]
	buyItem,
	//reachLevel <level> <skill>
	reachLevel,
	//reachXp <xp> <skill>
	reachXp,
	//giveNPCItem <id> [data] [name]
	giveNPCItem;
	
}
