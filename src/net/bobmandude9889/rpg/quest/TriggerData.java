package net.bobmandude9889.rpg.quest;

import java.util.HashMap;
import java.util.List;

public class TriggerData {
	
	public TriggerType[] triggers;
	public List<HashMap<String,String>> params;

	public TriggerData(TriggerType[] triggers, List<HashMap<String,String>> params) {
		this.triggers = triggers;
		this.params = params;
	}

}
