package net.bobmandude9889.rpg.vault;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Scanner;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.bobmandude9889.rpg.RPG;
import net.bobmandude9889.rpg.gui.GUI;
import net.bobmandude9889.rpg.gui.GUIHandler;
import net.bobmandude9889.rpg.gui.GUITemplate;
import net.bobmandude9889.rpg.gui.action.GUIAction;
import net.bobmandude9889.rpg.gui.event.GUIClickEvent;
import net.bobmandude9889.rpg.player.RPGPlayer;
import net.bobmandude9889.rpg.player.RPGPlayerManager;
import net.bobmandude9889.rpg.util.ItemBuilder;
import net.bobmandude9889.rpg.util.Util;
import net.md_5.bungee.api.ChatColor;

public class VaultManager implements Listener {

	public static HashMap<Player, Vault> vaults;

	private static File dataFolder;

	private static GUITemplate coinTemp;

	public VaultManager() {
		vaults = new HashMap<Player, Vault>();
		RPG.instance.getDataFolder().mkdir();
		dataFolder = new File(RPG.instance.getDataFolder(), "vaults");
		dataFolder.mkdir();

		coinTemp = new GUITemplate(27, "Coin Transfer");

		// Disable changes
		coinTemp.setAllowChange(false);

		// Iron fence border
		for (int i = 0; i < 9; i++) {
			ItemStack fence = new ItemBuilder(Material.IRON_FENCE).setName(" ").getItem();
			coinTemp.setItem(i, fence);
			if (i % 8 != 0) {
				coinTemp.setItem(i + 18, fence);
			}
		}

		// Coin amount buttons
		for (int i = 0; i < 9; i++) {
			ItemBuilder btnBuilder = new ItemBuilder(Util.createCoinItem()).setLore("&cLeft click to deposit.", "&cRight click to withdraw.");
			if (i == 8) {
				btnBuilder.setName("&eAll coins");
				coinTemp.setButton(i + 9, btnBuilder.getItem(), new GUIAction() {

					@Override
					public void onClick(GUIClickEvent e) {
						Vault vault = vaults.get(e.player);
						RPGPlayer rPlayer = RPGPlayerManager.getPlayer(e.player);
						if (e.leftClick) {
							vault.addCoins(rPlayer.getCoins());
							rPlayer.setCoins(BigInteger.ZERO);
						} else if (e.rightClick) {
							rPlayer.addCoins(vault.getCoins());
							vault.setCoins(BigInteger.ZERO);
						}
						updateTitle(GUIHandler.instance.guis.get(e.player));
					}
				});
			} else {
				String name = "1";
				for (int j = 0; j < i; j++) {
					name += "0";
				}
				final BigInteger amount = new BigInteger(name);
				name = Util.formatInt(name);
				name += " coin" + (i == 0 ? "" : "s");
				btnBuilder.setName(ChatColor.YELLOW + name);
				coinTemp.setButton(i + 9, btnBuilder.getItem(), new GUIAction() {

					@Override
					public void onClick(GUIClickEvent e) {
						Vault vault = vaults.get(e.player);
						RPGPlayer rPlayer = RPGPlayerManager.getPlayer(e.player);
						if (e.leftClick && rPlayer.getCoins().subtract(amount).compareTo(BigInteger.ZERO) > -1) {
							vault.addCoins(amount);
							rPlayer.removeCoins(amount);
						} else if (e.rightClick && vault.getCoins().subtract(amount).compareTo(BigInteger.ZERO) > -1) {
							rPlayer.addCoins(amount);
							vault.removeCoins(amount);
						}
						updateTitle(GUIHandler.instance.guis.get(e.player));
					}
				});
			}
		}

		// Back buttons
		GUIAction backAction = new GUIAction() {
			@Override
			public void onClick(GUIClickEvent e) {
				GUIHandler.instance.closeGUI(e.player);
				open(e.player, 0);
			}
		};

		ItemStack btn = new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 7).setName("&aBack").getItem();
		coinTemp.setButton(18, btn, backAction);
		coinTemp.setButton(26, btn, backAction);
	}

	private static void updateTitle(final GUI gui) {
		final Vault vault = vaults.get(gui.player);
		final RPGPlayer player = RPGPlayerManager.getPlayer(gui.player);
		if (gui != null) {
			Bukkit.getScheduler().scheduleSyncDelayedTask(RPG.instance, new Runnable() {
				@Override
				public void run() {
					gui.setName("Balance: &e" + Util.shortInt(vault.getCoins().toString()) + " &8Coins: &e" + Util.shortInt(player.getCoins().toString()));
				};
			});
		}
	}

	public static Vault getVault(Player player) {
		if (!vaults.containsKey(player))
			loadPlayerVault(player);

		return vaults.get(player);
	}

	public static void open(Player player, int page) {
		if (!vaults.containsKey(player))
			loadPlayerVault(player);

		Vault vault = vaults.get(player);

		ItemStack[] contents = vault.getPageContents(page);

		Inventory inv = Bukkit.createInventory(player, Vault.pageSize + 9, ChatColor.DARK_RED + "Vault Page: " + ChatColor.DARK_GRAY + (page + 1));

		for (int i = 0; i < contents.length; i++) {
			inv.setItem(i, contents[i]);
		}

		for (VaultButton btn : VaultButton.values()) {
			ItemStack item = new ItemStack(btn.type, 1);

			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(btn.name);
			item.setItemMeta(meta);
			item.setDurability(btn.data);

			inv.setItem(btn.index + Vault.pageSize, item);
		}

		int index = 4 + Vault.pageSize;
		inv.setItem(index, new ItemBuilder(Util.createCoinItem()).setName("&e" + Util.formatInt(vault.getCoins().toString()) + " coins").setLore("&aClick to deposit/withdraw.").getItem());

		vault.page = page;

		player.openInventory(inv);

		vault.isOpen = true;
	}

	public static void openCoinTransfer(Player player) {
		GUI gui = coinTemp.open(player);
		updateTitle(gui);
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		loadPlayerVault(e.getPlayer());
	}

	public void onLeave(PlayerQuitEvent e) {
		savePlayerVault(e.getPlayer());
	}

	@EventHandler
	public void inventoryClick(InventoryClickEvent e) {
		if (!vaults.containsKey(e.getWhoClicked()))
			return;
		Vault vault = vaults.get(e.getWhoClicked());
		ItemStack item = null;
		if (e.getClickedInventory() != null)
			item = e.getClickedInventory().getItem(e.getSlot());
		if (vault.isOpen) {
			if (item != null && Util.isCoin(item)) {
				openCoinTransfer((Player) e.getWhoClicked());
			} else if (vault.isOpen && e.getSlot() >= Vault.pageSize) {
				int i = e.getSlot() - Vault.pageSize;
				for (VaultButton btn : VaultButton.values()) {
					if (btn.index == i) {
						e.setCancelled(true);
						if (btn.action != null)
							btn.action.run(vault);
					}
				}
			}
		}
	}

	@EventHandler
	public void inventoryClose(InventoryCloseEvent e) {
		if (!vaults.containsKey(e.getPlayer()))
			return;
		Vault vault = vaults.get(e.getPlayer());
		if (vault.isOpen) {
			vault.setPageContents(vault.page, e.getInventory().getContents());
			vault.isOpen = false;
		}
		savePlayerVault((Player) e.getPlayer());
	}

	public static void loadPlayerVault(Player player) {
		ItemStack[] contents = new ItemStack[Vault.pages * Vault.pageSize];
		BigInteger coins = new BigInteger("0");

		File playerFile = new File(dataFolder, player.getUniqueId() + ".bv");
		if (playerFile.exists()) {

			try {
				Scanner fileIn = new Scanner(playerFile);

				boolean firstLine = true;

				String b64 = "";
				String coinString = "";
				while (fileIn.hasNextLine()) {
					String line = fileIn.nextLine();
					if (firstLine) {
						coinString = line;
						firstLine = false;
					} else
						b64 += line + (fileIn.hasNextLine() ? "\n" : "");
				}

				contents = Util.itemStackArrayFromBase64(b64.trim());
				coins = new BigInteger(coinString);
				fileIn.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

		Vault vault = new Vault(player, contents, coins);
		vaults.put(player, vault);
	}

	public static void savePlayerVault(Player player) {
		if (!vaults.containsKey(player))
			return;

		File playerFile = new File(dataFolder, player.getUniqueId() + ".bv");
		if (!playerFile.exists()) {
			try {
				playerFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		try {
			PrintWriter fileOut = new PrintWriter(playerFile);
			Vault vault = vaults.get(player);
			fileOut.print(vault.coins.toString() + "\n" + Util.itemStackArrayToBase64(vault.contents));
			fileOut.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

}