package net.bobmandude9889.rpg.vault;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.bobmandude9889.rpg.command.Command;

public class VaultCommand {

	@Command
	public void vault(CommandSender sender, String[] args) {
		if (sender instanceof Player)
			VaultManager.open((Player) sender, 0);
	}
	
}
