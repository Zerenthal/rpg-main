package net.bobmandude9889.rpg.vault;

import java.math.BigInteger;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Vault {
	
	public ItemStack[] contents;
	public BigInteger coins;
	
	public Player player;
	public int page;
	public boolean isOpen;
	
	public static int pages = 5;
	public static int pageSize = 45;
	
	public Vault(Player player, ItemStack[] contents, BigInteger coins) {
		this.contents = contents;
		this.player = player;
		this.coins = coins;
	}
	
	public ItemStack[] getPageContents(int page){
		ItemStack[] pageContents = new ItemStack[pageSize];
		int start = page * pageSize;
		for(int i = start; i < start + pageSize; i++){
			pageContents[i - start] = contents[i];
		}
		return pageContents;
	}
	
	public void setPageContents(int page, ItemStack[] contents){
		for(int i = 0; i < pageSize; i++){
			this.contents[i + (page * pageSize)] = contents[i];
		}
	}
	
	public void updatePage(){
		setPageContents(page, player.getOpenInventory().getTopInventory().getContents());
	}
	
	public void close(){
		if(isOpen && player.getOpenInventory() != null){
			isOpen = false;
			updatePage();
			player.closeInventory();
		}
	}

	public BigInteger getCoins() {
		return coins;
	}

	public void setCoins(BigInteger i) {
		coins = i;
	}

	public void addCoins(BigInteger i) {
		setCoins(getCoins().add(i));
	}

	public void removeCoins(BigInteger i) {
		setCoins(getCoins().subtract(i));
	}
	
}
