package net.bobmandude9889.rpg.vault;

import org.bukkit.Material;

public enum VaultButton {

	PREV(0, Material.STAINED_GLASS_PANE, (short) 7, "Previous Page", new ButtonAction() {
		@Override
		public void run(Vault vault) {
			vault.updatePage();
			int page = vault.page - 1;
			page = page < 0 ? 0 : page;
			VaultManager.open(vault.player,page);
		};
	}),
	
	VOID1(1, Material.IRON_FENCE, (short) 0, " ", null),
	VOID2(2, Material.IRON_FENCE, (short) 0, " ", null),
	VOID3(3, Material.IRON_FENCE, (short) 0, " ", null),
	OPEN_TRANSFER(4, Material.SKULL_ITEM, (short) 0, "Transfer", null),
	VOID5(5, Material.IRON_FENCE, (short) 0, " ", null),
	VOID6(6, Material.IRON_FENCE, (short) 0, " ", null),
	VOID7(7, Material.IRON_FENCE, (short) 0, " ", null),
	
	NEXT(8, Material.STAINED_GLASS_PANE, (short) 7, "Next Page", new ButtonAction() {
		@Override
		public void run(Vault vault) {
			vault.updatePage();
			int page = vault.page + 1;
			page = page == Vault.pages ? Vault.pages - 1 : page;
			VaultManager.open(vault.player,page);
		};
	});

	int index;
	Material type;
	short data;
	String name;
	ButtonAction action;

	private VaultButton(int index, Material type, short data, String name, ButtonAction action) {
		this.index = index;
		this.type = type;
		this.data = data;
		this.name = name;
		this.action = action;
	}

	interface ButtonAction {
		
		public void run(Vault vault);
		
	}
	
}