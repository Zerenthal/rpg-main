package net.bobmandude9889.rpg.player;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;

import net.bobmandude9889.rpg.RPG;
import net.bobmandude9889.rpg.util.Title;
import net.md_5.bungee.api.ChatColor;

public class AFKManager implements Listener {

	public static HashMap<Player, Long> lastActivity;

	private static boolean warn = true;

	public AFKManager() {
		lastActivity = new HashMap<Player, Long>();
	}

	private void update(Player player) {
		lastActivity.put(player, System.currentTimeMillis());
	}

	@EventHandler
	public void playerMove(PlayerMoveEvent e) {
		Location oldLoc = e.getFrom();
		Location newLoc = e.getTo();
		if (oldLoc.getX() == newLoc.getX() && oldLoc.getY() == newLoc.getY() && oldLoc.getZ() == newLoc.getZ())
			return;
		update(e.getPlayer());
	}

	@EventHandler
	public void playerChat(AsyncPlayerChatEvent e) {
		update(e.getPlayer());
	}

	@EventHandler
	public void playerCommand(PlayerCommandPreprocessEvent e) {
		update(e.getPlayer());
	}

	@EventHandler
	public void playerJoin(PlayerJoinEvent e) {
		update(e.getPlayer());
	}

	public static void checkAFK() {
		ConcurrentHashMap<Player, Long> lastActivityClone = new ConcurrentHashMap<Player, Long>(lastActivity);
		for (Player player : lastActivityClone.keySet()) {
			if (!player.isOp()) {
				if (System.currentTimeMillis() - lastActivityClone.get(player) >= RPG.config.getDouble("afk_kick") * 60000) {
					player.kickPlayer(ChatColor.RED + "You were idling for more than "
							+ ((long) RPG.config.getDouble("afk_kick")) + " minutes!");
					lastActivity.remove(player);
				} else if ((System.currentTimeMillis() - lastActivityClone.get(player) >= RPG.config.getDouble("afk_warn")
						* 60000) && warn) {
					Title afkWarnTitle = new Title("&cAre you still there?", "&cMove so you don't get kicked!", 0, 1,
							0);
					afkWarnTitle.setTimingsToSeconds();
					afkWarnTitle.send(player);
				}
			}
		}
		warn = !warn;
	}

}
