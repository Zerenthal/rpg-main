package net.bobmandude9889.rpg.player;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.map.MapRenderer;
import org.bukkit.map.MapView;

import net.bobmandude9889.rpg.RPG;
import net.bobmandude9889.rpg.map.ContextualMapImageRenderer;

@SuppressWarnings("deprecation")
public class RPGPlayerManager implements Listener {

	static HashMap<Player, RPGPlayer> players;

	public RPGPlayerManager() {
		players = new HashMap<Player, RPGPlayer>();
		for (Player player : Bukkit.getOnlinePlayers()) {
			addPlayer(player);
		}
	}

	public static void addPlayer(Player player) {
		players.put(player, new RPGPlayer(player));
	}

	public static void removePlayer(Player player) {
		players.get(player).saveConfig();
		players.remove(player);
	}

	public static RPGPlayer getPlayer(Player player) {
		return players.get(player);
	}

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		addPlayer(e.getPlayer());
		Player player = e.getPlayer();
		for(ItemStack item : player.getInventory()) {
			if(item != null && item.getType().equals(Material.MAP)) {
				MapView map = Bukkit.getMap(item.getDurability());
				for(MapRenderer r : map.getRenderers()) {
					map.removeRenderer(r);
				}
				map.addRenderer(new ContextualMapImageRenderer());
			}
		}
	}

	@EventHandler
	public void onLeave(PlayerQuitEvent e) {
		removePlayer(e.getPlayer());
	}

	@EventHandler
	public void onPickup(final PlayerPickupItemEvent e) {
		Bukkit.getScheduler().scheduleSyncDelayedTask(RPG.instance, new Runnable() {
			@Override
			public void run() {
				getPlayer(e.getPlayer()).combineCoins();
			}
		});
	}

}
