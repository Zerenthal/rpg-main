package net.bobmandude9889.rpg.player;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.bobmandude9889.rpg.RPG;
import net.bobmandude9889.rpg.quest.Quest;
import net.bobmandude9889.rpg.quest.QuestManager;
import net.bobmandude9889.rpg.quest.TriggerType;
import net.bobmandude9889.rpg.skills.SkillDataManager;
import net.bobmandude9889.rpg.skills.SkillManager;
import net.bobmandude9889.rpg.skills.SkillType;
import net.bobmandude9889.rpg.util.Util;
import net.bobmandude9889.rpg.util.tellraw.TellrawBuilder;
import net.minecraft.server.v1_12_R1.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_12_R1.PacketPlayOutChat;

public class RPGPlayer {

	HashMap<SkillType, SkillManager> skills;
	public Player player;
	List<String> flags;
	public File file;
	public YamlConfiguration config;

	public String actionBar;
	
	public QuestManager questManager;

	public RPGPlayer(Player player) {
		this.player = player;

		file = SkillDataManager.getPlayerFile(player);
		config = YamlConfiguration.loadConfiguration(file);
		
		questManager = new QuestManager();
		questManager.load(this);

		skills = new HashMap<SkillType, SkillManager>();
		flags = config.getStringList("flags");

		for (SkillType type : SkillType.values()) {
			@SuppressWarnings("unchecked")
			Class<SkillManager> managerClass = (Class<SkillManager>) type.skillManager;

			if (managerClass != null) {

				Constructor<SkillManager> constructor = null;
				try {
					constructor = managerClass.getConstructor(RPGPlayer.class, SkillType.class);
				} catch (NoSuchMethodException | SecurityException e1) {
					e1.printStackTrace();
				}
				SkillManager manager = null;
				try {
					manager = constructor.newInstance(this, type);
				} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
				skills.put(type, manager);
				if (manager instanceof Listener)
					Bukkit.getPluginManager().registerEvents(manager, RPG.instance);
			}
		}

		Bukkit.getScheduler().scheduleSyncRepeatingTask(RPG.instance, new Runnable() {
			@Override
			public void run() {
				if(RPGPlayer.this.actionBar != null) {
					PacketPlayOutChat packet = new PacketPlayOutChat(ChatSerializer.a(new TellrawBuilder().addText(Util.parseColors(RPGPlayer.this.actionBar)).getJSON()));
					((CraftPlayer) RPGPlayer.this.player).getHandle().playerConnection.sendPacket(packet);
				}
			}
		}, 20l, 20l);
	}

	public boolean hasFlag(String key) {
		return flags.contains(key);
	}

	public void setFlag(String key) {
		if (!flags.contains(key)) {
			flags.add(key);
			HashMap<String,String> params = new HashMap<>();
			params.put("flag", key);
			this.questManager.attemptTrigger(TriggerType.gainFlag, params);
		}
	}

	public void removeFlag(String key) {
		if(flags.contains(key)) {
			flags.remove(key);
			HashMap<String,String> params = new HashMap<>();
			params.put("flag", key);
			this.questManager.attemptTrigger(TriggerType.loseFlag, params);
		}
	}

	public SkillManager getSkill(SkillType type) {
		return skills.get(type);
	}

	public void saveConfig() {
		config.set("flags", flags);
		questManager.save();
		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public ItemStack getCoinItem() {
		for (ItemStack item : player.getInventory().getStorageContents()) {
			if (Util.isCoin(item))
				return item;
		}
		if (Util.isCoin(player.getInventory().getItemInOffHand()))
			return player.getInventory().getItemInOffHand();
		return null;
	}

	public int getCoinIndex() {
		for (int i = 0; i < player.getInventory().getStorageContents().length; i++) {
			ItemStack item = player.getInventory().getStorageContents()[i];
			if (Util.isCoin(item))
				return i;
		}
		if (Util.isCoin(player.getInventory().getItemInOffHand()))
			return -2;
		return -1;
	}

	public BigInteger getCoins() {
		ItemStack coin = getCoinItem();
		return getCoins(coin);
	}

	private BigInteger getCoins(ItemStack coin) {
		if (coin != null) {
			String name = coin.getItemMeta().getDisplayName();
			return (Util.parseInt(name));
		} else {
			return new BigInteger("0");
		}
	}

	public void setCoins(BigInteger i) {
		ItemStack coin = getCoinItem();
		if (i.equals(BigInteger.ZERO)) {
			int index = getCoinIndex();
			if (index != -1)
				player.getInventory().setItem(getCoinIndex(), null);
		} else {
			if (coin != null) {
				Util.setCoinValue(coin, i);
			} else {
				coin = Util.createCoinItem();
				Util.setCoinValue(coin, i);
				giveItem(coin);
			}
		}
		combineCoins();
	}

	public void addCoins(BigInteger i) {
		ItemStack coin = getCoinItem();
		if (coin != null) {
			Util.setCoinValue(coin, getCoins(coin).add(i));
		} else {
			coin = Util.createCoinItem();
			Util.setCoinValue(coin, i);
			giveItem(coin);
		}
		combineCoins();
	}

	public void removeCoins(BigInteger i) {
		ItemStack coin = getCoinItem();
		if (coin != null) {
			Util.setCoinValue(coin, getCoins(coin).subtract(i));
		}
		combineCoins();
	}

	public void combineCoins() {
		if (Util.isCoin(player.getInventory().getItemInOffHand())) {
			ItemStack coin = player.getInventory().getItemInOffHand();
			player.getInventory().setItemInOffHand(null);
			giveItem(coin);
		}
		ItemStack[] contents = player.getInventory().getStorageContents();

		List<Integer> indices = new ArrayList<Integer>();

		for (int i = 0; i < contents.length; i++) {
			ItemStack item = contents[i];
			if (Util.isCoin(item)) {
				indices.add(i);
				if (item.getAmount() > 1) {
					ItemMeta meta = item.getItemMeta();
					BigInteger val = Util.parseInt(meta.getDisplayName());
					val = val.multiply(new BigInteger(Integer.toString(item.getAmount())));
					Util.setCoinValue(item, val);
					item.setAmount(1);
				}
			}
			contents[i] = item;
		}

		BigInteger total = new BigInteger("0");

		for (int i = 0; i < indices.size(); i++) {
			int index = indices.get(i);
			ItemStack item = contents[index];
			ItemMeta meta = item.getItemMeta();
			total = total.add(Util.parseInt(meta.getDisplayName()));

			item = null;

			contents[index] = item;
		}

		if (!total.equals(BigInteger.ZERO)) {
			ItemStack coin = Util.createCoinItem();
			Util.setCoinValue(coin, total);
			contents[indices.get(0)] = coin;
		}

		player.getInventory().setStorageContents(contents);
	}

	public boolean hasEnough(BigInteger i) {
		return getCoins().compareTo(i) > -1;
	}

	public void giveItem(ItemStack item) {
		if (item == null)
			return;
		ItemStack[] contents = player.getInventory().getContents();
		for (int i = 0; i < 36; i++) {
			ItemStack invItem = contents[i];
			if (invItem == null) {
				player.getInventory().addItem(item);
				return;
			}

			// Item stack is same type and won't overflow if items are added.

			if (invItem.isSimilar(item) && invItem.getAmount() + item.getAmount() <= item.getType().getMaxStackSize()) {
				invItem.setAmount(invItem.getAmount() + item.getAmount());
				return;
			}
		}
		player.getLocation().getWorld().dropItem(player.getLocation(), item);
	}

	public void setActionBar(String text) {
		this.actionBar = text;
	}

	public void startQuest(Quest quest) {
		
	}
	
}
