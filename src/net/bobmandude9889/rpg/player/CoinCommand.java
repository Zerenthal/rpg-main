package net.bobmandude9889.rpg.player;

import java.math.BigInteger;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.bobmandude9889.rpg.command.Command;
import net.bobmandude9889.rpg.util.Util;
import net.bobmandude9889.rpg.vault.VaultManager;
import net.md_5.bungee.api.ChatColor;

public class CoinCommand {

	@Command
	public void coinCommand(CommandSender sender, String[] args) {
		if (args.length > 0 && args[0].equalsIgnoreCase("get")) {
			Player player = (Player) sender;
			if (args.length > 1) {
				player = Bukkit.getPlayer(args[1]);
				if (player == null) {
					sender.sendMessage(ChatColor.RED + "Player not found.");
					return;
				}
			}

			String coinsOnPlayer = Util.formatInt(RPGPlayerManager.getPlayer(player).getCoins().toString());
			String coinsInBank = Util.formatInt(VaultManager.getVault(player).getCoins().toString());

			sender.sendMessage(ChatColor.GRAY + "Coins for " + ChatColor.GREEN + player.getName() + ChatColor.GRAY + ":");
			sender.sendMessage(ChatColor.GRAY + "On player: " + ChatColor.YELLOW + coinsOnPlayer);
			sender.sendMessage(ChatColor.GRAY + "In bank: " + ChatColor.YELLOW + coinsInBank);
		} else if (args.length > 1) {
			Player player = (Player) sender;
			if (args.length > 2) {
				player = Bukkit.getPlayer(args[2]);
				if (player == null) {
					sender.sendMessage(ChatColor.RED + "Player not found.");
					return;
				}
			}

			RPGPlayer rPlayer = RPGPlayerManager.getPlayer(player);
			try {
				switch (args[0].toLowerCase()) {
				case "add":
					rPlayer.addCoins(new BigInteger(args[1]));
					break;
				case "remove":
					rPlayer.removeCoins(new BigInteger(args[1]));
					break;
				case "set":
					rPlayer.setCoins(new BigInteger(args[1]));
					break;
				}
				
				sender.sendMessage(ChatColor.GREEN + player.getName() + ChatColor.GRAY + " now has " + ChatColor.YELLOW + Util.formatInt(rPlayer.getCoins().toString()) + " coins");
			} catch (Exception e) {
				e.printStackTrace();
				sender.sendMessage(ChatColor.GREEN + "/coins set,add,remove <amount> [player]");
			}
		}
	}

}
