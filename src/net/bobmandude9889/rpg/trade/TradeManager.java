package net.bobmandude9889.rpg.trade;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import net.bobmandude9889.rpg.RPG;
import net.bobmandude9889.rpg.gui.GUIHandler;
import net.md_5.bungee.api.ChatColor;

public class TradeManager {

	private static HashMap<Player, TradeRequest> requests;
	
	private static HashMap<Player, Long> lastRequest;
	
	public static void init() {
		requests = new HashMap<Player,TradeRequest>();
		lastRequest = new HashMap<Player, Long>();
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(RPG.instance, new Runnable() {
			@Override
			public void run() {
				checkForExpiredRequests();
			}
		}, 20, 20);
	}
	
	private static void checkForExpiredRequests() {
		for(Player player : requests.keySet()) {
			TradeRequest request = requests.get(player);
			if((System.currentTimeMillis() - request.timeSent) / 1000 >= RPG.config.getInt("request_timeout")) {
				request.expire();
				requests.remove(player);
			}
		}
	}
	
	public static void setLastRequest(Player player) {
		lastRequest.put(player, System.currentTimeMillis());
	}
	
	public static long getLastRequest(Player player) {
		return lastRequest.get(player);
	}
	
	public static boolean canSendRequest(Player player) {
		if(!lastRequest.containsKey(player))
			return true;
		return (System.currentTimeMillis() - lastRequest.get(player)) / 1000 >= RPG.config.getInt("trade_request_cooldown");
	}
	
	public static int getRemainingRequestCooldown(Player player) {
		if(!lastRequest.containsKey(player))
			return 0;
		return RPG.config.getInt("trade_request_cooldown") - (int) ((System.currentTimeMillis() - lastRequest.get(player)) / 1000);
	}
	
	public static void sendRequest(TradeRequest request) {
		requests.put(request.sender, request);
		request.sendMessage();
	}
	
	public static TradeRequest getRequest(Player sender) {
		if(!requests.containsKey(sender))
			return null;
		return requests.get(sender);
	}
	
	public static void cancelRequest(Player sender, boolean message) {
		TradeRequest request = requests.get(sender);
		if(request != null) {
			if(message) {
				request.receiver.sendMessage(ChatColor.RED + sender.getName() + ChatColor.GRAY + " has canceled their trade request.");
				request.sender.sendMessage(ChatColor.RED + "Your request has been canceled.");
			}
			GUIHandler.instance.closeGUI(sender);
			GUIHandler.instance.closeGUI(request.receiver);
		}
		requests.remove(sender);
	}
	
	public static void denyRequest(Player sender) {
		TradeRequest request = requests.get(sender);
		if(request != null) {
			request.receiver.sendMessage(ChatColor.RED + "Request denied.");
			request.sender.sendMessage(ChatColor.RED + request.receiver.getName() + ChatColor.GRAY + " denied your request.");
			GUIHandler.instance.closeGUI(sender);
			GUIHandler.instance.closeGUI(request.receiver);
		}
		requests.remove(sender);
	}
	
	public static void removeRequest(Player sender) {
		requests.remove(sender);
	}
	
}
