package net.bobmandude9889.rpg.trade;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import net.bobmandude9889.rpg.RPG;
import net.bobmandude9889.rpg.gui.GUIHandler;
import net.bobmandude9889.rpg.player.RPGPlayer;
import net.bobmandude9889.rpg.player.RPGPlayerManager;
import net.bobmandude9889.rpg.util.tellraw.TellrawBuilder;
import net.bobmandude9889.rpg.util.tellraw.TellrawChatText;
import net.bobmandude9889.rpg.util.tellraw.TellrawText;

public class TradeRequest {
	
	public Player sender;
	public Player receiver;
	public ItemStack[] contents;
	
	public boolean senderAccepted = false;
	public boolean receiverAccepted = false;
	
	public long timeSent = Long.MAX_VALUE;

	public TradeRequest(Player sender, Player receiver) {
		this.sender = sender;
		this.receiver = receiver;
	}
	
	public void sendToBoth(String message) {
		receiver.sendMessage(message);
		sender.sendMessage(message);
	}
	
	public void sendMessage() {
		timeSent = System.currentTimeMillis();
		
		TellrawBuilder builder = new TellrawBuilder();
		builder.addText(new TellrawChatText(sender.getName()).setColor(ChatColor.AQUA).setColor(ChatColor.BOLD));
		builder.addText(new TellrawChatText(" wants to trade with you!").setColor(ChatColor.GRAY));
		
		builder.sendToPlayer(receiver);
		builder = new TellrawBuilder();
		
		builder.addText(new TellrawChatText("You have ").setColor(ChatColor.GRAY));
		builder.addText(new TellrawChatText(RPG.config.getInt("request_timeout") + " seconds").setColor(ChatColor.GREEN));
		builder.addText(new TellrawChatText(" to accept.").setColor(ChatColor.GRAY));
		
		builder.sendToPlayer(receiver);
		builder = new TellrawBuilder();
		
		builder.addText(new TellrawChatText("ACCEPT").setColor(ChatColor.GREEN).setColor(ChatColor.BOLD).setRunCommandOnClick("/trade accept " + sender.getName()).setShowTextOnHover(new TellrawText("Click to accept!").setColor(ChatColor.GREEN)));
		builder.addText(new TellrawChatText(" DENY").setColor(ChatColor.RED).setColor(ChatColor.BOLD).setRunCommandOnClick("/trade deny " + sender.getName()).setShowTextOnHover(new TellrawText("Click to deny!").setColor(ChatColor.RED)));
		
		builder.sendToPlayer(receiver);
	}
	
	public void expire() {
		sender.sendMessage(ChatColor.RED + receiver.getName() + ChatColor.GRAY + " did not accept your request in time!");
		receiver.sendMessage(ChatColor.GRAY + "The trade request from " + ChatColor.RED + sender.getName() + ChatColor.GRAY + " has expired.");
	}
	
	public void completeTrade() {
		RPGPlayer rSender = RPGPlayerManager.getPlayer(sender);
		RPGPlayer rReceiver = RPGPlayerManager.getPlayer(receiver);
		for(int i = 0; i < contents.length; i++) {
			ItemStack item = contents[i];
			if(item != null)
				item = item.clone();
			if((i % 9) < 4) {
				rReceiver.giveItem(item);
			} else if((i % 9 > 4)) {
				rSender.giveItem(item);
			}
		}
		sendToBoth(ChatColor.GREEN + "Trade has been completed!");
		GUIHandler.instance.closeGUI(sender);
		GUIHandler.instance.closeGUI(receiver);
	}
	
}
