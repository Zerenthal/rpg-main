package net.bobmandude9889.rpg.trade;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;

import net.bobmandude9889.rpg.RPG;
import net.bobmandude9889.rpg.command.Command;
import net.bobmandude9889.rpg.gui.GUI;
import net.bobmandude9889.rpg.gui.GUIHandler;
import net.bobmandude9889.rpg.gui.GUITemplate;
import net.bobmandude9889.rpg.gui.action.GUIAction;
import net.bobmandude9889.rpg.gui.action.GUIChangeAction;
import net.bobmandude9889.rpg.gui.action.GUICloseAction;
import net.bobmandude9889.rpg.gui.action.GUIOnClickAction;
import net.bobmandude9889.rpg.gui.event.GUIChangeEvent;
import net.bobmandude9889.rpg.gui.event.GUIClickEvent;
import net.bobmandude9889.rpg.gui.event.GUICloseEvent;
import net.bobmandude9889.rpg.player.RPGPlayerManager;
import net.bobmandude9889.rpg.util.ItemBuilder;
import net.bobmandude9889.rpg.util.Util;
import net.bobmandude9889.rpg.util.tellraw.TellrawBuilder;
import net.bobmandude9889.rpg.util.tellraw.TellrawChatText;
import net.bobmandude9889.rpg.util.tellraw.TellrawText;

public class TradeCommand {

	GUITemplate tradeTemp;

	List<InventoryAction> cancel;

	public TradeCommand() {
		cancel = Lists.newArrayList(InventoryAction.COLLECT_TO_CURSOR, InventoryAction.DROP_ALL_CURSOR, InventoryAction.DROP_ALL_SLOT, InventoryAction.DROP_ONE_CURSOR, InventoryAction.DROP_ONE_SLOT, InventoryAction.HOTBAR_MOVE_AND_READD, InventoryAction.HOTBAR_SWAP);

		tradeTemp = new GUITemplate(54, "Trade");

		for (int i = 0; i < 5; i++) {
			tradeTemp.setButton(4 + (i * 9), new ItemBuilder(Material.IRON_FENCE).setName(" ").getItem(), GUIAction.blank);
		}
	}

	private void updateInventories(final GUI senderGUI, final GUI receiverGUI, final TradeRequest request, GUIChangeEvent event) {
		String waiting = "&cWaiting for partner to accept!";
		String accept = "&aPartner has accepted!";

		ItemStack acceptButton = new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 5).setName("&aAccept").setLore("&cIf your inventory is full,", "&citems will be dropped at your location.").getItem();
		ItemStack cancelButton = new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 14).setName("&cCancel").setLore("&aChange your mind about the items?").getItem();

		final GUIAction senderAcceptAction = new GUIAction() {
			@Override
			public void onClick(GUIClickEvent e) {
				request.senderAccepted = true;
				updateInventories(senderGUI, receiverGUI, request, null);
			}
		};

		final GUIAction senderCancelAcceptAction = new GUIAction() {
			@Override
			public void onClick(GUIClickEvent e) {
				request.senderAccepted = false;
				updateInventories(senderGUI, receiverGUI, request, null);
			}
		};

		final GUIAction receiverAcceptAction = new GUIAction() {
			@Override
			public void onClick(GUIClickEvent e) {
				request.receiverAccepted = true;
				updateInventories(senderGUI, receiverGUI, request, null);
			}
		};

		final GUIAction receiverCancelAcceptAction = new GUIAction() {
			@Override
			public void onClick(GUIClickEvent e) {
				request.receiverAccepted = false;
				updateInventories(senderGUI, receiverGUI, request, null);
			}
		};

		if (event == null) {

			if (request.receiverAccepted) {
				senderGUI.setName(accept);
				receiverGUI.setButton(49, cancelButton, receiverCancelAcceptAction);
			} else {
				senderGUI.setName(waiting);
				receiverGUI.setButton(49, acceptButton, receiverAcceptAction);
			}

			if (request.senderAccepted) {
				receiverGUI.setName(accept);
				senderGUI.setButton(49, cancelButton, senderCancelAcceptAction);
			} else {
				receiverGUI.setName(waiting);
				senderGUI.setButton(49, acceptButton, senderAcceptAction);
			}

			if (request.receiverAccepted && request.senderAccepted) {
				ItemStack[] contents = new ItemStack[senderGUI.inventory.getSize()];
				for (int i = 0; i < senderGUI.size; i++) {
					ItemStack item = senderGUI.inventory.getItem(i);
					if ((i % 9) != 4) {
						contents[i] = item;
					}
				}
				request.contents = contents;
				request.completeTrade();
				TradeManager.removeRequest(request.sender);
			}
		}

		if (event != null) {
			Bukkit.getScheduler().scheduleSyncDelayedTask(RPG.instance, () -> {
				request.senderAccepted = request.receiverAccepted = false;
				updateInventories(senderGUI, receiverGUI, request, null);
			});
			
			GUI gui = event.gui;
			GUI other = gui.player.equals(senderGUI.player) ? receiverGUI : senderGUI;

			boolean changedRight = false;
			for (int i = 0; i < event.inventory.getSize(); i++) {
				if ((i % 9) > 4) {
					if (event.added[i] != null || event.removed[i] != null)
						changedRight = true;
				}
			}

			ItemStack[] leftContents = new ItemStack[24];
			ItemStack[] rightAdded = new ItemStack[24];
			ItemStack[] rightContents = new ItemStack[24];

			for (int i = 0; i < event.inventory.getSize(); i++) {
				ItemStack item = event.contents[i];
				if ((i % 9) < 4) {
					int index = i - (Math.floorDiv(i, 9) * 5);
					leftContents[index] = item;
				} else if ((i % 9) > 4) {
					int index = (i - 5) - (Math.floorDiv(i, 9) * 5);
					rightAdded[index] = event.added[i];
					rightContents[index] = event.contents[i];
				}
			}

			if (changedRight && !event.action.equals(InventoryAction.MOVE_TO_OTHER_INVENTORY)) {
				for (int i = 0; i < leftContents.length; i++) {
					int index = i + (Math.floorDiv(i, 4) * 5);
					gui.setItem(index, leftContents[i]);
					gui.setItem(index + 5, rightContents[i]);
				}
			}

			for (int i = 0; i < leftContents.length; i++) {
				ItemStack item = leftContents[i];
				int index = i + (Math.floorDiv(i, 4) * 5) + 5;
				if (Util.isCoin(item)) {
					gui.setItem(index - 5, null);
					leftContents[i] = null;
					RPGPlayerManager.getPlayer(gui.player).giveItem(item);
					item = null;
				}
				other.setItem(index, item);
			}
		}
	}

	public void acceptRequest(final Player sender, final Player receiver) {
		final TradeRequest request = TradeManager.getRequest(sender);

		if (request != null && request.receiver.equals(receiver)) {
			request.timeSent = Long.MAX_VALUE;
			final GUI senderGUI = tradeTemp.open(sender);
			senderGUI.setAllowChange(true);
			final GUI receiverGUI = tradeTemp.open(receiver);
			receiverGUI.setAllowChange(true);

			GUIChangeAction onChange = new GUIChangeAction() {

				@Override
				public void onChange(GUIChangeEvent e) {
					updateInventories(senderGUI, receiverGUI, request, e);
				}

			};

			GUIOnClickAction onClick = new GUIOnClickAction() {

				@Override
				public void onClick(InventoryClickEvent e) {
					e.setCancelled(cancel.contains(e.getAction()));
					if (e.getClickedInventory() != null && e.getClickedInventory().getType().equals(InventoryType.CHEST) && (e.getSlot() % 9) > 4) {
						e.setCancelled(true);
					}
					if (e.getClickedInventory() != null && e.getAction().equals(InventoryAction.MOVE_TO_OTHER_INVENTORY) && e.getClickedInventory().getType().equals(InventoryType.PLAYER)) {
						e.setCancelled(true);
						GUI gui = GUIHandler.instance.guis.get(e.getWhoClicked());
						GUI other = gui.player.equals(senderGUI.player) ? receiverGUI : senderGUI;
						for (int i = 0; i < gui.size; i++) {
							ItemStack item = gui.inventory.getItem(i);
							if ((i % 9) < 4 && item == null) {
								gui.setItem(i, e.getCurrentItem());
								other.setItem(i + 5, e.getCurrentItem());
								e.getClickedInventory().setItem(e.getSlot(), null);
								break;
							}
						}
					}
				}
			};

			GUICloseAction onClose = new GUICloseAction() {

				@Override
				public void onClose(GUICloseEvent e) {
					if (e.playerClosed) {
						if (e.player.getName().equals(sender.getName())) {
							cancelRequest(sender);
						} else {
							denyRequest(sender, receiver);
						}

						for (int i = 0; i < senderGUI.inventory.getSize(); i++) {
							ItemStack senderItem = senderGUI.inventory.getContents()[i];
							ItemStack receiverItem = receiverGUI.inventory.getContents()[i];
							if ((i % 9) < 4) {
								RPGPlayerManager.getPlayer(sender).giveItem(senderItem);
								RPGPlayerManager.getPlayer(receiver).giveItem(receiverItem);
							}
						}
					}
				}

			};

			senderGUI.setOnChange(onChange);
			receiverGUI.setOnChange(onChange);

			senderGUI.setOnClick(onClick);
			receiverGUI.setOnClick(onClick);

			senderGUI.setOnClose(onClose);
			receiverGUI.setOnClose(onClose);

			updateInventories(senderGUI, receiverGUI, request, null);
		} else {
			receiver.sendMessage(ChatColor.GRAY + "You do not have a request from " + ChatColor.RED + sender.getName());
		}
	}

	public void denyRequest(Player sender, Player receiver) {
		TradeRequest request = TradeManager.getRequest(sender);
		if (request != null && request.receiver.equals(receiver)) {
			TradeManager.denyRequest(sender);
		} else {
			receiver.sendMessage(ChatColor.GRAY + "You do not have a request from " + ChatColor.RED + sender.getName());
		}
	}

	public void cancelRequest(Player sender) {
		if (TradeManager.getRequest(sender) != null) {
			TradeManager.cancelRequest(sender, true);
		} else {
			sender.sendMessage(ChatColor.RED + "You do not have a request you can cancel!");
		}
	}

	public void sendRequest(Player sender, Player receiver) {
		if (TradeManager.getRequest(sender) == null) {

			if (TradeManager.canSendRequest(sender)) {
				if (sender.getLocation().distance(receiver.getLocation()) < RPG.config.getInt("trade_radius")) {
					sender.sendMessage(ChatColor.GRAY + "Trade request sent to " + ChatColor.GREEN + receiver.getName() + ChatColor.GRAY + ".");
					TradeManager.sendRequest(new TradeRequest(sender, receiver));
				} else {
					sender.sendMessage(ChatColor.GRAY + "The player cannot be more than " + ChatColor.RED + RPG.config.getInt("trade_radius") + " blocks " + ChatColor.GRAY + "away!");
				}
			} else {
				sender.sendMessage(ChatColor.GRAY + "Please wait " + ChatColor.RED + TradeManager.getRemainingRequestCooldown(sender) + " seconds " + ChatColor.GRAY + "before sending another request.");
			}
		} else {
			TellrawBuilder builder = new TellrawBuilder();
			builder.addText(new TellrawChatText("You already have an open request. Use ").setColor(ChatColor.RED));
			builder.addText(new TellrawChatText("/trade cancel").setColor(ChatColor.GREEN).setRunCommandOnClick("/trade cancel").setShowTextOnHover(new TellrawText("Click to cancel.").setColor(ChatColor.AQUA)));
			builder.addText(new TellrawChatText(" to cancel your current request.").setColor(ChatColor.RED));
			builder.sendToPlayer(sender);
			sendHelp(sender);
		}
	}

	public void sendHelp(Player player) {
		player.sendMessage(ChatColor.GREEN + "\n\n---===[Trading]===---");
		player.sendMessage(ChatColor.GRAY + "/trade <player>\n" + ChatColor.GREEN + "Sends a player a trade request.");
		player.sendMessage(ChatColor.GRAY + "/trade accept <player>\n" + ChatColor.GREEN + "Accepts a trade request from a player.");
		player.sendMessage(ChatColor.GRAY + "/trade deny <player>\n" + ChatColor.GREEN + "Denys a trade request from a player.");
		player.sendMessage(ChatColor.GRAY + "/trade cancel\n" + ChatColor.GREEN + "Cancels your current request.");
	}

	@Command
	public void tradeCommand(CommandSender sender, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (args.length >= 2 && args[0].equalsIgnoreCase("accept")) {
				Player pSender = Bukkit.getPlayer(args[1]);
				if (pSender != null)
					acceptRequest(pSender, player);
				else
					player.sendMessage(ChatColor.GRAY + "Player not found: " + ChatColor.RED + args[1]);
			} else if (args.length >= 2 && args[0].equalsIgnoreCase("deny")) {
				Player pSender = Bukkit.getPlayer(args[1]);
				if (pSender != null)
					denyRequest(pSender, player);
				else
					player.sendMessage(ChatColor.GRAY + "Player not found: " + ChatColor.RED + args[1]);
			} else if (args.length >= 1 && args[0].equalsIgnoreCase("cancel")) {
				cancelRequest(player);
			} else if (args.length >= 1) {
				Player receiver = Bukkit.getPlayer(args[0]);
				if (receiver != null)
					if (receiver.equals(player)) {
						sender.sendMessage(ChatColor.RED + "You can not trade with yourself!");
					} else
						sendRequest(player, receiver);
				else {
					player.sendMessage(ChatColor.GRAY + "Player not found: " + ChatColor.RED + args[0]);
					sendHelp(player);
				}
			} else {
				sendHelp(player);
			}
		}
	}

}
