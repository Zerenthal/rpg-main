package net.bobmandude9889.rpg.util.tellraw;

import java.util.HashMap;

import org.bukkit.ChatColor;
import org.json.simple.JSONObject;

@SuppressWarnings("unchecked")
public class TellrawText {

	protected JSONObject obj;
	
	private static HashMap<ChatColor, String> mods;
	
	public TellrawText(String text) {
		obj = new JSONObject();
		obj.put("text", text);
		
		if(mods == null) {
			mods = new HashMap<ChatColor,String>();
			mods.put(ChatColor.BOLD, "bold");
			mods.put(ChatColor.ITALIC, "italic");
			mods.put(ChatColor.UNDERLINE, "underlined");
			mods.put(ChatColor.STRIKETHROUGH, "strikethrough");
			mods.put(ChatColor.MAGIC, "obfuscated");
		}
	}
	
	public TellrawText setText(String text) {
		obj.put("text", text);
		return this;
	}
	
	public TellrawText setColor(ChatColor color) {
		if(mods.containsKey(color)){
			obj.put(mods.get(color), true);
		} else if(color.equals(ChatColor.RESET)) {
			obj.put("color","none");
		} else {
			obj.put("color", color.name().toLowerCase());
		}
		return this;
	}
	
	@Override
	public String toString() {
		return obj.toJSONString();
	}
	
}
