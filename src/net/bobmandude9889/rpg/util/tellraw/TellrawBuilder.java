package net.bobmandude9889.rpg.util.tellraw;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.json.simple.JSONArray;

@SuppressWarnings("unchecked")
public class TellrawBuilder {

	JSONArray obj;
	
	public TellrawBuilder() {
		obj = new JSONArray();
		obj.add("");
	}
	
	public TellrawBuilder addText(String... textArray) {
		for(String text : textArray)
			addText(new TellrawChatText(text));
		return this;
	}
	
	public TellrawBuilder addText(TellrawChatText... textArray) {
		for(TellrawChatText text : textArray)
			obj.add(text.obj);
		return this;
	}
	
	public String getJSON() {
		return obj.toJSONString().replace("\\/", "/");
	}
	
	public void sendToPlayer(Player player) {
		Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "tellraw " + player.getName() + " " + getJSON());
	}
	
	@Override
	public String toString() {
		return getJSON();
	}
	
}
