package net.bobmandude9889.rpg.util;

import org.bukkit.Chunk;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.bobmandude9889.rpg.command.Command;

public class ChunkCommand {

	@Command
	public void chunk(CommandSender sender) {
		Player player = (Player) sender;
		Chunk chunk = player.getLocation().getChunk();
		int topX = chunk.getX() * 16;
		int topZ = chunk.getZ() * 16;
		player.sendMessage("The chunk you are in ranges from x:(" + topX + "-" + topX + 16 + ") to z:(" + (topZ) + "," + (topZ + 16) + ")");
	}
	
}
