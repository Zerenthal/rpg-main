package net.bobmandude9889.rpg.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import com.sk89q.worldguard.bukkit.RegionContainer;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import net.bobmandude9889.rpg.RPG;
import net.minecraft.server.v1_12_R1.GameProfileSerializer;
import net.minecraft.server.v1_12_R1.Item;
import net.minecraft.server.v1_12_R1.NBTTagCompound;

public class Util {

	public static Enchantment glow = new GlowEnchant(69);

	public static String itemStackArrayToBase64(ItemStack[] items) throws IllegalStateException {
		try {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);

			// Write the size of the inventory
			dataOutput.writeInt(items.length);

			// Save every element in the list
			for (int i = 0; i < items.length; i++) {
				dataOutput.writeObject(items[i]);
			}

			// Serialize that array
			dataOutput.close();
			return Base64Coder.encodeLines(outputStream.toByteArray());
		} catch (Exception e) {
			throw new IllegalStateException("Unable to save item stacks.", e);
		}
	}

	public static ItemStack[] itemStackArrayFromBase64(String data) throws IOException {
		try {
			ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(data));
			BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
			ItemStack[] items = new ItemStack[dataInput.readInt()];

			// Read the serialized inventory
			for (int i = 0; i < items.length; i++) {
				items[i] = (ItemStack) dataInput.readObject();
			}

			dataInput.close();
			return items;
		} catch (ClassNotFoundException e) {
			throw new IOException("Unable to decode class type.", e);
		}
	}

	public static String locationToString(Location loc) {
		// @formatter:off
		String str = loc.getWorld().getName() + ","
				+ loc.getX() + ","
				+ loc.getY() + ","
				+ loc.getZ() + ","
				+ loc.getYaw() + ","
				+ loc.getPitch();
		// @formatter:on
		return str;
	}

	public static Location stringToLocation(String str) {
		String[] data = str.split(",");
		// @formatter:off
		Location loc = new Location(Bukkit.getWorld(data[0])
				,Double.parseDouble(data[1])
				,Double.parseDouble(data[2])
				,Double.parseDouble(data[3])
				,Float.parseFloat(data[4])
				,Float.parseFloat(data[5]));
		// @formatter:on
		return loc;
	}

	public static Entity getEntityByUUID(String uuid, World world) {
		for (Entity e : world.getEntities()) {
			if (e.getUniqueId().toString().equals(uuid))
				return e;
		}
		return null;
	}

	public static String formatInt(String num) {
		String out = "";
		for (int j = 0; j < num.length() - 3; j += 3) {
			out = "," + num.substring(num.length() - (j + 3), num.length() - j) + out;
		}
		out = num.substring(0, num.length() % 3) + out;
		if (num.length() % 3 == 0)
			out = num.substring(0, 3) + out;
		return out;
	}

	public static String[] numExtensions = { "K", "M", "B", "T", "Quadrillion", "Quintillion", "Sextillion", "Septillion", "Octillion", "Nonillion", "Decillion", "Undecillion", "Duodecillion", "Tredecillion" };

	public static Boolean takeItem(Player player, Material item, int amount) {
		int amountInInv = 1;
		while (player.getInventory().contains(item, amountInInv)) {
			amountInInv++;
		}
		amountInInv--;
		if (amountInInv < amount) {
			return false;
		}
		for (ItemStack itemStack : player.getInventory().getContents()) {
			if (itemStack != null && itemStack.getType().equals(item)) {
				int itemAmount = itemStack.getAmount();
				if (itemAmount > amount) {
					itemStack.setAmount(itemAmount - amount);
					break;
				} else {
					amount -= itemAmount;
					player.getInventory().remove(itemStack);
					if (amount == 0)
						break;
				}
			}
		}

		return true;
	}

	public static String shortInt(String num) {
		String format = formatInt(num);
		String[] split = format.split(",");
		int ext = split.length - 2;
		if (ext < 0) {
			return num;
		}
		return split[0] + "." + split[1].substring(0, 1) + numExtensions[ext];
	}

	public static BigInteger parseInt(String i) {
		String out = "";
		for (char c : i.toCharArray()) {
			if ("0123456789".contains(Character.toString(c)))
				out += c;
		}
		return new BigInteger(out);
	}

	public static boolean isCoin(ItemStack item) {
		if (item != null) {
			ItemMeta meta = item.getItemMeta();
			if (item.getType().equals(Material.SKULL_ITEM) && item.hasItemMeta()) {
				String stripName = ChatColor.stripColor(meta.getDisplayName());
				return stripName.endsWith("coins") || stripName.endsWith("coin");
			}
			return false;
		}
		return false;
	}

	public static void setCoinValue(ItemStack item, BigInteger i) {
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.YELLOW + formatInt(i.toString()) + " coin" + (i.equals(new BigInteger("1")) ? "" : "s"));
		item.setItemMeta(meta);
	}

	public static ItemStack createCoinItem() {
		net.minecraft.server.v1_12_R1.ItemStack coin = new net.minecraft.server.v1_12_R1.ItemStack(Item.getById(397));
		coin.setData(3);
		NBTTagCompound tag = new NBTTagCompound();
		NBTTagCompound owner = new NBTTagCompound();
		GameProfileSerializer.serialize(owner, getNonPlayerProfile(RPG.config.getString("coin_texture"), "da608428-e220-435c-9669-2fe3c0575458", "Money Bag"));
		tag.set("SkullOwner", owner);
		coin.setTag(tag);
		return CraftItemStack.asCraftMirror(coin);
	}

	public static GameProfile getNonPlayerProfile(String skinUrl, String uuid, String name) {
		UUID rawuuid = UUID.fromString(uuid);
		GameProfile profile = new GameProfile(rawuuid, name);

		List<Object> args = new ArrayList<Object>();
		args.add(System.currentTimeMillis());
		args.add(rawuuid.toString().replace("-", ""));
		args.add(name);
		args.add(skinUrl);

		profile.getProperties().put("textures", new Property("textures", Base64Coder.encodeString(String.format("{\"timestamp\":%d,\"profileId\":\"%s\",\"profileName\":\"%s\",\"textures\":{\"SKIN\":{\"url\":\"%s\"}}}", args.toArray(new Object[args.size()])))));
		return profile;
	}

	public static String formatDouble(Double num, String format) {
		DecimalFormat df = new DecimalFormat(format);
		return df.format(num);
	}

	public static String parseColors(String message) {
		return ChatColor.translateAlternateColorCodes('&', message);
	}

	public static boolean registerGlowEnchantment() {
		try {
			Field f = Enchantment.class.getDeclaredField("acceptingNew");
			f.setAccessible(true);
			f.set(null, true);
			try {
				Enchantment.registerEnchantment(glow);
				return true;
			} catch (IllegalArgumentException e) {

			}
		} catch (Exception e) {
		}
		return false;
	}

	public static String capName(String name) {
		String[] split = name.split("_");
		name = "";
		for (String part : split) {
			name += part.substring(0, 1).toUpperCase() + part.substring(1).toLowerCase() + " ";
		}
		return name.substring(0, name.length() - 1);
	}

	public static boolean isIn(Location location, Chunk chunk) {
		return location.getChunk().equals(chunk);
	}

	public static WorldGuardPlugin getWorldGuard() {
		Plugin plugin = RPG.instance.getServer().getPluginManager().getPlugin("WorldGuard");

		// WorldGuard may not be loaded
		if (plugin == null || !(plugin instanceof WorldGuardPlugin)) {
			return null; // Maybe you want throw an exception instead
		}

		return (WorldGuardPlugin) plugin;
	}

	public static Location getHighestBlock(Location loc) {
		for (int y = 255; y > 0; y--) {
			Location newLoc = new Location(loc.getWorld(), loc.getX(), y, loc.getZ());
			if (newLoc.getBlock().getType().isSolid()) {
				newLoc.add(0.5, 1, 0.5);
				return newLoc;
			}
		}
		return new Location(loc.getWorld(), loc.getX(), 0, loc.getZ());
	}

	public static Location randomLocation(int x1, int z1, int x2, int z2, World world) {
		int minX = x1;
		int maxX = x2;
		if (minX > maxX) {
			minX = x2;
			maxX = x1;
		}

		int minZ = z1;
		int maxZ = z2;
		if (minZ > maxZ) {
			minZ = z2;
			maxZ = z1;
		}

		Random r = new Random();
		Location loc = new Location(world, r.nextInt(maxX - minX) + minX, 0, r.nextInt(maxZ - minZ) + minZ);
		return getHighestBlock(loc);
	}

	public static boolean isInRegion(Location loc, String region) {
		RegionContainer container = Util.getWorldGuard().getRegionContainer();
		RegionManager manager = container.get(loc.getWorld());
		ApplicableRegionSet regions = manager.getApplicableRegions(loc);
		for (ProtectedRegion r : regions) {
			if (r.getId().equalsIgnoreCase(region)) {
				return true;
			}
		}
		return false;
	}

}
