package net.bobmandude9889.rpg.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.bobmandude9889.rpg.RPG;
import net.bobmandude9889.rpg.command.Command;

public class TokenCommand {

	@Command
	public void getToken(final CommandSender sender) {
		final String token = randomToken();
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				Connection conn;
				try {
					conn = RPG.sqlForumConn.getConn();

					Statement stmt = conn.createStatement();
					String uuid = ((Player) sender).getUniqueId().toString().replaceAll("-", "");

					String sql = "DELETE FROM Tokens WHERE Uuid='" + uuid + "';";
					stmt.execute(sql);

					sql = "INSERT INTO Tokens (Uuid, Token) VALUES ('" + uuid + "','" + token + "')";
					stmt.execute(sql);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});

		thread.start();
		sender.sendMessage(Util.parseColors("&8&m-----------------------\n&7Your website token is:&b " + token + "\n&7Please enter this token to continue your website registration.\n&8&m-----------------------"));
	}

	public static String randomToken() {
		String token = "";
		Random r = new Random();
		for (int i = 0; i < 6; i++) {
			token += (char) (r.nextInt(26) + 97);
		}
		return token;
	}

}
