package net.bobmandude9889.rpg.util.sql;

import java.sql.Connection;
import java.sql.DriverManager;

import java.sql.SQLException;
import java.util.Properties;

public class DBConn {
	private String ip;
	private String port;
	private String db;
	private String username;
	private String password;
	private Connection conn;

	public DBConn(String ip, String port, String db, String username,
			String password) {

		this.ip = ip;
		this.port = port;
		this.db = db;
		this.username = username;
		this.password = password;

		updateConnection();
	}

	public boolean isConnected() {
		return (conn != null);
	}

	public void closeConnection() {
		try {
			if (conn != null)
				conn.close();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	public void updateConnection() {
		try {
			if (conn != null)
				conn.close();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		String connectionStatement = "jdbc:mysql://" + this.ip + ":"
				+ this.port + "/" + this.db;

		Properties connectionProps = new Properties();

		connectionProps.put("user", this.username);
		connectionProps.put("password", this.password);
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(connectionStatement,
					connectionProps);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		System.out.println("Connected to database");
		this.conn = conn;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getDb() {
		return db;
	}

	public void setDb(String db) {
		this.db = db;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Connection getConn() {
		return conn;
	}

	public void setConn(Connection conn) {
		this.conn = conn;
	}

}
