package net.bobmandude9889.rpg.util.sql;

public class StatementBuilder {
	//type
	//table
	//columns
	//values
	//where
	//order by
	//limit
	/*Types:
	 * SELECT column_name(s) FROM table_name WHERE condition ORDER BY column1, column2, column3,... ASC, DESC
	 * INSERT INTO table_name (column1, column2, column3,...) VALUES (value1, value2, value3,....)
	 */
	public static String columnBuilder(String... cols){
		String colStmt = "";
		for (String col : cols){
			colStmt += ", " + col;
		}
		colStmt = colStmt.replaceFirst(", ", "");
		return colStmt;
	}
	
	public static String valueBuilder(Object... vals){
		String valStmt = "";
		for (Object val : vals){
			valStmt += ", ";
			if (val instanceof Integer){
				valStmt += String.valueOf(val);
			} else {
				valStmt += "'" + String.valueOf(val) + "'";
			}
		}
		return valStmt;
	}
}
