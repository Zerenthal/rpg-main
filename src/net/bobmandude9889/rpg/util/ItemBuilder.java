package net.bobmandude9889.rpg.util;

import java.util.ArrayList;
import java.util.List;

import net.md_5.bungee.api.ChatColor;
import net.milkbowl.vault.item.Items;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;

public class ItemBuilder {

	ItemStack item;

	public ItemBuilder(ItemStack item) {
		this.item = new ItemStack(item);
	}

	public ItemBuilder(Material type) {
		this.item = new ItemStack(type);
	}

	public ItemBuilder(Material type, int amount) {
		this.item = new ItemStack(type, amount);
	}

	public ItemBuilder(Material type, int amount, short damage) {
		this.item = new ItemStack(type, amount, damage);
	}

	public ItemStack getItem() {
		return item;
	}

	public ItemBuilder addEnchantment(Enchantment type, int level) {
		item.addEnchantment(type, level);
		return this;
	}

	public ItemBuilder setName(String name) {
		if (name == null)
			name = "";
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
		item.setItemMeta(meta);
		return this;
	}

	public String getName() {
		ItemMeta meta = item.getItemMeta();
		if (meta.getDisplayName() != null)
			return meta.getDisplayName();
		return Items.itemByType(item.getType()).getName();
	}

	public ItemBuilder setLore(String... lore) {
		List<String> loreList = new ArrayList<String>();

		for (int i = 0; i < lore.length; i++) {
			loreList.add(ChatColor.translateAlternateColorCodes('&', lore[i]));
		}

		ItemMeta meta = item.getItemMeta();
		meta.setLore(loreList);
		item.setItemMeta(meta);
		return this;
	}

	public ItemBuilder setMaterial(Material material) {
		item.setType(material);
		return this;
	}

	public ItemBuilder setAmount(int amount) {
		item.setAmount(amount);
		return this;
	}

	public ItemBuilder setDurability(short durability) {
		item.setDurability(durability);
		return this;
	}

	public ItemBuilder setPotion(PotionType type, boolean extend, boolean upgrade) {
		PotionMeta meta = (PotionMeta) item.getItemMeta();
		meta.setBasePotionData(new PotionData(type, extend, upgrade));
		item.setItemMeta(meta);
		return this;
	}

	public ItemBuilder glow() {
		item.addEnchantment(Util.glow, 1);
		return this;
	}

}
