package net.bobmandude9889.rpg.util;

import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import net.bobmandude9889.rpg.RPG;
import net.md_5.bungee.api.ChatColor;

public class MobHealthDisplay implements Listener{

	ConcurrentHashMap<Entity, Long> named;
	
	public MobHealthDisplay() {
		named = new ConcurrentHashMap<>();
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(RPG.instance, new Runnable() {
			@Override
			public void run() {
				for(Entity e : named.keySet()) {
					if(System.currentTimeMillis() - 5000 >= named.get(e)) {
						e.setCustomName("");
						e.setCustomNameVisible(false);
						named.remove(e);
					}
				}
			}
		},20l,20l);
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onDamage(EntityDamageByEntityEvent e) {
		if(!e.getEntityType().equals(EntityType.PLAYER) && !e.isCancelled()) {
			Damageable ent = (Damageable) e.getEntity();
			int health = (int) Math.round(ent.getHealth() - e.getFinalDamage());
			if(health < 0)
				health = 0;
			String hearts = "";
			for(int i = 0; i < 10; i ++) {
				int val = (int) (ent.getMaxHealth() * ((i + 1d) / 10d));
				if(health < val)
					hearts += ChatColor.GRAY + "";
				else 
					hearts += ChatColor.DARK_RED + "";
				hearts += "\u2764";
			}
			String name = hearts + " " + ChatColor.RED + health  + " / " + ((int) ent.getMaxHealth());
			ent.setCustomName(name);
			ent.setCustomNameVisible(true);
			named.put(ent, System.currentTimeMillis());
		}
	}
	
}
