package net.bobmandude9889.rpg.shop;

import org.bukkit.Material;

public class MaterialData {

	public Material material;
	public Short data;
	
	public MaterialData(Material material, Short data) {
		this.material = material;
		this.data = data;
	}

	@Override
	public String toString() {
		return material + " : " + data;
	}
	
}
