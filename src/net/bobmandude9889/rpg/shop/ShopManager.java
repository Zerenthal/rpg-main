package net.bobmandude9889.rpg.shop;

import java.io.File;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import net.bobmandude9889.rpg.RPG;
import net.bobmandude9889.rpg.gui.GUI;
import net.bobmandude9889.rpg.gui.GUITemplate;
import net.bobmandude9889.rpg.gui.action.GUIAction;
import net.bobmandude9889.rpg.gui.action.GUIChangeAction;
import net.bobmandude9889.rpg.gui.action.GUICloseAction;
import net.bobmandude9889.rpg.gui.action.GUIOnClickAction;
import net.bobmandude9889.rpg.gui.event.GUIChangeEvent;
import net.bobmandude9889.rpg.gui.event.GUIClickEvent;
import net.bobmandude9889.rpg.gui.event.GUICloseEvent;
import net.bobmandude9889.rpg.player.RPGPlayer;
import net.bobmandude9889.rpg.player.RPGPlayerManager;
import net.bobmandude9889.rpg.util.ItemBuilder;
import net.bobmandude9889.rpg.util.Util;
import net.milkbowl.vault.item.ItemInfo;
import net.milkbowl.vault.item.Items;

public class ShopManager implements Listener {

	private static List<Material> denySell;

	private static File dataFolder;

	private static File priceFile;
	public static YamlConfiguration priceConfig;

	private static File configFile;
	public static YamlConfiguration config;

	static GUITemplate shopTemp;
	static GUITemplate sellTemp;

	static int pageSize = 45;
	static int guiSize = 54;
	
	public static void init() {
		shopTemp = new GUITemplate(54, "Item Shop");
		sellTemp = new GUITemplate(54, "Move items in to sell them.");
		sellTemp.setAllowChange(true);
		sellTemp.setAllowDrag(false);
		
		sellTemp.setOnChange(new GUIChangeAction() {
			@Override
			public void onChange(GUIChangeEvent e) {
				e.gui.setContents(e.contents);
				int price = getPrice(e.gui);
				if(price >= 0)
					e.gui.setName("&e" + price + " coins");
				else
					e.gui.setName("&cYou cannot sell those items!");
			}
		});
		sellTemp.setOnClick(new GUIOnClickAction() {
			@Override
			public void onClick(InventoryClickEvent e) {
				ItemStack item = e.getClickedInventory().getItem(e.getSlot());
				if((item != null && !PriceManager.hasPrice(item.getType(), item.getDurability())) || Util.isCoin(item)) {
					e.setCancelled(true);
				}
			}
		});
		sellTemp.setOnClose(new GUICloseAction() {
			@Override
			public void onClose(GUICloseEvent e) {
				if(e.playerClosed)
					returnItems(e.gui);
			}
		});
		
		sellTemp.setButton(53, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 5).setName("&aSell items").getItem(), new GUIAction() {
			@Override
			public void onClick(GUIClickEvent e) {
				int price = getPrice(e.gui);
				if(price >= 0) {
					RPGPlayerManager.getPlayer(e.player).addCoins(new BigInteger(Integer.toString(price)));
				} else {
					e.player.sendMessage(ChatColor.RED + "You cannot sell those items!");
					returnItems(e.gui);
				}
				e.gui.closeInventory();
			}
		});
		
		dataFolder = new File(RPG.instance.getDataFolder(), "shop");

		priceFile = new File(dataFolder, "prices.yml");

		configFile = new File(dataFolder, "config.yml");

		denySell = new ArrayList<Material>();
		PriceManager.init();

		reload();
	}

	public static void reload(){
		priceConfig = YamlConfiguration.loadConfiguration(priceFile);
		config = YamlConfiguration.loadConfiguration(configFile);

		PriceManager.priceMap.clear();
		
		for (String materialName : config.getStringList("deny_sell")) {
			denySell.add(Material.getMaterial(materialName));
		}

		for (String materialName : ShopManager.priceConfig.getKeys(false)) {
			ConfigurationSection itemSect = ShopManager.priceConfig.getConfigurationSection(materialName);
			Integer price = itemSect.getInt("price");
			String section = itemSect.getString("section");
			List<Short> dataList = itemSect.getShortList("data");
			
			if (dataList.size() == 0) 
				dataList.add((short) 0);

			Material material = Material.getMaterial(materialName);
			MaterialPrice materialPrice = PriceManager.priceMap.get(material);
			for (Short data : dataList) {
				if (materialPrice == null) {
					materialPrice = new MaterialPrice();
				}
				materialPrice.setPrice(data, price);
				MaterialData item = new MaterialData(material, data);
				SectionManager.sections.get(section).add(item);
			}
			PriceManager.priceMap.put(material, materialPrice);
		}
	}
	
	public static int getPrice(GUI gui) {
		int total = 0;
		for(int i = 0; i < gui.size; i++) {
			ItemStack item = gui.contents[i];
			if(gui.getAction(i) == null && item != null) {
				if(PriceManager.hasPrice(item.getType(), item.getDurability()) && !Util.isCoin(item)) {
					total += PriceManager.getPrice(item.getType(), item.getDurability()) * item.getAmount();
				} else {
					return -1;
				}
			}
		}
		return total;
	}
	
	public static void returnItems(GUI gui) {
		for(int i = 0; i < gui.size; i++) {
			if(gui.getAction(i) == null)
				RPGPlayerManager.getPlayer(gui.player).giveItem(gui.contents[i]);
		}
	}
	
	public static void openSell(Player player) {
		sellTemp.open(player);
	}
	
	public static void openPage(final String sName, int page, final Player player) {
		final String sectionName = sName.toLowerCase();
		
		GUI shop = shopTemp.open(player);

		List<MaterialData> section = SectionManager.sections.get(sectionName);
		int pages = (int) Math.floor((section.size() - 1) / pageSize) + 1;
		if (page >= pages)
			page = 0;
		if (page < 0)
			page = pages - 1;
		int start = page * pageSize;
		int end = (page + 1) * pageSize;
		for (int i = start; i < end && i < section.size(); i++) {
			final MaterialData item = section.get(i);
			Integer price = PriceManager.getPrice(item.material, item.data);
			ItemInfo itemInfo = Items.itemByType(item.material, item.data);
			String name = itemInfo.name;
			ItemStack itemStack = new ItemBuilder(item.material, 1, item.data).setName("&b" + name + " &7(&e" + price + " coin" + (price > 1 ? "s" : "") + "&7)").setLore("&bClick to select amount.").getItem();
			int slotNumber = i - start;
			shop.setButton(slotNumber, itemStack, new GUIAction() {

				@Override
				public void onClick(GUIClickEvent e) {
					e.gui.closeInventory();
					openAmountSelection(player, item, 1, sName);
				}

			});
		}

		final int currentPage = page;

		shop.setButton(45, new ItemBuilder(Material.STAINED_GLASS_PANE).setName("&f&lPrevious page").getItem(), new GUIAction() {
			@Override
			public void onClick(GUIClickEvent event) {
				openPage(sectionName, currentPage - 1, player);
			}
		});

		shop.setButton(53, new ItemBuilder(Material.STAINED_GLASS_PANE).setName("&f&lNext Page").getItem(), new GUIAction() {
			@Override
			public void onClick(GUIClickEvent event) {
				openPage(sectionName, currentPage + 1, player);
			}
		});
	}

	public static void openAmountSelection(final Player player, final MaterialData item, final int amount, final String sName) {
		ItemInfo itemInfo = Items.itemByType(item.material, item.data);
		String amountText = Integer.toString(amount);

		if (amount >= 64) {
			amountText = (int) Math.floor(amount / 64) + " stack";
			
			if(amount >= 128)
				amountText += "s";

			if (amount % 64 != 0) 
				amountText += " and " + amount % 64;
			
			amountText += " of";
		}

		String title = amountText + " " + itemInfo.name + (amount > 1 && !itemInfo.name.endsWith("s") ? "s" : "");

		GUITemplate temp = new GUITemplate(27, title);
		GUI gui = temp.open(player);
		gui.setAllowChange(false);
		
		gui.setButton(3, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 5).setName("&a&lAdd 1").getItem(), new GUIAction() {
			@Override
			public void onClick(GUIClickEvent e) {
				int amount1 = amount;
				amount1++;
				if (amount1 < 1)
					amount1 = 1;
				openAmountSelection(player, item, amount1, sName);
			}
		});
		gui.setButton(4, new ItemBuilder(Material.STAINED_GLASS_PANE, 10, (short) 5).setName("&a&lAdd 10").getItem(), new GUIAction() {
			@Override
			public void onClick(GUIClickEvent e) {
				int amount1 = amount;
				amount1 += 10;
				if (amount1 < 1)
					amount1 = 1;
				openAmountSelection(player, item, amount1, sName);
			}
		});
		gui.setButton(5, new ItemBuilder(Material.STAINED_GLASS_PANE, 64, (short) 5).setName("&a&lAdd 64").getItem(), new GUIAction() {
			@Override
			public void onClick(GUIClickEvent e) {
				int amount1 = amount;
				amount1 += 64;
				if (amount1 < 1)
					amount1 = 1;
				openAmountSelection(player, item, amount1, sName);
			}
		});
		
		gui.setButton(21, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 14).setName("&c&lRemove 1").getItem(), new GUIAction() {
			@Override
			public void onClick(GUIClickEvent e) {
				int amount1 = amount;
				amount1--;
				if (amount1 < 1)
					amount1 = 1;
				openAmountSelection(player, item, amount1, sName);
			}
		});
		gui.setButton(22, new ItemBuilder(Material.STAINED_GLASS_PANE, 10, (short) 14).setName("&c&lRemove 10").getItem(), new GUIAction() {
			@Override
			public void onClick(GUIClickEvent e) {
				int amount1 = amount;
				amount1 -= 10;
				if (amount1 < 1)
					amount1 = 1;
				openAmountSelection(player, item, amount1, sName);
			}
		});
		gui.setButton(23, new ItemBuilder(Material.STAINED_GLASS_PANE, 64, (short) 14).setName("&c&lRemove 64").getItem(), new GUIAction() {
			@Override
			public void onClick(GUIClickEvent e) {
				int amount1 = amount;
				amount1 -= 64;
				if (amount1 < 1)
					amount1 = 1;
				openAmountSelection(player, item, amount1, sName);
			}
		});
		
		gui.setButton(10, new ItemBuilder(Material.STAINED_GLASS_PANE, 1, (short) 14).setName("&c&lCancel").getItem(), new GUIAction() {
			@Override
			public void onClick(GUIClickEvent e) {
				openPage(sName, 0, player);
			}
		});
		final int price = PriceManager.getPrice(item.material, item.data) * amount;
		gui.setItem(13, new ItemBuilder(item.material, amount, item.data).setName("&e" + price + " coins").getItem());
		gui.setButton(16, new ItemBuilder(Material.STAINED_GLASS_PANE).setName("&a&lBuy &7(&e" + price + " coin" + (price > 1 ? "s" : "") + "&7)").getItem(), new GUIAction() {
			@Override
			public void onClick(GUIClickEvent e) {
				RPGPlayer rPlayer = RPGPlayerManager.getPlayer(player);
				BigInteger priceBig = new BigInteger(Integer.toString(price));
				if(rPlayer.hasEnough(priceBig)) {
					rPlayer.removeCoins(priceBig);
					rPlayer.giveItem(new ItemBuilder(item.material, amount, item.data).getItem());
					e.gui.closeInventory();
					player.sendMessage(Util.parseColors("&e" + price + " coin" + (price > 1 ? "s &7have" : " &7has") + " been taken from you."));
				} else {
					player.sendMessage(ChatColor.RED + "You do not have enough money!");
				}
			}
		});
	}
	
}
