package net.bobmandude9889.rpg.shop;

import java.util.HashMap;

public class MaterialPrice {

	HashMap<Short, Integer> dataPrices;

	public MaterialPrice() {
		dataPrices = new HashMap<Short, Integer>();
	}

	public void setPrice(short data, Integer price) {
		dataPrices.remove(data);
		dataPrices.put(data, price);
	}
	
	public int getPrice(short data) {
		return dataPrices.getOrDefault(data, -1);
	}
	
}